module.exports = {
  aliases: {
    PropTypes: 'importjs-alias/prop-types',
    map: 'lodash/map',
    filter: 'lodash/filter',
    reject: 'lodash/reject',
    bind: 'lodash/bind',
    compact: 'lodash/compact',
    transform: 'lodash/transform',
    isArray: 'lodash/isArray',
    isFunction: 'lodash/isFunction',
    isNumber: 'lodash/isNumber',
    isString: 'lodash/isString',
    range: 'lodash/range',
    sortBy: 'lodash/sortBy',
    values: 'lodash/values',
    keys: 'lodash/keys',
    cx: 'classnames',
    Observable: 'rxjs/Observable',
    Subject: 'rxjs/Subject',
  },

  useRelativePaths: false,
  moduleNameFormatter: ({ moduleName, pathToCurrentFile }) => {
    if (moduleName.startsWith('importjs-alias/')) {
      return moduleName.slice('importjs-alias/'.length)
    }

    if (moduleName.startsWith('src/')) {
      return moduleName.slice(4)
    }

    return moduleName
  },

  importStatementFormatter: ({ importStatement }) => {
    return importStatement.replace(/;$/, '')
  },

  globals: ['module'],
}
