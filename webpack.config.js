const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')

const isDev = process.env.NODE_ENV !== 'production'
const src = path.resolve(__dirname, 'src')

const plugins = []

if (!isDev) {
  plugins.push(new webpack.optimize.UglifyJsPlugin())
}

plugins.push(
  new HtmlWebpackPlugin({
    template: './src/index.html',
  })
)

plugins.push(
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    filename: 'vendor.js',
  })
)

if (isDev) {
  plugins.push(new webpack.HotModuleReplacementPlugin())
}

module.exports = {
  devtool: 'sourcemap',

  resolve: {
    modules: [src, 'node_modules'],
  },

  entry: {
    vendor: [
      'react',
      'react-dom',
    ],

    index: isDev ? [
      'whatwg-fetch',
      'babel-polyfill',
      'react-hot-loader/patch',
      'webpack-hot-middleware/client',
      path.join(src, 'index.js'),
    ] : [
      'whatwg-fetch',
      'babel-polyfill',
      path.join(src, 'index.js'),
    ],
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[hash].js',
    publicPath: '/assets/',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: { loader: 'babel-loader' },
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
          'postcss-loader',
        ],
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
        ],
      },
    ],
  },

  plugins,
}
