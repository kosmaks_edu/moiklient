const path = require('path')

module.exports = {
  plugins: {
    'precss': {
      path: path.resolve(__dirname, 'src'),
    },
    'postcss-cssnext': {},
    'postcss-custom-media': {},
    'postcss-color-function': {},
  },
}
