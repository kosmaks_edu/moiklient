import 'rxjs/add/operator/do'

import 'helpers/momentLocale'

import { AppRegistry } from 'react-native'
import { ConnectedRouter } from 'react-router-redux'
import { Provider } from 'react-redux'
import React, { Component } from 'react'
import createHistory from 'history/createMemoryHistory'

import App from 'components/App'
import createStore from 'store'

const history = createHistory()
const { store } = createStore({ history })

export default class MoiKlient extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
      </Provider>
    )
  }
}

AppRegistry.registerComponent('moiklient', () => MoiKlient)
