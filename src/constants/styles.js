// Colors

import Color from 'color'

export const COLOR_SHADOW = 'rgba(0, 0, 0, .3)'
export const COLOR_BACKGROUND = '#ECF0F1'
export const COLOR_BACKGROUND_LIGHT = '#FAFAFA'
export const COLOR_WHITE = '#FFFFFF'
export const COLOR_BORDER = '#E5E5E5'

export const COLOR_PRIMARY = '#E74C3C'
export const COLOR_PRIMARY_ACTIVE = '#C63B2B'
export const COLOR_PRIMARY_LIGHTEN = Color(COLOR_PRIMARY)
  .lighten(0.7)
  .string()

export const COLOR_SECONDARY = '#3498DB'
export const COLOR_SECONDARY_ACTIVE = '#2387CA'
export const COLOR_SECONDARY_LIGHTEN = Color(COLOR_SECONDARY)
  .lighten(0.7)
  .string()

export const COLOR_EVENT = '#B97FB9'
export const COLOR_EVENT_BACKGROUND = Color(COLOR_EVENT)
  .fade(0.7)
  .string()

export const COLOR_FONT_DEFAULT = '#444'
export const COLOR_FONT_LIGHTEN = '#505050'

export const COLOR_VK = '#507299'
export const COLOR_INSTAGRAM = '#3897F0'
export const COLOR_FACEBOOK = '#3B5998'

// Sizes

export const BORDER_RADIUS = 4
