const appName = 'Мой Клиент'

const environment =
  process.env.NODE_ENV === 'production' ? 'production' : 'development'

const api = {
  url: 'http://localhost:8000/api',
}

export default {
  appName,
  environment,
  api,
}
