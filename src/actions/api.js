export const genAsyncTypes = name => ({
  REQUEST: `${name}.REQUEST`,
  SUCCESS: `${name}.SUCCESS`,
  FAILURE: `${name}.FAILURE`,
})

export const API_QUERY = 'API_QUERY'

export const apiQuery = payload => ({
  type: API_QUERY,
  payload,
})
