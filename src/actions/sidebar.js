export const SET_SIDEBAR_VISIBILITY = 'SET_SIDEBAR_VISIBILITY'

export const openSidebar = () => ({
  type: SET_SIDEBAR_VISIBILITY,
  payload: true,
})

export const closeSidebar = () => ({
  type: SET_SIDEBAR_VISIBILITY,
  payload: false,
})
