import Observable from 'rxjs/Observable'

export const uploadImage = file => () => Observable.just(file).delay(3000)
