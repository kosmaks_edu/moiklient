export const SET_ONBOARDING_STEP = 'SET_ONBOARDING_STEP'

export const setOnboardingStep = step => ({
  type: SET_ONBOARDING_STEP,
  payload: step,
})
