import { apiQuery, genAsyncTypes } from 'actions/api'

export const SIGN_UP = genAsyncTypes('SIGN_UP')

export const signUp = ({ email, password, fullName }) =>
  apiQuery({
    types: SIGN_UP,
    method: 'POST',
    path: '/signUp',
    query: { email, password, fullName },
  })

export const SIGN_IN = genAsyncTypes('SIGN_IN')

export const signIn = ({ email, password }) =>
  apiQuery({
    types: SIGN_IN,
    method: 'POST',
    path: '/signIn',
    query: { email, password },
  })

export const BECOME_USER = genAsyncTypes('BECOME_USER')

export const becomeUser = () =>
  apiQuery({
    types: BECOME_USER,
    method: 'POST',
    path: '/become',
  })

export const LOG_OUT = 'LOG_OUT'

export const logOut = () => ({
  type: LOG_OUT,
})
