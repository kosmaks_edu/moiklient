import { apiQuery } from 'actions/api'
import { createAsyncAction } from 'helpers/redux'

export const LOAD_SERVICES = createAsyncAction('LOAD_SERVICES')

export const loadServices = () =>
  apiQuery({
    path: '/services',
    types: LOAD_SERVICES,
  })

export const CREATE_SERVICE = createAsyncAction('CREATE_SERVICE')

export const createService = data =>
  apiQuery({
    path: '/services',
    method: 'POST',
    types: CREATE_SERVICE,
    query: data,
  })

export const UPDATE_SERVICE = createAsyncAction('UPDATE_SERVICE')

export const updateService = (id, data) =>
  apiQuery({
    path: '/services',
    method: 'PUT',
    types: UPDATE_SERVICE,
    query: { ...data, id },
  })

export const DELETE_SERVICE = createAsyncAction('DELETE_SERVICE')

export const deleteService = id =>
  apiQuery({
    path: '/services',
    method: 'DELETE',
    types: DELETE_SERVICE,
    query: { id },
    meta: { id },
  })
