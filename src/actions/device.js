export const CHANGE_DEVICE = 'CHANGE_DEVICE'

export const changeDevice = (device, orientation) => ({
  type: CHANGE_DEVICE,
  payload: { device, orientation },
})

export default {
  changeDevice,
}
