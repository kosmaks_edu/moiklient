import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

import ClientRoutes from 'components/ClientRoutes'
import EditEventPage from 'components/pages/EditEvent/EditEventPage'
import EditService from 'components/pages/EditService'
import EventRequest from 'components/pages/EventRequest'
import EventViewPage from 'components/pages/EventViewPage'
import Main from 'components/layouts/Main'
import Onboarding from 'components/pages/Onboarding'
import OnboardingLayout from 'components/layouts/OnboardingLayout'
import Requests from 'components/pages/Requests'
import Schedule from 'components/pages/Schedule'
import ServicesPage from 'components/pages/ServicesPage'
import Settings from 'components/pages/Settings'
import SidebarLayout from 'components/layouts/SidebarLayout'
import SignIn from 'components/pages/SignIn'
import SignUp from 'components/pages/SignUp'
import routes, { multiRoute } from 'helpers/routes'
import selectors from 'store/reducers/selectors'

const signUpPath = multiRoute(routes.root(), routes.signUp())

const schedulePath = multiRoute(
  routes.root(),
  routes.monthlySchedule(),
  routes.dailySchedule()
)

const settingsPath = multiRoute(
  routes.settings(),
  routes.profileSettings(),
  routes.scheduleSettings(),
  routes.additionalSettings(),
  routes.paymentsSettings()
)

class Routes extends Component {
  renderCommonRoutes() {
    return <ClientRoutes />
  }

  renderUnauthorizedRoutes() {
    return (
      <OnboardingLayout>
        <Switch>
          <Route exact path={signUpPath} component={SignUp} />
          <Route exact path={routes.signIn()} component={SignIn} />
        </Switch>
      </OnboardingLayout>
    )
  }

  renderOnboardingRoutes() {
    return (
      <Switch>
        <Route exact path={routes.root()} component={Onboarding} />
        <Route
          component={() => (
            <SidebarLayout noDrawer navbar={this.renderNavbarRoutes()}>
              <Switch>
                <Route
                  exact
                  path={routes.editService()}
                  component={EditService}
                />
                <Route
                  exact
                  path={routes.newService()}
                  component={EditService}
                />
              </Switch>
            </SidebarLayout>
          )}
        />
      </Switch>
    )
  }

  renderNavbarRoutes() {
    if (this.props.isDesktop) return null
    return (
      <Switch>
        <Route exact path={schedulePath} component={Schedule.navbar} />
        <Route
          exact
          path={routes.newEventPath()}
          component={EditEventPage.navbar}
        />
        <Route
          exact
          path={routes.editEventPath()}
          component={EditEventPage.navbar}
        />
        <Route
          exact
          path={routes.eventViewPath()}
          component={EventViewPage.navbar}
        />
        <Route
          exact
          path={routes.eventRequestPath()}
          component={EventRequest.navbar}
        />
        <Route exact path={routes.requests()} component={Requests.navbar} />
        <Route exact path={routes.services()} component={ServicesPage.navbar} />
        <Route exact path={settingsPath} component={Settings.navbar} />
      </Switch>
    )
  }

  renderAuthorizedRoutes() {
    return (
      <Switch>
        <Route exact path={routes.editService()} component={EditService} />
        <Route exact path={routes.newService()} component={EditService} />

        <SidebarLayout navbar={this.renderNavbarRoutes()}>
          <Switch>
            <Route exact path={schedulePath} component={Schedule} />

            <Route exact path={routes.newEventPath()} component={EditEventPage} />
            <Route
              exact
              path={routes.editEventPath()}
              component={EditEventPage}
            />
            <Route
              exact
              path={routes.eventViewPath()}
              component={EventViewPage}
            />
            <Route
              exact
              path={routes.eventRequestPath()}
              component={EventRequest}
            />
            <Route exact path={routes.requests()} component={Requests} />
            <Route exact path={routes.services()} component={ServicesPage} />
            <Route exact path={settingsPath} component={Settings} />
          </Switch>
        </SidebarLayout>
      </Switch>
    )
  }

  render() {
    const { isAuthorized, onboardingIsFinished } = this.props

    return (
      <Main>
        <Switch>
          {!isAuthorized && this.renderUnauthorizedRoutes()}
          {isAuthorized &&
            (onboardingIsFinished
              ? this.renderAuthorizedRoutes()
              : this.renderOnboardingRoutes())}
        </Switch>
      </Main>
    )
  }
}

Routes.propTypes = {
  isDesktop: PropTypes.bool.isRequired,
  isAuthorized: PropTypes.bool.isRequired,
  onboardingIsFinished: PropTypes.bool.isRequired,
}

const selector = createStructuredSelector({
  isDesktop: selectors.isDesktop,
  isAuthorized: selectors.isAuthorized,
  onboardingIsFinished: selectors.onboardingIsFinished,
})

export default connect(selector, null, null, { pure: false })(Routes)
