import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'

import Button from 'components/ui-elements/Button'
import Switcher from 'components/ui-elements/Switcher'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

const ScheduleHeader = ({ pathname, onPush }) => {
  const isMonth = pathname === '/' || pathname === routes.monthlySchedule()
  const onSwitcherChange = route =>
    onPush(
      {
        month: routes.monthlySchedule(),
        day: routes.dailySchedule(),
      }[route]
    )

  return (
    <View
      border="bnormal"
      background="light"
      padding={['hlarge', 'tlarge', 'bnormal', 'tmnormal', 'hmnormal']}
      flexDirection={['row']}
      align={['center', 'mstretch']}
    >
      <View flexGrow={1} padding="bmnormal">
        <Switcher
          active={isMonth ? 'month' : 'day'}
          items={[
            { value: 'month', label: 'Месяц' },
            { value: 'day', label: 'День' },
          ]}
          onChange={onSwitcherChange}
        />
      </View>

      <Button icon="plus">Добавить клиента</Button>
    </View>
  )
}

ScheduleHeader.propTypes = {
  pathname: PropTypes.string.isRequired,
  onPush: PropTypes.func.isRequired,
}

const selector = createStructuredSelector({
  pathname: selectors.getPathname,
})

const actions = {
  onPush: push,
}

export default connect(selector, actions)(ScheduleHeader)
