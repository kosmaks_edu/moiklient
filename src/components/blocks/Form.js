import { Component } from 'react'
import bind from 'lodash/bind'
import reduce from 'lodash/reduce'
import isEmpty from 'lodash/isEmpty'
import get from 'lodash/get'

export const validations = {
  required: value => !value && 'Field is required',
  number: value => value && isNaN(parseFloat(value)) && 'Must be a number',
}

class Form extends Component {
  constructor(props, context) {
    super(props, context)

    this.setField = bind(this.setField, this)
    this.setFields = bind(this.setFields, this)
    this.handleSubmit = bind(this.handleSubmit, this)

    this.validations = {}

    this.state = {
      fields: this.getInitialFieldsFromProps(props),
      isSaving: false,
      errors: {},
    }
  }

  componentWillUnmount() {
    if (this.request$) {
      this.request$.unsubscribe()
    }
  }

  onValidationFail() {}

  getInitialFieldsFromProps() {
    return {}
  }

  setFields(values) {
    this.setState(state => ({ fields: { ...state.fields, ...values } }))
  }

  setField(field, value) {
    this.setFields({ [field]: value })
  }

  getField(field) {
    return get(this.state.fields, field)
  }

  getErrors(field) {
    return (this.state.errors || {})[field] || []
  }

  getInputProps(field) {
    return {
      onChange: val => this.setField(field, val),
      value: this.getField(field),
    }
  }

  getRadioProps(field, value) {
    return {
      onChange: val => val && this.setField(field, value),
      value: this.getField(field) === value,
    }
  }

  handleSubmit() {
    const errors = this.validate()

    if (!isEmpty(errors)) {
      this.onValidationFail(errors)
      return this.setState({ errors })
    }

    const onEnd = () => this.setState({ isSaving: false })

    this.setState({ errors: {}, isSaving: true }, () => {
      const result = this.submit()

      if (!result || !result.subscribe) {
        return onEnd()
      }

      this.request$ = result.subscribe(onEnd, onEnd)
    })
  }

  validate() {
    return reduce(
      this.validations,
      (acc, fieldValidations, field) => {
        const value = this.getField(field)
        const fieldErrors = reduce(
          fieldValidations,
          (errors, validator) => {
            const error = validator(value)
            if (error) {
              return [...errors, error]
            }

            return errors
          },
          []
        )

        return fieldErrors.length > 0 ? { ...acc, [field]: fieldErrors } : acc
      },
      {}
    )
  }
}

export default Form
