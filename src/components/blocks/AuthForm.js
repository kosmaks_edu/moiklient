import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { signIn, signUp } from 'actions/viewer'
import Button from 'components/ui-elements/Button'
import Form from 'components/blocks/Form'
import Input from 'components/ui-elements/Input'
import SplitLine from 'components/ui-elements/SplitLine'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'

class AuthForm extends Form {
  constructor(props, context) {
    super(props, context)

    this.state = {
      ...this.state,
      isExpanded: false,
    }
  }

  signUp() {
    const { onSignUp } = this.props

    return onSignUp(this.state.fields)
  }

  signIn() {
    const { onSignIn } = this.props

    return onSignIn(this.state.fields)
  }

  submit() {
    if (this.props.isSignIn) {
      return this.signIn()
    } else {
      return this.signUp()
    }
  }

  renderTabs() {
    const { isSignIn, onPush } = this.props

    return (
      <View flexDirection="row" border="bnormal" padding="bsmall">
        <View flexGrow={1} padding="rsmall">
          <Button
            outline
            large
            borderless
            footer
            colorless={isSignIn}
            onClick={() => onPush(routes.signUp())}
          >
            Регистрация
          </Button>
        </View>
        <View flexGrow={1}>
          <Button
            outline
            large
            borderless
            footer
            colorless={!isSignIn}
            onClick={() => onPush(routes.signIn())}
          >
            Войти
          </Button>
        </View>
      </View>
    )
  }

  renderField(title, field) {
    return (
      <View padding="bnormal">
        <Text bold>{title}:</Text>
        <View padding="bsmall" />
        {field}
      </View>
    )
  }

  renderSignUpForm() {
    const { isExpanded } = this.state

    if (!isExpanded) {
      return (
        <View>
          {this.renderOauthLinks('Регистрация')}
          <Button
            icon="envelope"
            onClick={() => this.setState({ isExpanded: true })}
          >
            Регистрация по <Text bold>Email</Text>
          </Button>
        </View>
      )
    }

    return (
      <View>
        {this.renderField(
          'Email',
          <Input
            icon="envelope"
            type="email"
            placeholder="somebody@example.com"
            {...this.getInputProps('email')}
          />
        )}
        {this.renderField(
          'Имя',
          <Input
            icon="user"
            placeholder="Ваше имя"
            {...this.getInputProps('fullName')}
          />
        )}
        {this.renderField(
          'Пароль',
          <Input
            icon="lock"
            type="password"
            placeholder="********"
            {...this.getInputProps('password')}
          />
        )}
        {this.renderField(
          'Повторите пароль',
          <Input
            icon="lock"
            type="password"
            placeholder="********"
            {...this.getInputProps('passwordConfirm')}
          />
        )}
        <Button onClick={this.handleSubmit}>Зарегистрироваться</Button>
        <View padding="vnormal">
          <SplitLine text="или" />
        </View>
        {this.renderOauthLinks('Зарегестрироваться')}
      </View>
    )
  }

  renderSignInForm() {
    return (
      <View>
        {this.renderField(
          'Email',
          <Input
            icon="envelope"
            type="email"
            placeholder="somebody@example.com"
            {...this.getInputProps('email')}
          />
        )}
        {this.renderField(
          'Пароль',
          <Input
            icon="lock"
            type="password"
            placeholder="********"
            {...this.getInputProps('password')}
          />
        )}
        <Button onClick={this.handleSubmit}>Войти</Button>
        <View padding="vnormal">
          <SplitLine text="или" />
        </View>
        {this.renderOauthLinks('Войти')}
      </View>
    )
  }

  renderOauthLinks(action, onClick) {
    return (
      <View>
        <View padding="bnormal">
          <Button onClick={onClick} icon="vk" color="vk">
            {action} через <Text bold>Вконтакте</Text>
          </Button>
        </View>
        <View padding="bnormal">
          <Button onClick={onClick} icon="instagram" color="instagram">
            {action} через <Text bold>Instagram</Text>
          </Button>
        </View>
        <View padding="bnormal">
          <Button onClick={onClick} icon="facebook" color="facebook">
            {action} через <Text bold>Facebook</Text>
          </Button>
        </View>
      </View>
    )
  }

  render() {
    const { isSignIn } = this.props

    return (
      <View flexGrow={1} scrollable>
        <View flexDirection="row" justify="center" padding={['hnormal']}>
          <View flexGrow={1} style={{ maxWidth: 370 }}>
            {this.renderTabs()}
            <View padding="vnormal">
              {isSignIn ? this.renderSignInForm() : this.renderSignUpForm()}
            </View>
          </View>
        </View>
      </View>
    )
  }
}

AuthForm.propTypes = {
  isSignIn: PropTypes.bool,
  onPush: PropTypes.func,
}

const actions = {
  onPush: push,
  onSignUp: signUp,
  onSignIn: signIn,
}

export default connect(null, actions)(AuthForm)
