import React from 'react'

import ScheduleSettings from 'components/blocks/ScheduleSettings'

const ScheduleStep = () => <ScheduleSettings isOnboarding />

export default ScheduleStep
