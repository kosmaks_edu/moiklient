import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'
import map from 'lodash/map'
import moment from 'moment'
import range from 'lodash/range'

import { MONTHS, WEEK_DAYS_SHORT } from 'constants/calendar'
import { platformPick } from 'helpers/platform'
import CalendarSwitcher from 'components/blocks/CalendarSwitcher'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'

import GridSwiper from './GridSwiper'

class MonthGrid extends Component {
  constructor(props) {
    super(props)

    this.onSetMonth = bind(this.onSetMonth, this)
    this.onNextMonth = bind(this.onNextMonth, this)
    this.onPrevMonth = bind(this.onPrevMonth, this)
    this.onToday = bind(this.onToday, this)
    this.state = { selectedDay: null }
  }

  getYear() {
    return this.props.date.year
  }

  getMonth() {
    return this.props.date.month
  }

  onPrevMonth() {
    const { onSelectDate } = this.props
    const month = this.getMonth()
    const year = this.getYear()

    const newDates =
      month === 1 ? { year: year - 1, month: 12 } : { year, month: month - 1 }

    if (this._swiper && this._swiper.scrollTo) {
      this._swiper.scrollTo(newDates)
    } else {
      onSelectDate(newDates.year, newDates.month)
    }
  }

  onToday() {
    const { onSelectDate } = this.props

    const today = moment()
    const year = today.year()
    const month = today.month() + 1

    if (this._swiper && this._swiper.scrollTo) {
      this._swiper.scrollTo({ month, year })
    } else {
      onSelectDate(year, month)
    }
  }

  onNextMonth() {
    const { onSelectDate } = this.props
    const month = this.getMonth()
    const year = this.getYear()

    const newDates =
      month === 12 ? { year: year + 1, month: 1 } : { year, month: month + 1 }

    if (this._swiper && this._swiper.scrollTo) {
      this._swiper.scrollTo(newDates)
    } else {
      onSelectDate(newDates.year, newDates.month)
    }
  }

  onSetMonth({ year, month }) {
    this.props.onSelectDate(year, month)
  }

  renderHeading() {
    const month = this.getMonth()
    const year = this.getYear()

    return (
      <View flexDirection="row" padding={['large', 'mnormal']} align="center">
        <View flexGrow={1}>
          <Text size={['large', 'mmedium']}>
            {MONTHS[month - 1]}{' '}
            <Text size={['large', 'mmedium']} light>
              {year}
            </Text>
          </Text>
        </View>

        <CalendarSwitcher
          onToday={this.onToday}
          onForward={this.onNextMonth}
          onBack={this.onPrevMonth}
        />
      </View>
    )
  }

  renderGridHeading() {
    return (
      <View flexDirection="row">
        {map(range(0, 7), day => (
          <View
            key={day}
            flexGrow={1}
            padding={['vsmall', 'hmedium']}
            align={['end', 'mcenter']}
            border="bnormal"
          >
            <Text color="lighten">{WEEK_DAYS_SHORT[day]}</Text>
          </View>
        ))}
      </View>
    )
  }

  render() {
    const { selectedDay } = this.state

    return (
      <View flexDirection="column" flexGrow={1}>
        {this.renderHeading()}
        {this.renderGridHeading()}

        <GridSwiper
          ref={platformPick({ mobile: () => el => (this._swiper = el) })}
          year={this.getYear()}
          month={this.getMonth()}
          selectedDay={selectedDay}
          onSetMonth={this.onSetMonth}
          onSelect={date => this.setState({ selectedDay: date })}
          onDeselect={() => this.setState({ selectedDay: null })}
        />
      </View>
    )
  }
}

MonthGrid.propTypes = {
  date: PropTypes.object.isRequired,
  isTabletOrLarger: PropTypes.bool.isRequired,
  onSelectDate: PropTypes.func.isRequired,
}

export default MonthGrid
