import { InteractionManager, View as RNView, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import React, { PureComponent, Component } from 'react'
import bind from 'lodash/bind'
import map from 'lodash/map'
import moment from 'moment'
import range from 'lodash/range'
import transform from 'lodash/transform'

import { COLOR_BORDER } from 'constants/styles'
import { MOBILE, getDevice } from 'helpers/device'
import { formatDate } from 'helpers/monthGrid'
import MonthCell from 'components/ui-elements/MonthCell'
import Spinner from 'components/ui-elements/Spinner'
import VirtualListView from 'components/blocks/MonthGrid/GridSwiper/VirtualListView'

const loBound = -300
const hiBound = 1000
const zeroWeek = moment()
  .startOf('month')
  .startOf('isoweek')

const genWeekFunc = (week, month) => () => {
  const cursor = moment(week)
  const today = moment().format('YYYY-MM-DD')
  const result = []

  for (let i = 0; i < 7; ++i) {
    const day = formatDate(cursor, month, today)
    result.push(day)
    cursor.add(1, 'day')
  }

  return result
}

const weeks = range(loBound, hiBound).map(x => moment(zeroWeek).add(x, 'week'))

const { items, weekIndex } = transform(
  weeks,
  (acc, week) => {
    const eow = moment(week).endOf('isoweek')
    const eowStr = eow.format('YYYY-MM')
    const monthSwitch = eow.month() !== week.month()

    if (!(eowStr in acc.weekIndex)) {
      acc.weekIndex[eowStr] = acc.items.length + (monthSwitch ? 1 : 0)
    }

    acc.items.push(genWeekFunc(week, week.month() + 1))
    if (monthSwitch) {
      acc.items.push(genWeekFunc(week, (week.month() + 1) % 12 + 1))
    }
  },
  { items: [], weekIndex: {} }
)

const itemHeight = getDevice() === MOBILE ? 70 : 100

const styles = StyleSheet.create({
  row: {
    height: itemHeight,
    borderBottomWidth: 1,
    borderBottomColor: COLOR_BORDER,
    flexDirection: 'row',
  },

  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  container: {
    flex: 1,
  },
})

class Row extends PureComponent {
  constructor(props, context) {
    super(props, context)
    this._cells = {}

    this.state = { loaded: false }
  }

  componentDidMount() {
    this._timeout = setImmediate(() => {
      this.setState(
        { loaded: true },
        () => this.pendingData && this.putData(this.pendingData)
      )
    })
  }

  componentWillUnmount() {
    if (this._timeout) {
      clearImmediate(this._timeout)
    }
  }

  putData({ item }) {
    this.pendingData = { item }
    const days = item ? item() : []

    days.forEach((day, i) => {
      const ref = this._cells[i]
      if (ref && ref.putData) {
        ref.putData(day)
      }
    })
  }

  render() {
    const { loaded } = this.state

    if (!loaded) {
      return <RNView style={styles.row} />
    }

    return (
      <RNView style={styles.row}>
        {map(range(0, 7), i => (
          <MonthCell key={i} ref={el => (this._cells[i] = el)} />
        ))}
      </RNView>
    )
  }
}

class GridSwiper extends Component {
  constructor(props, context) {
    super(props, context)

    this.onIndexChange = bind(this.onIndexChange, this)
    this.scrollTo = bind(this.scrollTo, this)

    const { month, year } = props

    const initialKey = moment()
      .month(month - 1)
      .year(year)
      .format('YYYY-MM')

    this.initialIndex = weekIndex[initialKey] || 0
    this.state = { loaded: false }
  }

  componentDidMount() {
    this._timeout = requestAnimationFrame(() => {
      InteractionManager.runAfterInteractions(() => {
        this.setState({ loaded: true })
      })
    })
  }

  componentWillUnmount() {
    if (this._timeout) {
      cancelAnimationFrame(this._timeout)
    }
  }

  scrollTo({ month, year }) {
    if (this._list) {
      const key = moment()
        .month(month - 1)
        .year(year)
        .format('YYYY-MM')
      const index = weekIndex[key] || 0
      this._timeout = requestAnimationFrame(() => this._list.scrollTo(index))
    }
  }

  onIndexChange(index) {
    const daysFunc = items[index]

    if (daysFunc && this.props.onSetMonth) {
      const days = daysFunc()
      const lastDay = days[days.length - 1]
      const month = lastDay.cursor.month() + 1
      const year = lastDay.cursor.year()

      if (month !== this.props.month || year !== this.props.year) {
        this.props.onSetMonth({ month, year })
      }
    }
  }

  render() {
    const { loaded } = this.state

    if (!loaded) {
      return (
        <RNView style={styles.loading}>
          <Spinner />
        </RNView>
      )
    }

    return (
      <RNView style={styles.container}>
        <VirtualListView
          ref={el => (this._list = el)}
          items={items}
          RenderComponent={Row}
          itemsBuffer={16}
          itemsBufferBefore={3}
          itemHeight={itemHeight}
          initialIndex={this.initialIndex}
          onIndexChange={this.onIndexChange}
        />
      </RNView>
    )
  }
}

GridSwiper.propTypes = {
  selectedDay: PropTypes.string,
  year: PropTypes.number,
  month: PropTypes.number,
  onSetMonth: PropTypes.func,
}

export default GridSwiper
