import React from 'react'

import Grid from 'components/blocks/MonthGrid/Grid'

const GridSwiper = props => <Grid {...props} />

export default GridSwiper
