import { View as RNView, ScrollView } from 'react-native'
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import bind from 'lodash/bind'
import map from 'lodash/map'
import range from 'lodash/range'
import throttle from 'lodash/throttle'

const cycleSub = (num, total, offset) => {
  const temp = num - offset % total
  if (temp < 0) return total + temp
  return temp
}

class Item extends PureComponent {
  constructor(props, context) {
    super(props, context)
    this.order = props.realIndex
    this.index = this.getIndex()
  }

  componentDidMount() {
    if (this._ref) {
      this._ref.setNativeProps({
        style: { top: this.order * this.getItemHeight() },
      })
    }
  }

  componentWillUnmount() {
    if (this._immediate) {
      clearImmediate(this._immediate)
    }
  }

  getIndex() {
    return this.props.scrollView.offset + this.order
  }

  getItem() {
    const index = this.getIndex()
    const items = this.props.scrollView.props.items

    if (index < 0 || index >= items.length - 1) {
      return null
    }

    return items[index]
  }

  getItemHeight() {
    return this.props.scrollView.props.itemHeight
  }

  setOrder(order) {
    if (this._ref && this.order !== order) {
      this.order = order
      this._ref.setNativeProps({
        style: { top: this.getIndex() * this.getItemHeight() },
      })
      const newIndex = this.getIndex()
      if (newIndex !== this.index) {
        this.index = newIndex
        this._immediate = setImmediate(() => {
          this.updateView()
        })
      }
    }
  }

  updateView() {
    if (this._comp && this._comp.putData) {
      const index = this.getIndex()
      const item = this.getItem()
      this._comp.putData({ item, index })
    } else {
      this.forceUpdate()
    }
  }

  render() {
    const { RenderComponent } = this.props

    const index = this.getIndex()
    const item = this.getItem()

    return (
      <RNView
        ref={el => (this._ref = el)}
        style={{
          height: this.getItemHeight(),
          borderBottomWidth: 1,
          position: 'absolute',
          left: 0,
          right: 0,
        }}
      >
        {item && (
          <RenderComponent
            ref={el => {
              this._comp = el
            }}
            item={item}
            index={index}
          />
        )}
      </RNView>
    )
  }
}

Item.propTypes = {
  RenderComponent: PropTypes.any.isRequired,
  scrollView: PropTypes.any.isRequired,
  realIndex: PropTypes.number.isRequired,
}

class VirtualListView extends PureComponent {
  constructor(props, context) {
    super(props, context)

    this.renderItem = bind(this.renderItem, this)
    this.onScroll = bind(this.onScroll, this)
    this.onMomentumScrollBegin = bind(this.onMomentumScrollBegin, this)
    this.onMomentumScrollEnd = bind(this.onMomentumScrollEnd, this)
    this.scrollTo = throttle(bind(this.scrollTo, this), 500)

    this.itemRefs = {}
    this.buffer = range(0, props.itemsBuffer)
    this.offset = 0
  }

  componentDidMount() {
    this._timeout = setTimeout(() => {
      if (this._innerView && this._scrollView) {
        const { initialIndex, itemHeight } = this.props
        this.offset = initialIndex
        this._scrollView.scrollTo({
          x: 0,
          y: this.offset * itemHeight,
          animated: false,
        })

        this.updateViews()
      }
    }, 0)
  }

  componentWillUnmount() {
    if (this._timeout) {
      clearTimeout(this._timeout)
    }
  }

  scrollTo(index) {
    const scroll = () => {
      const { itemHeight } = this.props

      this.offset = index
      this._scrollView.scrollTo({
        x: 0,
        y: this.offset * itemHeight,
        animated: true,
      })
      this.updateViews()

      if (this.props.onIndexChange) {
        this.props.onIndexChange(this.offset)
      }
    }

    if (this._scrolling) {
      this._pendingEvent = scroll
    } else {
      scroll()
    }
  }

  setOrder(index, order) {
    const item = this.itemRefs[index]

    if (item) {
      item.setOrder(order)
    }
  }

  updateViews() {
    const { itemsBuffer, itemsBufferBefore } = this.props

    for (let i = 0; i < itemsBuffer; ++i) {
      const key = cycleSub(i, itemsBuffer, this.offset) - itemsBufferBefore
      this.setOrder(i, key)
    }
  }

  onScroll(e) {
    const offsetTop = Math.max(e.nativeEvent.contentOffset.y, 0)

    if (this._innerView) {
      const { itemHeight } = this.props
      const offset = Math.floor(offsetTop / itemHeight)
      const diff = offset - this.offset

      if (diff !== 0) {
        this.offset = offset
        this.updateViews()

        if (this.props.onIndexChange) {
          this.props.onIndexChange(this.offset)
        }
      }
    }
  }

  onMomentumScrollBegin() {
    this._scrolling = true
  }

  onMomentumScrollEnd() {
    this._scrolling = false

    if (this._pendingEvent) {
      this._pendingEvent()
      this._pendingEvent = false
    }
  }

  renderItem(realIndex) {
    const { RenderComponent } = this.props

    return (
      <Item
        key={realIndex}
        scrollView={this}
        realIndex={realIndex}
        ref={el => {
          this.itemRefs[realIndex] = el
        }}
        RenderComponent={RenderComponent}
      />
    )
  }

  render() {
    const { items, itemHeight } = this.props
    const renderedItems = map(this.buffer, this.renderItem)

    return (
      <RNView
        ref={el => {
          this._wrapper = el
        }}
        style={{ flex: 1 }}
      >
        <ScrollView
          ref={el => {
            this._scrollView = el
          }}
          scrollEventThrottle={16}
          showsVerticalScrollIndicator={false}
          onScroll={this.onScroll}
          onMomentumScrollBegin={this.onMomentumScrollBegin}
          onMomentumScrollEnd={this.onMomentumScrollEnd}
        >
          <RNView
            ref={el => {
              this._innerView = el
            }}
            style={{
              position: 'relative',
              height: items.length * itemHeight,
            }}
          >
            {renderedItems}
          </RNView>
        </ScrollView>
      </RNView>
    )
  }
}

VirtualListView.propTypes = {
  items: PropTypes.array.isRequired,
  itemsBuffer: PropTypes.number.isRequired,
  itemsBufferBefore: PropTypes.number.isRequired,
  itemHeight: PropTypes.number.isRequired,
  initialIndex: PropTypes.number.isRequired,
  RenderComponent: PropTypes.any.isRequired,
  onIndexChange: PropTypes.func,
}

VirtualListView.defaultProps = {
  initialIndex: 0,
  itemsBufferBefore: 0,
}

export default VirtualListView
