import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import map from 'lodash/map'

import { getMonthGrid } from 'helpers/monthGrid'
import MonthCell from 'components/ui-elements/MonthCell'
import View from 'components/ui-elements/View'
import selectors from 'store/reducers/selectors'

class Grid extends PureComponent {
  render() {
    const {
      year,
      month,
      selectedDay,
      isTabletOrLarger,
      onSelect,
      onDeselect,
    } = this.props
    const grid = getMonthGrid(year, month)

    return (
      <View>
        {map(grid, (week, key) => (
          <View key={key} flexDirection="row">
            {map(week, (day, i) => (
              <MonthCell
                text={day.text}
                key={day.date}
                isCurrentMonth={day.currentMonth}
                isToday={day.today}
                isLast={key === grid.length - 1}
                isLastWeekday={i === week.length - 1}
                isSelected={selectedDay === day.date}
                isTabletOrLarger={isTabletOrLarger}
                items={
                  i <= 4 ? (
                    [
                      { text: 'Ирина', time: '12:00' },
                      { text: 'Кристина', time: '13:00' },
                      { text: 'Лиза', time: '14:00' },
                      { text: 'Света', time: '15:00' },
                      { text: 'Оля', time: '16:00' },
                    ]
                  ) : (
                    []
                  )
                }
                isWeekend={i > 4}
                onSelect={() => onSelect && onSelect(day.date)}
                onDeselect={() => onDeselect && onDeselect(day.date)}
              />
            ))}
          </View>
        ))}
      </View>
    )
  }
}

Grid.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
  selectedDay: PropTypes.string,
  isTabletOrLarger: PropTypes.bool.isRequired,
  onSelect: PropTypes.func,
  onDeselect: PropTypes.func,
}

const selector = createStructuredSelector({
  isTabletOrLarger: selectors.isTabletOrLarger,
})

export default connect(selector)(Grid)
