import { connect } from 'react-redux'
import { createSelector, createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'

import moment from 'moment'

import selectors from 'store/reducers/selectors'

import routes from 'helpers/routes'

import MonthGrid from './MonthGrid'

const dateSelector = createSelector(selectors.getQuery, query => {
  const month = parseFloat(query.month)
  const year = parseFloat(query.year)
  const today = moment()

  return {
    year: isNaN(year) ? today.year() : year,
    month: isNaN(month) || month > 12 || month < 1 ? today.month() + 1 : month,
  }
})

const selector = createStructuredSelector({
  isTabletOrLarger: selectors.isTabletOrLarger,
  date: dateSelector,
})

const actions = {
  onSelectDate: (year, month) => push(routes.monthlySchedule({ year, month })),
}

export default connect(selector, actions)(MonthGrid)
