import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { logOut } from 'actions/viewer'
import Button from 'components/ui-elements/Button'
import Input from 'components/ui-elements/Input'
import PhotoPicker from 'components/ui-elements/PhotoPicker'
import SplitLine from 'components/ui-elements/SplitLine'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'

const ProfileSettings = ({ isOnboarding, onLogOut }) => (
  <View scrollable flexGrow={1}>
    <View padding={['large', 'mnormal']}>
      <View padding="blarge">
        <Text bold>Ваше имя:</Text>
        <View padding="bsmall" />
        <Input />
      </View>

      <View>
        <Text bold>Фотография:</Text>
        <View padding="bsmall" />
        <View padding="normal" align={isOnboarding ? 'center' : 'left'}>
          <PhotoPicker />
        </View>
      </View>

      <SplitLine />

      <View padding="tnormal" flexDirection="row">
        <Button outline onClick={onLogOut}>Выйти из аккаунта</Button>
      </View>
    </View>
  </View>
)

ProfileSettings.propTypes = {
  isOnboarding: PropTypes.bool,
}

const actions = {
  onLogOut: logOut,
}

export default connect(null, actions)(ProfileSettings)
