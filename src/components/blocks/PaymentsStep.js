import React from 'react'

import PaymentsSettings from 'components/blocks/PaymentsSettings'

const PaymentsStep = () => <PaymentsSettings />

export default PaymentsStep
