import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { COLOR_FONT_DEFAULT } from 'constants/styles'
import Icon from 'components/ui-elements/Icon'
import ListView from 'components/ui-elements/ListView'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'

const SettingsList = ({ onPush }) => (
  <ListView
    data={[
      { path: routes.profileSettings(), text: 'Профиль', icon: 'user' },
      { path: routes.scheduleSettings(), text: 'Расписание', icon: 'calendar' },
      { path: routes.additionalSettings(), text: 'Заявки и уведомления', icon: 'bell' },
      { path: routes.paymentsSettings(), text: 'Платежи', icon: 'cog' },
    ]}
    keyExtractor={x => x.path}
    renderItem={({ item }) => (
      <View
        touchable
        padding="normal"
        border="bnormal"
        flexDirection="row"
        align="center"
        onClick={() => onPush(item.path)}
      >
        <Icon
          glyph={item.icon}
          style={{ fontSize: 23, color: COLOR_FONT_DEFAULT }}
        />
        <View flexGrow={1} padding="hnormal">
          <Text>{item.text}</Text>
        </View>
        <Icon glyph="chevron-right" />
      </View>
    )}
  />
)

SettingsList.propTypes = {
  onPush: PropTypes.func.isRequired,
}

export default connect(null, { onPush: push })(SettingsList)
