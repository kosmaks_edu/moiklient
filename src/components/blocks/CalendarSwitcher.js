import React from 'react'
import PropTypes from 'prop-types'

import View from 'components/ui-elements/View'
import Button from 'components/ui-elements/Button'

const CalendarSwitcher = ({ onBack, onToday, onForward }) => (
  <View flexDirection="row" align="center">
    <Button secondary icon="chevron-left" onClick={onBack} />
    <View padding="hsmall">
      <Button secondary onClick={onToday}>
        Сегодня
      </Button>
    </View>
    <Button secondary icon="chevron-right" onClick={onForward} />
  </View>
)

CalendarSwitcher.propTypes = {
  onBack: PropTypes.func,
  onForward: PropTypes.func,
  onToday: PropTypes.func,
}

export default CalendarSwitcher
