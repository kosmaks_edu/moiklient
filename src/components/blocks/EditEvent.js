import React from 'react'

import Input from 'components/ui-elements/Input'
import Select from 'components/ui-elements/Select'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'

const EditEvent = () => (
  <View>
    <View padding="bnormal">
      <View padding="bsmall">
        <Text>Услуга: *</Text>
      </View>
      <Select
        placeholder="Выберите услугу"
        options={[
          { value: 1, label: 'Маникюор' },
          { value: 2, label: 'Педикюр' },
        ]}
      />
    </View>

    <View padding="bnormal">
      <View padding="bsmall">
        <Text>Дата и время: *</Text>
      </View>
      <View flexDirection="row" padding="bsmall">
        <View flexGrow={3} padding="rsmall">
          <Select
            placeholder="Дата"
            options={[
              { value: 1, label: '28 авг' },
              { value: 2, label: '29 авг (сегодня)' },
              { value: 3, label: '30 авг' },
            ]}
          />
        </View>
        <View flexGrow={2}>
          <Select
            placeholder="Время"
            options={[
              { value: 1, label: '15:00' },
              { value: 2, label: '17:00' },
              { value: 3, label: '20:00' },
            ]}
          />
        </View>
      </View>
      <View align="end">
        <Text size="small" color="lighten">
          Показано только свободное время
        </Text>
      </View>
    </View>

    <View padding="bnormal">
      <View padding="bsmall">
        <Text>Имя: *</Text>
      </View>
      <Input placeholder="Имя клиента" />
    </View>

    <View padding="bnormal">
      <View padding="bsmall">
        <Text>Email:</Text>
      </View>
      <Input placeholder="Электронная почта клиента" />
    </View>

    <View padding="bnormal">
      <View padding="bsmall">
        <Text>Телефон: *</Text>
      </View>
      <Input placeholder="+791......." />
    </View>

    <View padding="bnormal">
      <View padding="bsmall">
        <Text>Комментарий:</Text>
      </View>
      <Input placeholder="Ваш комментарий" />
    </View>
  </View>
)

export default EditEvent
