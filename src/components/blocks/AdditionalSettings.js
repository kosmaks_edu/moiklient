import React from 'react'

import Checkbox from 'components/ui-elements/Checkbox'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'

const AdditionalSettings = () => (
  <View flexGrow={1} padding={['large', 'mnormal']}>
    <Checkbox text={<Text bold>Отправлять SMS-уведомления клиентам</Text>} />
    <View padding={['tnormal', 'llarge']}>
      <Text>
        Вашим клиентам за два часа до сеанса будет приходить SMS с напоминанием
        о записи. Также у вас появляется возможность одобрять заявки. Стоимость
        SMS не входит в тариф.
      </Text>
    </View>
    <View padding="blarge" />
    <Checkbox text={<Text bold>Автоматически подтверждать заявки</Text>} />
    <View padding={['tnormal', 'llarge']}>
      <Text>
        Включите, если Вы хотите, чтобы клиенты автоматически добавлялись в
        календарь без Вашего подтверждения.
      </Text>
    </View>
  </View>
)

export default AdditionalSettings
