import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { closeSidebar } from 'actions/sidebar'
import AccountWidget from 'components/blocks/AccountWidget'
import SidebarItem from 'components/ui-elements/SidebarItem'
import SplitLine from 'components/ui-elements/SplitLine'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

const Sidebar = ({ pathname, onPush }) => (
  <View flexGrow={1}>
    <SidebarItem
      icon="inbox"
      text="Заявки"
      count={2}
      isActive={pathname === routes.requests()}
      onClick={() => onPush(routes.requests())}
    />
    <SidebarItem
      icon="calendar"
      text="Расписание"
      isActive={
        pathname === routes.root() ||
        pathname === routes.monthlySchedule() ||
        pathname === routes.dailySchedule()
      }
      onClick={() => onPush(routes.monthlySchedule())}
    />
    <SidebarItem
      icon="th-list"
      text="Услуги"
      isActive={pathname === routes.services()}
      onClick={() => onPush(routes.services())}
    />
    <SidebarItem
      icon="cog"
      text="Настройки"
      isActive={
        pathname === routes.settings() ||
        pathname === routes.profileSettings() ||
        pathname === routes.scheduleSettings() ||
        pathname === routes.additionalSettings() ||
        pathname === routes.paymentsSettings()
      }
      onClick={() => onPush(routes.settings())}
    />

    <View flexGrow={1} />
    <SplitLine />
    <AccountWidget />
  </View>
)

Sidebar.propTypes = {
  pathname: PropTypes.string.isRequired,
  onPush: PropTypes.func.isRequired,
}

const selector = createStructuredSelector({
  pathname: selectors.getPathname,
})

const actions = {
  onPush: route => dispatch => {
    dispatch(push(route))
    dispatch(closeSidebar())
  },
}

export default connect(selector, actions)(Sidebar)
