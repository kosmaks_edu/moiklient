import React from 'react'

import ProfileSettings from 'components/blocks/ProfileSettings'
import View from 'components/ui-elements/View'

const ProfileStep = () => (
  <View flexGrow={1} flexDirection="row" justify="center">
    <View flexGrow={1} style={{ maxWidth: 387 }}>
      <ProfileSettings isOnboarding />
    </View>
  </View>
)

export default ProfileStep
