import PropTypes from 'prop-types'
import React from 'react'
import map from 'lodash/map'
import range from 'lodash/range'

import { durationToStr } from 'helpers/time'
import Select from 'components/ui-elements/Select'

const DurationPicker = ({ onChange, value }) => (
  <Select
    onChange={onChange}
    value={`${value}`}
    options={map(range(15, 7 * 60, 15), duration => ({
      value: `${duration}`,
      label: durationToStr(duration),
    }))}
  />
)

DurationPicker.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
}

export default DurationPicker
