import { connect } from 'react-redux'
import { createSelector, createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'

import moment from 'moment'

import selectors from 'store/reducers/selectors'

import routes from 'helpers/routes'

import WeekGrid from './WeekGrid'

const dateSelector = createSelector(selectors.getQuery, query => {
  const date = parseFloat(query.date)

  if (isNaN(date)) {
    return moment()
      .startOf('week')
      .format('YYYY-MM-DD')
  }

  return date
})

const selector = createStructuredSelector({
  isTabletOrLarger: selectors.isTabletOrLarger,
  date: dateSelector,
})

const actions = {
  onSelectDate: date => push(routes.weeklySchedule(date)),
}

export default connect(selector, actions)(WeekGrid)
