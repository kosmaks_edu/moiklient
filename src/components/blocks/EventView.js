import React from 'react'

import Icon from 'components/ui-elements/Icon'
import Link from 'components/ui-elements/Link'
import SplitLine from 'components/ui-elements/SplitLine'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'

const EventView = () => (
  <View>
    <Text size="medium">
      Запись на{' '}
      <Text size="medium" bold>
        Маникюр
      </Text>{' '}
      (600 р.)
    </Text>

    <SplitLine />

    <View padding="bnormal" flexDirection="row" align="center">
      <View padding="rnormal" align="center" style={{ width: 30 }}>
        <Icon glyph="clock-o" size="lg" />
      </View>

      <View padding="rnormal">
        <Text bold>09:30</Text>
      </View>
      <View padding="rnormal">
        <Text color="lighten">|</Text>
      </View>
      <Text>28 авг. 2017 (понедельник)</Text>
    </View>

    <View padding="bnormal" flexDirection="row" align="center">
      <View padding="rnormal" align="center" style={{ width: 30 }}>
        <Icon glyph="user" size="lg" />
      </View>
      <Text>Светлана</Text>
    </View>

    <View padding="bnormal" flexDirection="row" align="center">
      <View padding="rnormal" align="center" style={{ width: 30 }}>
        <Icon glyph="envelope" size="lg" />
      </View>
      <Link>kstmaks@gmail.com</Link>
    </View>

    <View flexDirection="row" align="center">
      <View padding="rnormal" align="center" style={{ width: 30 }}>
        <Icon glyph="phone" size="lg" />
      </View>
      <Link>+7 (912) 891-24-25</Link>
    </View>

    <SplitLine />

    <View padding="bnormal" flexDirection="row" align="center">
      <View padding="rnormal" align="center" style={{ width: 30 }}>
        <Icon glyph="commenting" size="lg" />
      </View>
      <Text bold>Комментарий от клиента:</Text>
    </View>

    <View flexDirection="row" align="center">
      <View padding="rnormal" style={{ width: 30 }} />
      <Text>Я кароч опаздаю малян, сорян</Text>
    </View>
  </View>
)

export default EventView
