import React from 'react'

import AdditionalSettings from 'components/blocks/AdditionalSettings'
import View from 'components/ui-elements/View'

const AdditionalStep = () => (
  <View flexGrow={1} scrollable>
    <View flexGrow={1} flexDirection="row" justify="center">
      <View flexGrow={1} style={{ maxWidth: 400 }}>
        <AdditionalSettings isOnboarding />
      </View>
    </View>
  </View>
)

export default AdditionalStep
