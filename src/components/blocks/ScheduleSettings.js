import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React from 'react'
import chunk from 'lodash/chunk'
import filter from 'lodash/filter'
import map from 'lodash/map'
import range from 'lodash/range'

import { WEEK_DAYS, WEEK_DAYS_SHORT } from 'constants/calendar'
import { timeToStr } from 'helpers/time'
import Checkbox from 'components/ui-elements/Checkbox'
import Form from 'components/blocks/Form'
import Radio from 'components/ui-elements/Radio'
import Select from 'components/ui-elements/Select'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import selectors from 'store/reducers/selectors'

const hours = map(range(0, 24 * 60, 15), value => ({
  value,
  label: timeToStr(value),
}))

const RangePicker = ({ leftTitle, rightTitle, isTabletOrLarger }) => (
  <View flexDirection="row">
    <View
      flexGrow={isTabletOrLarger ? 0 : 1}
      flexDirection={isTabletOrLarger ? 'row' : 'column'}
      align={isTabletOrLarger ? 'center' : 'stretch'}
    >
      <Text>{leftTitle}</Text>
      <View padding="small" />
      <Select header={leftTitle} options={hours} />
    </View>
    <View padding="small" />
    <View
      flexGrow={isTabletOrLarger ? 0 : 1}
      flexDirection={isTabletOrLarger ? 'row' : 'column'}
      align={isTabletOrLarger ? 'center' : 'stretch'}
    >
      <Text>{rightTitle}</Text>
      <View padding="small" />
      <Select header={rightTitle} options={hours} />
    </View>
  </View>
)

RangePicker.propTypes = {
  leftTitle: PropTypes.string.isRequired,
  rightTitle: PropTypes.string.isRequired,
  isTabletOrLarger: PropTypes.bool.isRequired,
}

class ScheduleSettings extends Form {
  getInitialFieldsFromProps() {
    return {
      sameTimeForWholeWeek: true,
      lunchBreak: true,
    }
  }

  renderWorkDays() {
    const { isTabletOrLarger } = this.props

    return (
      <View frame padding="normal">
        <Text bold>Рабочие дни:</Text>
        <View padding="bnormal" />
        {map(
          chunk(range(0, 8), isTabletOrLarger ? 4 : 2),
          (daysOfWeek, i, chunks) => (
            <View
              key={i}
              flexDirection="row"
              padding={i === chunks.length - 1 ? [] : 'bnormal'}
            >
              {map(daysOfWeek, dayOfWeek => (
                <View key={dayOfWeek} flexGrow={1}>
                  {dayOfWeek < 7 && (
                    <Checkbox
                      text={WEEK_DAYS[dayOfWeek]}
                      {...this.getInputProps(`workDay${dayOfWeek}`)}
                    />
                  )}
                </View>
              ))}
            </View>
          )
        )}
      </View>
    )
  }

  renderWorkHours() {
    const { isTabletOrLarger } = this.props
    const sameTimeForWholeWeek = this.getField('sameTimeForWholeWeek')
    const weekDays = filter(range(0, 7), x => this.getField(`workDay${x}`))
    const showPlaceholder = !sameTimeForWholeWeek && weekDays.length === 0

    return (
      <View frame padding="normal">
        <Text bold>Рабочие часы:</Text>
        <View padding="bnormal" />
        <Radio
          text="Одинаково для всех дней недели"
          {...this.getRadioProps('sameTimeForWholeWeek', true)}
        />
        <View padding="bnormal" />
        <Radio
          text="Указать разные рабочие часы для каждого дня недели"
          {...this.getRadioProps('sameTimeForWholeWeek', false)}
        />

        <View padding="blarge" />

        {showPlaceholder && (
          <View
            padding="vnormal"
            align="center"
            justify="center"
            background="light"
          >
            <Text>Выберите хотя бы один рабочий день</Text>
          </View>
        )}

        {sameTimeForWholeWeek && (
          <RangePicker
            leftTitle="Начало дня:"
            rightTitle="Конец дня:"
            isTabletOrLarger={isTabletOrLarger}
          />
        )}

        {!sameTimeForWholeWeek &&
          map(weekDays, (dayOfWeek, i, allDays) => (
            <View flexDirection="row" key={dayOfWeek}>
              <View
                style={{ width: 50 }}
                justify="center"
                border={
                  i === allDays.length - 1 ? (
                    ['rnormal']
                  ) : (
                    ['rnormal', 'bnormal']
                  )
                }
              >
                <Text bold>{WEEK_DAYS_SHORT[dayOfWeek]}</Text>
              </View>
              <View
                padding={['lnormal', 'vnormal']}
                flexGrow={1}
                border={i === allDays.length - 1 ? [] : 'bnormal'}
              >
                <RangePicker
                  leftTitle="Начало дня:"
                  rightTitle="Конец дня:"
                  isTabletOrLarger={isTabletOrLarger}
                />
              </View>
            </View>
          ))}
      </View>
    )
  }

  renderLunchBreak() {
    const { isTabletOrLarger } = this.props
    const lunchBreak = this.getField('lunchBreak')

    return (
      <View frame padding="normal">
        <Text bold>Обед:</Text>
        <View padding="bnormal" />
        <View flexDirection="row" alignItems="center">
          <Radio text="С обедом" {...this.getRadioProps('lunchBreak', true)} />
          <View padding="rnormal" />
          <Radio
            text="Без обеда"
            {...this.getRadioProps('lunchBreak', false)}
          />
        </View>

        {lunchBreak && <View padding="blarge" />}
        {lunchBreak && (
          <RangePicker
            leftTitle="Обед с:"
            rightTitle="Обед по:"
            isTabletOrLarger={isTabletOrLarger}
          />
        )}
      </View>
    )
  }

  render() {
    const { isOnboarding } = this.props

    return (
      <View flexGrow={1} scrollable>
        <View
          padding={['large', 'mnormal']}
          flexDirection="row"
          justify="center"
        >
          <View flexGrow={1} style={isOnboarding ? { maxWidth: 600 } : {}}>
            {this.renderWorkDays()}
            <View padding="blarge" />
            {this.renderWorkHours()}
            <View padding="blarge" />
            {this.renderLunchBreak()}
          </View>
        </View>
      </View>
    )
  }
}

ScheduleSettings.propTypes = {
  isTabletOrLarger: PropTypes.bool.isRequired,
  isOnboarding: PropTypes.bool,
}

const selector = createStructuredSelector({
  isTabletOrLarger: selectors.isTabletOrLarger,
})

export default connect(selector)(ScheduleSettings)
