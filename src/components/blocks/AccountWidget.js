import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React from 'react'

import { closeSidebar } from 'actions/sidebar'
import Image from 'components/ui-elements/Image'
import Link from 'components/ui-elements/Link'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

const AccountWidget = ({ profile, onCloseSidebar }) => (
  <View flexDirection="row" align="center">
    <Image
      src="https://s3.amazonaws.com/37assets/svn/1065-IMG_2529.jpg"
      src2x="https://s3.amazonaws.com/37assets/svn/1065-IMG_2529.jpg"
      circle
      width={56}
      height={56}
    />

    <View padding="hnormal">
      <View padding="vsmall">
        <Text>{profile.fullName}</Text>
      </View>

      <Link to={routes.profileSettings()} onClick={onCloseSidebar}>
        Настройки аккаунта
      </Link>
    </View>
  </View>
)

const selector = createStructuredSelector({
  profile: selectors.getProfile,
})

AccountWidget.propTypes = {
  profile: PropTypes.object.isRequired,
}

const actions = {
  onCloseSidebar: closeSidebar,
}

export default connect(selector, actions)(AccountWidget)
