import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'
import compact from 'lodash/compact'

import Button from 'components/ui-elements/Button'
import Services from 'components/blocks/Services'
import View from 'components/ui-elements/View'
import selectors from 'store/reducers/selectors'

class ServicesStep extends Component {
  constructor(props) {
    super(props)

    this.showEditor = bind(this.showEditor, this)
    this.hideEditor = bind(this.hideEditor, this)

    this.state = { showEditor: false }
  }

  showEditor() {
    this.setState({ showEditor: true })
  }

  hideEditor() {
    this.setState({ showEditor: false })
  }

  render() {
    const { showEditor } = this.state
    const { services, isTabletOrLarger } = this.props

    return (
      <View
        flexGrow={1}
        padding={compact(['blarge', 'bmnormal', isTabletOrLarger && 'hlarge'])}
      >
        <View flexGrow={1}>
          <Services
            showEditor={showEditor}
            services={services}
            onAddSuccess={this.hideEditor}
            onAddCancel={this.hideEditor}
          />
        </View>

        <View padding="tnormal" align="center" justify="center">
          <Button icon="plus" outline onClick={this.showEditor}>
            Добавить услугу
          </Button>
        </View>
      </View>
    )
  }
}

ServicesStep.propTypes = {
  services: PropTypes.array.isRequired,
  isTabletOrLarger: PropTypes.bool.isRequired,
}

const selector = createStructuredSelector({
  services: selectors.getSortedServices,
  isTabletOrLarger: selectors.isTabletOrLarger,
})

export default connect(selector)(ServicesStep)
