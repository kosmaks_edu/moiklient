import { connect } from 'react-redux'
import { createSelector, createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'
import moment from 'moment'

import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

import { events } from './mocks'
import DayView from './DayView'

const dateSelector = createSelector(selectors.getQuery, query => {
  const month = parseFloat(query.month)
  const year = parseFloat(query.year)
  const day = parseFloat(query.day)
  const today = moment()

  const finalYear = isNaN(year) ? today.year() : year
  const finalMonth =
    isNaN(month) || month > 12 || month < 1 ? today.month() + 1 : month
  const selectedMom = moment()
    .year(finalYear)
    .month(finalMonth - 1)
  const lastDay = selectedMom.endOf('month').date()

  return {
    year: finalYear,
    month: finalMonth,
    day: isNaN(day) || day > lastDay || day < 1 ? today.date() : day,
  }
})

const selector = createStructuredSelector({
  timeRange: () => [480, 1080],
  events: () => events,
  breaks: () => [[720, 780]],
  date: dateSelector,
  isTabletOrLarger: selectors.isTabletOrLarger,
})

const actions = {
  onEventView: event =>
    event.isPending
      ? push(routes.eventRequestPath())
      : push(routes.eventViewPath()),

  onSelectDate: mom =>
    push(
      routes.dailySchedule({
        year: mom.year(),
        month: mom.month() + 1,
        day: mom.date(),
      })
    ),
}

export default connect(selector, actions)(DayView)
