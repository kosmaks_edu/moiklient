export const events = [
  {
    id: 0,
    time: 490,
    duration: 60,
    isPending: false,
    client: {
      name: 'Светлана',
    },
    service: {
      name: 'Маникюр',
    },
  },
  {
    id: 1,
    time: 600,
    duration: 45,
    isPending: false,
    client: {
      name: 'Ирина',
    },
    service: {
      name: 'Педикюр',
    },
  },
  {
    id: 2,
    time: 650,
    duration: 30,
    isPending: true,
    client: {
      phone: '+7 (912) 891-24-25',
      name: 'Ирина',
    },
    service: {
      name: 'Педикюр',
    },
  },
  {
    id: 3,
    time: 800,
    duration: 15,
    isPending: false,
    client: {
      name: 'Ирина',
    },
    service: {
      name: 'Педикюр',
    },
  },
  {
    id: 4,
    time: 850,
    duration: 120,
    isPending: true,
    client: {
      phone: '+7 (912) 891-24-25',
      email: 'kstmaks@gmail.com',
      name: 'Ирина',
    },
    service: {
      name: 'Маникюр',
    },
  },
]
