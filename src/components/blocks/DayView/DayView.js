import PropTypes from 'prop-types'
import React, { Component } from 'react'
import moment from 'moment'

import { POS_MONTHS, WEEK_DAYS } from 'constants/calendar'
import Button from 'components/ui-elements/Button'
import CalendarSwitcher from 'components/blocks/CalendarSwitcher'
import DayGrid from 'components/ui-elements/DayGrid'
import EditEvent from 'components/blocks/EditEvent'
import EventView from 'components/blocks/EventView'
import SplitLine from 'components/ui-elements/SplitLine'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'

class DayView extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = { isEditing: false }
  }

  render() {
    const {
      date,
      timeRange,
      events,
      breaks,
      isTabletOrLarger,
      onEventView,
      onSelectDate,
    } = this.props

    const { isEditing } = this.state

    const mom = moment()
      .year(date.year)
      .month(date.month - 1)
      .date(date.day)

    const today = moment()
    const isToday = mom.format('YYYY-MM-DD') === today.format('YYYY-MM-DD')

    return (
      <View flexGrow={1}>
        <View
          flexDirection="row"
          border="bnormal"
          padding={['large', 'mnormal']}
          align="center"
        >
          <View flexGrow={1}>
            <Text size={['large', 'mmedium']}>
              {date.day} {POS_MONTHS[date.month - 1]}
              <Text size={['large', 'mmedium']} light>
                {' '}
                {date.year}
              </Text>
            </Text>

            <View padding="tsmall">
              <Text size="small">
                {WEEK_DAYS[mom.day()]}
                {isToday && (
                  <Text bold size="small">
                    {' '}
                    | Сегодня
                  </Text>
                )}
              </Text>
            </View>
          </View>

          <CalendarSwitcher
            onBack={() => onSelectDate(mom.add(-1, 'day'))}
            onToday={() => onSelectDate(today)}
            onForward={() => onSelectDate(mom.add(1, 'day'))}
          />
        </View>

        <View flexDirection="row" flexGrow={1}>
          <DayGrid
            timeRange={timeRange}
            events={events}
            breaks={breaks}
            onEventView={onEventView}
          />

          {isTabletOrLarger &&
            (isEditing ? (
              <View
                flexGrow={1}
                border="lnormal"
                background="light"
                padding={['vlarge', 'hnormal']}
              >
                <EditEvent />

                <View flexDirection="row" justify="end">
                  <View padding="rnormal">
                    <Button
                      outline
                      colorless
                      onClick={() => this.setState({ isEditing: true })}
                    >
                      Отменить
                    </Button>
                  </View>
                  <Button>
                    Сохранить
                  </Button>
                </View>
              </View>
            ) : (
              <View
                flexGrow={1}
                border="lnormal"
                background="light"
                padding={['vlarge', 'hnormal']}
              >
                <EventView />
                <SplitLine />

                <View flexDirection="row">
                  <View padding="rnormal">
                    <Button
                      icon="pencil"
                      outline
                      onClick={() => this.setState({ isEditing: true })}
                    >
                      Изменить
                    </Button>
                  </View>
                  <Button icon="times" colorless outline>
                    Удалить
                  </Button>
                </View>
              </View>
            ))}
        </View>
      </View>
    )
  }
}

DayView.propTypes = {
  date: PropTypes.object.isRequired,
  timeRange: PropTypes.array.isRequired,
  events: PropTypes.array.isRequired,
  breaks: PropTypes.array.isRequired,
  isTabletOrLarger: PropTypes.bool.isRequired,
  onEventView: PropTypes.func.isRequired,
  onSelectDate: PropTypes.func.isRequired,
}

export default DayView
