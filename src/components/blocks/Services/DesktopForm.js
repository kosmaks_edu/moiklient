import PropTypes from 'prop-types'
import React from 'react'
import bind from 'lodash/bind'

import { durationToStr } from 'helpers/time'
import { formatMoney } from 'helpers/price'
import Button from 'components/ui-elements/Button'
import DurationPicker from 'components/blocks/DurationPicker'
import Form from 'components/blocks/Form'
import Icon from 'components/ui-elements/Icon'
import Input from 'components/ui-elements/Input'
import Link from 'components/ui-elements/Link'

class DesktopForm extends Form {
  constructor(props) {
    super(props)

    this.onCancel = bind(this.onCancel, this)

    const service = props.service || {}

    this.state = {
      isEditing: false,
      fields: {
        title: service.title || '',
        duration: service.duration || 60,
        price: service.price ? `${service.price / 100}` : null,
      },
    }
  }

  componentDidUpdate(props) {
    if (this.props.service !== props.service) {
      this.resetFormFromProps(props)
    }
  }

  onCancel() {
    const { onAddCancel } = this.props
    this.resetFormFromProps(this.props)

    if (onAddCancel) {
      onAddCancel()
    } else {
      this.setState({ isEditing: false })
    }
  }

  submit() {
    const {
      service,
      onAddSuccess,
      onCreateService,
      onUpdateService,
    } = this.props
    const { fields } = this.state

    const method = service
      ? data => onUpdateService(service.id, data)
      : data => onCreateService(data)

    return method({
      ...fields,
      price: Math.round(parseFloat(fields.price) * 100),
    }).do(
      () =>
        onAddSuccess ? onAddSuccess() : this.setState({ isEditing: false })
    )
  }

  resetFormFromProps(props) {
    const service = props.service || {}

    this.setState({
      title: service.title || '',
      duration: service.duration || 60,
      price: service.price ? `${service.price / 100}` : null,
    })
  }

  renderEditor() {
    const { isSaving } = this.state

    return (
      <tr className="Services-editor">
        <td>
          <Input {...this.getInputProps('title')} />
        </td>
        <td className="Services-price">
          <Input {...this.getInputProps('price')} />
        </td>
        <td className="Services-duration">
          <DurationPicker {...this.getInputProps('duration')} />
        </td>
        <td className="Services-actions">
          <div className="Services-buttonActions">
            <Button
              icon="check"
              loading={isSaving}
              onClick={this.handleSubmit}
            />
            <Button
              disabled={isSaving}
              outline
              colorless
              icon="times"
              onClick={this.onCancel}
            />
          </div>
        </td>
      </tr>
    )
  }

  renderView() {
    const { service, onDeleteService } = this.props

    return (
      <tr>
        <td>{service.title}</td>
        <td className="Services-price">
          {formatMoney(service.price, service.currency)}
        </td>
        <td className="Services-duration">{durationToStr(service.duration)}</td>
        <td className="Services-actions">
          <Link
            className="Services-action"
            onClick={() => this.setState({ isEditing: true })}
          >
            <Icon glyph="pencil" /> Изменить
          </Link>
          <Link
            className="Services-action"
            onClick={() => onDeleteService(service.id)}
          >
            <Icon glyph="times" /> Удалить
          </Link>
        </td>
      </tr>
    )
  }

  render() {
    return this.props.isEditing || this.state.isEditing
      ? this.renderEditor()
      : this.renderView()
  }
}

DesktopForm.propTypes = {
  service: PropTypes.object,
  isEditing: PropTypes.bool,
  onAddSuccess: PropTypes.func,
  onAddCancel: PropTypes.func,
  onCreateService: PropTypes.func.isRequired,
  onUpdateService: PropTypes.func.isRequired,
  onDeleteService: PropTypes.func.isRequired,
}

export default DesktopForm
