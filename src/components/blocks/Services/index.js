import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React from 'react'

import { createService, deleteService, updateService } from 'actions/services'
import selectors from 'store/reducers/selectors'

import Desktop from './Desktop'
import Mobile from './Mobile'

const Services = props =>
  props.isDesktop ? <Desktop {...props} /> : <Mobile {...props} />

Services.propTypes = {
  isDesktop: PropTypes.bool.isRequired,
}

const selector = createStructuredSelector({
  isDesktop: selectors.isDesktop,
})

const actions = {
  onCreateService: createService,
  onUpdateService: updateService,
  onDeleteService: deleteService,
}

export default connect(selector, actions)(Services)
