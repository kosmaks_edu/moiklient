import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'

import { durationToStr } from 'helpers/time'
import { formatMoney } from 'helpers/price'
import Icon from 'components/ui-elements/Icon'
import ListView from 'components/ui-elements/ListView'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'

class Mobile extends Component {
  constructor(props, context) {
    super(props, context)

    this.renderItem = bind(this.renderItem, this)
  }

  renderItem({ item }) {
    const { onPush } = this.props

    return (
      <View
        border="bnormal"
        padding="normal"
        flexDirection="row"
        align="center"
        touchable
        onClick={() => onPush(routes.editService(item.id))}
      >
        <View flexGrow={1}>
          <Text size="medium">{item.title}</Text>
          <View padding="bsmall" />
          <Text>
            {formatMoney(item.price, item.currency)}{' '}
            <Text color="lighten">|</Text> {durationToStr(item.duration)}
          </Text>
        </View>
        <Icon glyph="chevron-right" />
      </View>
    )
  }

  render() {
    const { services } = this.props

    return (
      <View flexGrow={1}>
        <ListView
          data={services}
          renderItem={this.renderItem}
          keyExtractor={x => x.id}
        />
      </View>
    )
  }
}

Mobile.propTypes = {
  services: PropTypes.array.isRequired,
  onPush: PropTypes.func.isRequired,
}

export default connect(null, { onPush: push })(Mobile)
