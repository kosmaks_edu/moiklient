import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'
import cx from 'classnames'
import map from 'lodash/map'

import DesktopForm from 'components/blocks/Services/DesktopForm'

const Services = ({
  services,
  showEditor,
  onAddCancel,
  onAddSuccess,
  onUpdateService,
  onCreateService,
  onDeleteService,
}) => (
  <table
    className={cx('Services', {
      'Services--showEditor': showEditor,
    })}
    cellSpacing={0}
  >
    <thead>
      <tr>
        <th>Название</th>
        <th>Цена</th>
        <th>Время сеанса</th>
        <th>Действия</th>
      </tr>
    </thead>
    <tbody>
      {showEditor && (
        <DesktopForm
          isEditing
          onAddCancel={onAddCancel}
          onAddSuccess={onAddSuccess}
          onCreateService={onCreateService}
          onUpdateService={onUpdateService}
          onDeleteService={onDeleteService}
        />
      )}
      {map(services, service => (
        <DesktopForm
          service={service}
          key={service.id}
          onCreateService={onCreateService}
          onUpdateService={onUpdateService}
          onDeleteService={onDeleteService}
        />
      ))}
    </tbody>
  </table>
)

Services.propTypes = {
  services: PropTypes.array.isRequired,
  showEditor: PropTypes.bool,
  onAddSuccess: PropTypes.func,
  onAddCancel: PropTypes.func,
  onCreateService: PropTypes.func.isRequired,
  onUpdateService: PropTypes.func.isRequired,
  onDeleteService: PropTypes.func.isRequired,
}

export default Services
