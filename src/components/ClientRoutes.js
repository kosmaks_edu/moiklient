import { Route, Switch } from 'react-router-dom'
import React from 'react'

import ClientRequest from 'components/pages/ClientRequest'
import routes from 'helpers/routes'

const ClientRoutes = () => (
  <Switch>
    <Route exact path={routes.clientRequest()} component={ClientRequest} />
  </Switch>
)

export default ClientRoutes
