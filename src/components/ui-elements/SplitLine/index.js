import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'

const SplitLine = ({ text }) => (
  <div className="SplitLine">
    {text && <div className="SplitLine-text">{text}</div>}
  </div>
)

SplitLine.propTypes = {
  text: PropTypes.string,
}

export default SplitLine
