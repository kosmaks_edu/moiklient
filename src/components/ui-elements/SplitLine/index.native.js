import { StyleSheet, View as RNView } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'

import { COLOR_BORDER, COLOR_WHITE } from 'constants/styles'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {
    borderTopWidth: 1,
    borderTopColor: COLOR_BORDER,
    marginTop: 15,
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    width: 50,
    backgroundColor: COLOR_WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -10,
  },
})

const SplitLine = ({ text }) => (
  <RNView style={styles.main}>
    {text && (
      <RNView style={styles.text}>
        <Text>{text}</Text>
      </RNView>
    )}
  </RNView>
)

SplitLine.propTypes = {
  text: PropTypes.string,
}

export default SplitLine
