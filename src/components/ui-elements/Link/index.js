import './styles.css'

import { Link as LinkRR } from 'react-router-dom'
import PropTypes from 'prop-types'
import React from 'react'
import cx from 'classnames'

const Link = ({ className, style, ...props }) => {
  const refProps = {
    ...props,
  }

  if (props.onClick) {
    refProps.onClick = props.onClick
  } else if (props.href) {
    refProps.href = props.href
    refProps.target = '_blank'
  }

  return (
    <span style={style} className={cx('Link', className)}>
      {props.to ? (
        <LinkRR {...refProps} />
      ) : (
        <a {...refProps} rel="noopener noreferrer" />
      )}
    </span>
  )
}

Link.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  href: PropTypes.string,
  to: PropTypes.oneOfType(PropTypes.object, PropTypes.string),
  onClick: PropTypes.func,
}

export default Link
