import { Linking, StyleSheet, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { COLOR_PRIMARY } from 'constants/styles'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {
    padding: 10,
    margin: -10,
  },

  text: {
    color: COLOR_PRIMARY,
  },
})

const Link = ({ children, to, href, onClick, onPush, ...props }) => {
  let onPress

  if (to) {
    onPress = () => onPush(to)
  } else if (href) {
    onPress = () => Linking.openUrl(href)
  } else if (onClick) {
    onPress = onClick
  }

  return (
    <TouchableOpacity style={styles.main} onPress={onPress}>
      <Text {...props} style={styles.text}>
        {children}
      </Text>
    </TouchableOpacity>
  )
}

Link.propTypes = {
  to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  href: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
  onPush: PropTypes.func.isRequired,
}

const actions = {
  onPush: push,
}

export default connect(null, actions)(Link)
