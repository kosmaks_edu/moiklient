import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'

import Icon from 'components/ui-elements/Icon'

const MenuButton = ({ count, onClick, onOpenDrawer }) => (
  <div className="MenuButton" onClick={onClick || onOpenDrawer}>
    <Icon glyph="bars" size="lg" />
    {count && (
      <div className="MenuButton-count">
        {count}
      </div>
    )}
  </div>
)

MenuButton.propTypes = {
  count: PropTypes.number.isRequired,
  onClick: PropTypes.func,
  onOpenDrawer: PropTypes.func,
}

export default MenuButton
