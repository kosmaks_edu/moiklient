import { View as RNView, StyleSheet, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'

import { COLOR_WHITE } from 'constants/styles'
import Icon from 'components/ui-elements/Icon'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {
    position: 'relative',
    width: 40,
    height: 40,
    marginLeft: -5,
    marginRight: -5,
    alignItems: 'center',
    justifyContent: 'center',
  },

  count: {
    position: 'absolute',
    top: 0,
    right: 0,
    minWidth: 18,
    height: 18,
    backgroundColor: '#FF2626',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 9,
  },

  countText: {
    color: COLOR_WHITE,
    fontSize: 12,
  },
})

const MenuButton = ({ count, onClick, onOpenDrawer }) => (
  <TouchableOpacity style={styles.main} onPress={onClick || onOpenDrawer}>
    <Icon glyph="bars" style={{ fontSize: 20, color: '#777' }} />
    {count && (
      <RNView style={styles.count}>
        <Text style={styles.countText}>{count}</Text>
      </RNView>
    )}
  </TouchableOpacity>
)

MenuButton.propTypes = {
  count: PropTypes.number.isRequired,
  onClick: PropTypes.func,
  onOpenDrawer: PropTypes.func,
}

export default MenuButton
