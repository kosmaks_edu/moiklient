import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { openSidebar } from 'actions/sidebar'

import MenuButton from './MenuButton'

const selector = createStructuredSelector({
  count: () => 2,
})

const actions = {
  onOpenDrawer: openSidebar,
}

export default connect(selector, actions)(MenuButton)
