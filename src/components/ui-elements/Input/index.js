import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'
import cx from 'classnames'

import Icon from 'components/ui-elements/Icon'

const Input = ({ type, placeholder, multiline, value, icon, onChange }) => {
  const Comp = multiline ? 'textarea' : 'input'

  return (
    <div
      className={cx('Input', {
        'is-withIcon': !!icon,
      })}
    >
      {icon && (
        <div className="Input-icon">
          <Icon glyph={icon} />
        </div>
      )}
      <Comp
        type={type}
        value={value || ''}
        placeholder={placeholder}
        onChange={e => onChange && onChange(e.target.value)}
      />
    </div>
  )
}

Input.propTypes = {
  placeholder: PropTypes.string,
  icon: PropTypes.string,
  type: PropTypes.string.isRequired,
  value: PropTypes.string,
  multiline: PropTypes.bool,
  onChange: PropTypes.func,
}

Input.defaultProps = {
  type: 'text',
}

export default Input
