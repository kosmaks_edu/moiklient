import { View as RNView, StyleSheet, TextInput } from 'react-native'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'
import compact from 'lodash/compact'

import { BORDER_RADIUS, COLOR_BORDER, COLOR_PRIMARY } from 'constants/styles'

const styles = StyleSheet.create({
  main: {
    borderWidth: 1,
    borderRadius: BORDER_RADIUS,
    borderColor: COLOR_BORDER,
    height: 48,
  },

  main__focused: {
    borderColor: COLOR_PRIMARY,
  },

  input: {
    flex: 1,
    lineHeight: 50,
    height: 48,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 0,
    paddingTop: 0,
    margin: 0,
    borderWidth: 0,
    fontSize: 16,
  },

  input__multiline: {
    marginTop: 10,
    marginBottom: 10,
    height: 77,
    fontSize: 16,
  },
})

class Input extends Component {
  constructor(props, context) {
    super(props, context)

    this.onFocus = bind(this.onFocus, this)
    this.onBlur = bind(this.onBlur, this)

    this.state = {
      isFocused: false,
    }
  }

  onFocus() {
    this.setState({ isFocused: true })
  }

  onBlur() {
    this.setState({ isFocused: false })
  }

  render() {
    const { type, placeholder, multiline, value, onChange } = this.props
    const { isFocused } = this.state

    return (
      <RNView style={compact([styles.main, isFocused && styles.main__focused])}>
        <TextInput
          secureTextEntry={type === 'password'}
          keyboardType={{
            email: 'email-address',
          }[type] || 'default'}
          style={compact([styles.input, multiline && styles.input__multiline])}
          multiline={multiline}
          placeholder={placeholder}
          value={value}
          onChangeText={onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
      </RNView>
    )
  }
}

Input.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  type: PropTypes.string,
  multiline: PropTypes.bool,
  onChange: PropTypes.func,
}

export default Input
