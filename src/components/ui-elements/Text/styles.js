import { StyleSheet } from 'react-native'

import {
  COLOR_FONT_DEFAULT,
  COLOR_FONT_LIGHTEN,
  COLOR_SECONDARY,
} from 'constants/styles'

const common = {
  main: {},

  sizesmall: { fontSize: 12 },
  sizenormal: { fontSize: 14 },
  sizemedium: { fontSize: 18 },
  sizelarge: { fontSize: 24 },

  bold: { fontWeight: 'bold' },
  light: { fontWeight: '300' },

  colordefault: { color: COLOR_FONT_DEFAULT },
  colorlighten: { color: COLOR_FONT_LIGHTEN },
  colorsecondary: { color: COLOR_SECONDARY },

  alignleft: { textAlign: 'left' },
  aligncenter: { textAlign: 'center' },
  alignright: { textAlign: 'right' },
}

const mobile = {
  sizemsmall: { fontSize: 12 },
  sizemnormal: { fontSize: 14 },
  sizemmedium: { fontSize: 18 },
  sizemlarge: { fontSize: 24 },

  colormdefault: { color: COLOR_FONT_DEFAULT },
  colormlighten: { color: COLOR_FONT_LIGHTEN },
  colormsecondary: { color: COLOR_SECONDARY },

  alignmleft: { textAlign: 'left' },
  alignmcenter: { textAlign: 'center' },
  alignmright: { textAlign: 'right' },
}

export const desktopStyles = StyleSheet.create(common)

export const mobileStyles = StyleSheet.create({ ...common, ...mobile })
