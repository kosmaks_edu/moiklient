import React from 'react'
import PropTypes from 'prop-types'

import cx from 'classnames'

import { formatModifiers } from 'helpers/components'

import './styles.css'

const Text = ({ children, color, align, size, bold, nowrap, light }) => (
  <span
    className={cx(
      'Text',
      formatModifiers('Text--size', size),
      formatModifiers('Text--color', color),
      formatModifiers('Text--align', align),
      {
        'Text--bold': bold,
        'Text--light': light,
        'Text--nowrap': nowrap,
      }
    )}
  >
    {children}
  </span>
)

Text.propTypes = {
  children: PropTypes.node,
  align: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  color: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  size: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]).isRequired,
  bold: PropTypes.bool,
  nowrap: PropTypes.bool,
  light: PropTypes.bool,
}

Text.defaultProps = {
  size: 'normal',
}

export default Text
