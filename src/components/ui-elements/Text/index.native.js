import React, { PureComponent } from 'react'
import { Text as RNText } from 'react-native'
import PropTypes from 'prop-types'

import { formatGetters } from 'helpers/components'
import { deviceSelect } from 'helpers/device'

import { mobileStyles, desktopStyles } from './styles'

class Text extends PureComponent {
  render() {
    const styles = deviceSelect(mobileStyles, desktopStyles)
    const { children, size, align, bold, color, nowrap, light, style } = this.props

    return (
      <RNText
        style={[
          styles.main,
          ...formatGetters(styles, 'size', size),
          ...formatGetters(styles, 'color', color),
          ...formatGetters(styles, 'align', align),
          ...(bold ? [styles.bold] : []),
          ...(light ? [styles.light] : []),
          ...(style ? [style] : []),
        ]}
        {...(nowrap ? { numberOfLines: 1 } : {})}
      >
        {children}
      </RNText>
    )
  }
}

Text.propTypes = {
  children: PropTypes.node,
  align: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  color: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  size: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]).isRequired,
  bold: PropTypes.bool,
  nowrap: PropTypes.bool,
  light: PropTypes.bool,
  style: RNText.propTypes.style,
}

Text.defaultProps = {
  size: 'normal',
}

export default Text
