import React from 'react'
import PropTypes from 'prop-types'

import cx from 'classnames'

import './styles.css'

const Image = ({ src, src2x, circle, width, height }) => (
  <div
    className={cx('Image', {
      'Image--circle': circle,
      'Image--fit': width || height,
    })}
    style={
      width || height ? (
        {
          width,
          height,
        }
      ) : (
        {}
      )
    }
  >
    <img src={src} srcSet={src2x ? `${src2x} 2x` : undefined} />
  </div>
)

Image.propTypes = {
  src: PropTypes.string,
  src2x: PropTypes.string,
  circle: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
}

export default Image
