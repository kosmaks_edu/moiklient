import { Image as RNImage } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'

const Image = ({ src, src2x, circle, width, height }) => (
  <RNImage
    source={{ uri: src2x || src }}
    resizeMode="cover"
    style={{
      width,
      height,
      borderRadius: circle ? Math.max(width, height) / 2 : 0,
    }}
  />
)

Image.propTypes = {
  src: PropTypes.string,
  src2x: PropTypes.string,
  circle: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
}

export default Image
