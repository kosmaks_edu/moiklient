import { View as RNView, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'
import compact from 'lodash/compact'
import noop from 'lodash/noop'

import { COLOR_PRIMARY, COLOR_WHITE } from 'constants/styles'
import Icon from 'components/ui-elements/Icon'
import Text from 'components/ui-elements/Text'

import styles from './styles'

const Button = ({
  secondary,
  footer,
  outline,
  borderless,
  colorless,
  disabled,
  loading,
  color,
  large,
  icon,
  iconRight,
  children,
  onClick,
}) => (
  <TouchableOpacity
    style={compact([
      styles.touchable,
      secondary && styles.touchable__secondary,
      footer && styles.touchable__footer,
      outline && styles.touchable__outline,
      disabled && styles.touchable__disabled,
      footer && outline && styles.touchable__footer__outline,
      borderless && styles.touchable__borderless,
      color && styles[`touchable__${color}`],
    ])}
    onPress={disabled || loading ? noop : onClick}
  >
    <RNView
      style={compact([
        styles.main,
        secondary && styles.main__secondary,
        secondary && icon && !children && styles.main__round,
        footer && styles.main__footer,
      ])}
    >
      {icon && (
        <RNView style={children && styles.iconLeft}>
          <Icon
            glyph={icon}
            style={{ color: outline ? COLOR_PRIMARY : COLOR_WHITE }}
          />
        </RNView>
      )}
      <Text
        style={compact([
          styles.text,
          outline && styles.text__outline,
          large && styles.text__large,
          colorless && styles.text__colorless,
        ])}
      >
        {children}
      </Text>
    </RNView>
  </TouchableOpacity>
)

Button.propTypes = {
  children: PropTypes.node,
  secondary: PropTypes.bool,
  color: PropTypes.string,
  borderless: PropTypes.bool,
  colorless: PropTypes.bool,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  large: PropTypes.bool,
  footer: PropTypes.bool,
  outline: PropTypes.bool,
  icon: PropTypes.string,
  iconRight: PropTypes.string,
  onClick: PropTypes.func,
}

export default Button
