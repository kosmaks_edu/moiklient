import { StyleSheet } from 'react-native'

import {
  BORDER_RADIUS,
  COLOR_BACKGROUND_LIGHT,
  COLOR_BORDER,
  COLOR_FACEBOOK,
  COLOR_FONT_DEFAULT,
  COLOR_INSTAGRAM,
  COLOR_PRIMARY,
  COLOR_SECONDARY,
  COLOR_VK,
  COLOR_WHITE,
} from 'constants/styles'

export default StyleSheet.create({
  touchable: {
    borderRadius: BORDER_RADIUS,
    backgroundColor: COLOR_PRIMARY,
    borderWidth: 1,
    borderColor: COLOR_PRIMARY,
  },

  touchable__secondary: {
    borderRadius: 15,
    backgroundColor: COLOR_SECONDARY,
    borderColor: COLOR_SECONDARY,
  },

  touchable__footer: {
    borderRadius: 0,
  },

  touchable__outline: {
    backgroundColor: COLOR_WHITE,
  },

  touchable__footer__outline: {
    backgroundColor: COLOR_BACKGROUND_LIGHT,
    borderWidth: 0,
    borderTopWidth: 1,
    borderTopColor: COLOR_BORDER,
  },

  touchable__borderless: {
    borderTopWidth: 0,
    backgroundColor: COLOR_WHITE,
  },

  touchable__vk: {
    backgroundColor: COLOR_VK,
    borderColor: COLOR_VK,
  },

  touchable__instagram: {
    backgroundColor: COLOR_INSTAGRAM,
    borderColor: COLOR_INSTAGRAM,
  },

  touchable__facebook: {
    backgroundColor: COLOR_FACEBOOK,
    borderColor: COLOR_FACEBOOK,
  },

  touchable__disabled: {
    opacity: 0.5,
  },

  main: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingRight: 16,
    paddingBottom: 10,
    paddingLeft: 16,
    borderRadius: BORDER_RADIUS,
    alignItems: 'center',
    justifyContent: 'center',
  },

  main__secondary: {
    paddingTop: 0,
    paddingLeft: 12,
    paddingRight: 12,
    paddingBottom: 0,
    height: 30,
    borderRadius: 15,
  },

  main__round: {
    paddingLeft: 0,
    paddingRight: 0,
    width: 30,
  },

  main__footer: {
    height: 50,
  },

  text: {
    color: COLOR_WHITE,
  },

  text__outline: {
    color: COLOR_PRIMARY,
  },

  text__colorless: {
    color: COLOR_FONT_DEFAULT,
  },

  text__large: {
    fontSize: 20,
  },

  iconLeft: {
    marginRight: 10,
  },
})
