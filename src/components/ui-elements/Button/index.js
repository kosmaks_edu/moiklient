import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'
import cx from 'classnames'
import noop from 'lodash/noop'

import { COLOR_FONT_DEFAULT, COLOR_WHITE } from 'constants/styles'
import Icon from 'components/ui-elements/Icon'
import Spinner from 'components/ui-elements/Spinner'

const Button = ({
  secondary,
  colorless,
  color,
  borderless,
  large,
  footer,
  outline,
  loading,
  disabled,
  thin,
  icon,
  iconRight,
  children,
  onClick,
}) => (
  <div
    onClick={disabled || loading ? noop : onClick}
    className={cx('Button', {
      'Button--footer': footer,
      'Button--colorless': colorless,
      'Button--secondary': secondary,
      'Button--outline': outline,
      'Button--thin': thin,
      'Button--borderless': borderless,
      'Button--loading': loading,
      'Button--disabled': disabled,
      'Button--large': large,
      [`Button--color${color}`]: color,
      'Button--round': secondary && icon && !children,
    })}
  >
    {icon && (
      <div className="Button-icon">
        <Icon glyph={icon} />
      </div>
    )}
    {children && <div className="Button-content">{children}</div>}
    {iconRight && (
      <div className="Button-icon">
        <Icon glyph={icon} />
      </div>
    )}
    {loading && (
      <div className="Button-spinner">
        <Spinner
          color={outline ? COLOR_FONT_DEFAULT : COLOR_WHITE}
          size="small"
        />
      </div>
    )}
  </div>
)

Button.propTypes = {
  children: PropTypes.node,
  secondary: PropTypes.bool,
  icon: PropTypes.string,
  color: PropTypes.string,
  iconRight: PropTypes.string,
  footer: PropTypes.bool,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  outline: PropTypes.bool,
  thin: PropTypes.bool,
  borderless: PropTypes.bool,
  large: PropTypes.bool,
  colorless: PropTypes.bool,
  onClick: PropTypes.func,
}

export default Button
