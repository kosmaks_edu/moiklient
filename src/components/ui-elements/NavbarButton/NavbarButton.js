import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'
import cx from 'classnames'
import noop from 'lodash/noop'

import Icon from 'components/ui-elements/Icon'
import Spinner from 'components/ui-elements/Spinner'
import Text from 'components/ui-elements/Text'

const NavbarButton = ({
  icon,
  isBack,
  isLeft,
  disabled,
  loading,
  text,
  onClick,
  onBack,
}) => {
  const leftIcon = isBack ? 'chevron-left' : isLeft && icon
  const rightIcon = !isBack && !isLeft && icon

  return (
    <div
      className={cx('NavbarButton', {
        'NavbarButton--disabled': disabled,
        'NavbarButton--loading': loading,
      })}
      onClick={disabled || loading ? noop : onClick || (isBack ? onBack : noop)}
    >
      {leftIcon && <Icon glyph={leftIcon} />}
      {text && (
        <div className="NavbarButton-text">
          <Text size="medium">{text}</Text>
        </div>
      )}
      {rightIcon && <Icon glyph={icon} />}
      {loading && (
        <div className="NavbarButton-spinner">
          <Spinner size="small" />
        </div>
      )}
    </div>
  )
}

NavbarButton.propTypes = {
  icon: PropTypes.string,
  text: PropTypes.string,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  isBack: PropTypes.bool.isRequired,
  isLeft: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
  onBack: PropTypes.func.isRequired,
}

NavbarButton.defaultProps = {
  isLeft: false,
  isBack: false,
}

export default NavbarButton
