import { View as RNView, StyleSheet, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'
import compact from 'lodash/compact'
import noop from 'lodash/noop'

import { COLOR_PRIMARY } from 'constants/styles'
import Icon from 'components/ui-elements/Icon'
import Spinner from 'components/ui-elements/Spinner'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {
    height: 40,
    minWidth: 40,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: -10,
    marginRight: -10,
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
  },

  disabled: {
    opacity: 0.5,
  },

  text: {
    color: COLOR_PRIMARY,
  },

  text__loading: {
    opacity: 0.3,
  },

  text__left: {
    marginLeft: 5,
  },

  text__right: {
    marginRight: 5,
  },

  spinner: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

const NavbarButton = ({
  icon,
  isBack,
  isLeft,
  loading,
  disabled,
  text,
  onClick,
  onBack,
}) => {
  const leftIcon = isBack ? 'chevron-left' : isLeft && icon
  const rightIcon = !isBack && !isLeft && icon

  return (
    <TouchableOpacity
      style={compact([styles.main, disabled && styles.disabled])}
      onPress={disabled || loading ? noop : onClick || (isBack ? onBack : noop)}
    >
      {leftIcon && (
        <Icon glyph={leftIcon} style={{ color: COLOR_PRIMARY, fontSize: 20 }} />
      )}
      {text && (
        <Text
          size="medium"
          nowrap
          style={compact([
            styles.text,
            leftIcon && styles.text__left,
            rightIcon && styles.text__right,
            loading && styles.text__loading,
          ])}
        >
          {text}
        </Text>
      )}
      {rightIcon && (
        <Icon glyph={icon} style={{ color: COLOR_PRIMARY, fontSize: 20 }} />
      )}
      {loading && (
        <RNView style={styles.spinner}>
          <Spinner size="small" />
        </RNView>
      )}
    </TouchableOpacity>
  )
}

NavbarButton.propTypes = {
  icon: PropTypes.string,
  text: PropTypes.string,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  isBack: PropTypes.bool.isRequired,
  isLeft: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
  onBack: PropTypes.func.isRequired,
}

NavbarButton.defaultProps = {
  isLeft: false,
  isBack: false,
}

export default NavbarButton
