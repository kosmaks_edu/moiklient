import { connect } from 'react-redux'
import { goBack } from 'react-router-redux'

import NavbarButton from './NavbarButton'

export default connect(null, { onBack: goBack })(NavbarButton)
