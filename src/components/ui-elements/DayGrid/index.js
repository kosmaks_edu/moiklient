import './styles.css'

import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import cx from 'classnames'
import map from 'lodash/map'
import partial from 'lodash/partial'

import { getHourTicks } from 'helpers/dayGrid'
import { timeToStr } from 'helpers/time'

const timeToSize = time => time

class DayGrid extends PureComponent {
  renderAxis(ticks) {
    return (
      <div className="DayGrid-axis">
        {map(ticks, (tick, i) => {
          const height =
            i === ticks.length - 1 ? 0 : ticks[i + 1].stamp - tick.stamp

          return (
            <div
              key={tick.stamp}
              className="DayGrid-axis-tick"
              style={{ height: timeToSize(height) }}
            >
              <div className="DayGrid-axis-tick-text">
                {timeToStr(tick.stamp)}
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  renderTicks(ticks) {
    return (
      <div className="DayGrid-events-ticks">
        {map(ticks, (tick, i) => {
          const height =
            i === ticks.length - 1 ? 0 : ticks[i + 1].stamp - tick.stamp

          if (height > 0) {
            return (
              <div
                key={tick.stamp}
                className="DayGrid-events-ticks-tick"
                style={{ height: timeToSize(height) }}
              />
            )
          }

          return null
        })}
      </div>
    )
  }

  renderEvents(ticks) {
    const { events, onEventView } = this.props
    const firstTick = ticks[0]

    return (
      <div className="DayGrid-events-content">
        {map(events, (event, i) => (
          <div
            key={i}
            className="DayGrid-event"
            style={{
              top: timeToSize(event.time - firstTick.stamp),
              height: timeToSize(event.duration),
            }}
            onClick={partial(onEventView, event)}
          >
            <div
              className={cx('DayGrid-event-bubble', {
                'is-inline': event.duration <= 40,
              })}
            >
              <div className="DayGrid-event-bubble-time">
                {timeToStr(event.time)}
              </div>
              <div className="DayGrid-event-bubble-content">
                <div className="DayGrid-event-bubble-content-name">
                  {event.client.name}
                </div>
                <div className="DayGrid-event-bubble-content-service">
                  {event.service.name}
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    )
  }

  renderBreaks(ticks) {
    const { breaks } = this.props
    const firstTick = ticks[0]

    return (
      <div className="DayGrid-breaks">
        {map(breaks, ([from, to], i) => (
          <div
            key={i}
            className="DayGrid-breaks-break"
            style={{
              top: timeToSize(from - firstTick.stamp),
              height: timeToSize(to - from),
            }}
          />
        ))}
      </div>
    )
  }

  render() {
    const { timeRange } = this.props
    const ticks = getHourTicks(timeRange)

    return (
      <div className="DayGrid">
        {this.renderAxis(ticks)}
        {this.renderBreaks(ticks)}

        <div className="DayGrid-events">
          {this.renderTicks(ticks)}
          {this.renderEvents(ticks)}
        </div>
      </div>
    )
  }
}

DayGrid.propTypes = {
  timeRange: PropTypes.array.isRequired,
  events: PropTypes.array.isRequired,
  breaks: PropTypes.array.isRequired,
  onEventView: PropTypes.func.isRequired,
}

export default DayGrid
