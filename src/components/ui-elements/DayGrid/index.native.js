import {
  View as RNView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import compact from 'lodash/compact'
import map from 'lodash/map'
import partial from 'lodash/partial'

import {
  BORDER_RADIUS,
  COLOR_BORDER,
  COLOR_EVENT,
  COLOR_EVENT_BACKGROUND,
} from 'constants/styles'
import { getHourTicks } from 'helpers/dayGrid'
import { timeToStr } from 'helpers/time'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },

  content: {
    flexDirection: 'row',
  },

  axis: {
    width: 50,
    alignItems: 'center',
    borderRightColor: COLOR_BORDER,
    borderRightWidth: 1,
  },

  events: {
    flex: 1,
    position: 'relative',
  },

  tick: {
    borderTopWidth: 1,
    borderTopColor: COLOR_BORDER,
  },

  tick__first: {
    borderTopWidth: 0,
  },

  eventsContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  event: {
    position: 'absolute',
    left: 0,
    right: 0,
    backgroundColor: COLOR_EVENT_BACKGROUND,
    borderRadius: BORDER_RADIUS,
    borderWidth: 1,
    borderColor: COLOR_EVENT,
    marginLeft: 5,
    marginRight: 5,
    flexDirection: 'row',
  },

  eventTime: {
    width: 50,
    flexShrink: 0,
    backgroundColor: COLOR_EVENT_BACKGROUND,
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
  },

  eventTime__inline: {
    justifyContent: 'center',
  },

  eventContent: {
    padding: 5,
  },

  eventContent__inline: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  eventContentName: {
    fontWeight: 'bold',
    marginRight: 5,
  },

  breaks: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  break: {
    position: 'absolute',
    left: 50,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, .05)',
  },
})

const timeToSize = duration => duration * 1.5

class DayGrid extends PureComponent {
  renderAxis(ticks) {
    return (
      <RNView style={styles.axis}>
        {map(ticks, (tick, i) => {
          const height =
            i === ticks.length - 1
              ? 20
              : timeToSize(ticks[i + 1].stamp - tick.stamp)

          let marginTop = 0
          if (i === 1) {
            marginTop = -10
          } else if (i === ticks.length - 1) {
            marginTop = -10
          }

          return (
            <Text
              key={tick.stamp}
              style={{ lineHeight: 20, marginTop, height }}
            >
              {timeToStr(tick.stamp)}
            </Text>
          )
        })}
      </RNView>
    )
  }

  renderTicks(ticks) {
    return (
      <RNView style={styles.eventsContainer}>
        {map(ticks, (tick, i) => {
          const height =
            i === ticks.length - 1
              ? 0
              : timeToSize(ticks[i + 1].stamp - tick.stamp)

          if (height === 0) return null

          return (
            <RNView
              key={tick.stamp}
              style={compact([
                styles.tick,
                i === 0 && styles.tick__first,
                { height },
              ])}
            />
          )
        })}
      </RNView>
    )
  }

  renderEvents(ticks) {
    const { events, onEventView } = this.props
    const firstTick = ticks[0]

    return (
      <RNView style={styles.events}>
        {map(events, (event, i) => (
          <TouchableOpacity
            key={i}
            style={[
              styles.event,
              {
                top: timeToSize(event.time - firstTick.stamp),
                height: timeToSize(event.duration),
              },
            ]}
            onPress={partial(onEventView, event)}
          >
            <RNView
              style={compact([
                styles.eventTime,
                event.duration <= 40 && styles.eventTime__inline,
              ])}
            >
              <Text>{timeToStr(event.time)}</Text>
            </RNView>
            <RNView
              style={compact([
                styles.eventContent,
                event.duration <= 40 && styles.eventContent__inline,
              ])}
            >
              <Text style={styles.eventContentName}>{event.client.name}</Text>
              <Text style={styles.eventContentService}>
                {event.service.name}
              </Text>
            </RNView>
          </TouchableOpacity>
        ))}
      </RNView>
    )
  }

  renderBreaks(ticks) {
    const { breaks } = this.props
    const firstTick = ticks[0]

    return (
      <RNView style={styles.breaks}>
        {map(breaks, ([from, to], i) => (
          <RNView
            key={i}
            style={[
              styles.break,
              {
                top: timeToSize(from - firstTick.stamp),
                height: timeToSize(to - from),
              },
            ]}
          />
        ))}
      </RNView>
    )
  }

  render() {
    const { timeRange } = this.props
    const ticks = getHourTicks(timeRange)

    return (
      <ScrollView style={styles.main}>
        <RNView style={styles.content}>
          {this.renderAxis(ticks)}
          {this.renderBreaks(ticks)}
          <RNView style={styles.events}>
            {this.renderTicks(ticks)}
            {this.renderEvents(ticks)}
          </RNView>
        </RNView>
      </ScrollView>
    )
  }
}

DayGrid.propTypes = {
  timeRange: PropTypes.array.isRequired,
  events: PropTypes.array.isRequired,
  breaks: PropTypes.array.isRequired,
  onEventView: PropTypes.func.isRequired,
}

export default DayGrid
