import {
  Modal,
  Picker,
  View as RNView,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'
import compact from 'lodash/compact'
import find from 'lodash/find'
import map from 'lodash/map'

import {
  BORDER_RADIUS,
  COLOR_BACKGROUND_LIGHT,
  COLOR_BORDER,
  COLOR_FONT_DEFAULT,
  COLOR_FONT_LIGHTEN,
  COLOR_SHADOW,
  COLOR_WHITE,
} from 'constants/styles'
import Icon from 'components/ui-elements/Icon'
import Link from 'components/ui-elements/Link'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {},

  selectBox: {
    flex: 1,
    width: null,
    borderRadius: BORDER_RADIUS,
    borderColor: COLOR_BORDER,
    backgroundColor: COLOR_WHITE,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
  },

  text: {
    flex: 1,
    color: COLOR_FONT_DEFAULT,
    fontSize: 16,
  },

  text__placeholder: {
    color: COLOR_FONT_LIGHTEN,
  },

  backdrop: {
    flex: 1,
    justifyContent: 'flex-end',
    flexDirection: 'column',
  },

  optionList: {
    backgroundColor: COLOR_BACKGROUND_LIGHT,
    shadowRadius: 3,
    shadowColor: COLOR_SHADOW,
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 2 },
  },

  option: {
    borderBottomWidth: 1,
    height: 50,
    borderBottomColor: COLOR_BORDER,
    flexDirection: 'row',
    alignItems: 'center',
  },

  optionText: {
    color: COLOR_FONT_DEFAULT,
  },

  header: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: COLOR_BORDER,
  },

  headerText: {
    flex: 2,
    textAlign: 'center',
    fontWeight: 'bold',
  },

  headerBtn__first: {
    alignItems: 'flex-start',
  },

  headerBtn: {
    flex: 1,
    alignItems: 'flex-end',
    paddingLeft: 10,
    paddingRight: 10,
  },
})

class Select extends Component {
  constructor(props, context) {
    super(props, context)

    this.open = bind(this.open, this)
    this.close = bind(this.close, this)
    this.choose = bind(this.choose, this)
    this.onValueChange = bind(this.onValueChange, this)

    this.state = { isOpen: false, selected: props.value || null }
  }

  componentWillReceiveProps(props) {
    if (
      props.value !== this.props.value &&
      props.value !== this.state.selected
    ) {
      this.setState({ selected: props.value })
    }
  }

  open() {
    this.setState({ isOpen: true })
  }

  close() {
    this.setState({ isOpen: false })
  }

  choose() {
    const { onChange } = this.props

    if (onChange) {
      onChange(this.state.selected)
    }

    this.close()
  }

  onValueChange(selected) {
    this.setState({ selected })
  }

  render() {
    const { isOpen, selected } = this.state
    const { placeholder, options, header, value } = this.props

    const selectedOption = find(options, o => o.value === value)

    return (
      <RNView style={styles.main}>
        <TouchableOpacity style={styles.selectBox} onPress={this.open}>
          <Text
            style={compact([
              styles.text,
              !selectedOption && styles.text__placeholder,
            ])}
          >
            {selectedOption ? selectedOption.label : placeholder}
          </Text>
          <Icon glyph="caret-down" style={{ color: COLOR_FONT_DEFAULT }} />
        </TouchableOpacity>
        {isOpen && (
          <Modal animationType="slide" onRequestClose={this.close} transparent>
            <TouchableWithoutFeedback style={{ flex: 1 }} onPress={this.close}>
              <RNView style={styles.backdrop}>
                <RNView style={styles.optionList}>
                  <RNView style={styles.header}>
                    <RNView style={[styles.headerBtn, styles.headerBtn__first]}>
                      <Link onClick={this.close}>Отмена</Link>
                    </RNView>
                    <Text style={styles.headerText}>
                      {header || placeholder}
                    </Text>
                    <RNView style={styles.headerBtn}>
                      <Link onClick={this.choose}>Выбрать</Link>
                    </RNView>
                  </RNView>
                  <Picker
                    onValueChange={this.onValueChange}
                    selectedValue={selected}
                  >
                    {map(options, ({ label, value }) => (
                      <Picker.Item key={value} label={label} value={value} />
                    ))}
                  </Picker>
                </RNView>
              </RNView>
            </TouchableWithoutFeedback>
          </Modal>
        )}
      </RNView>
    )
  }
}

Select.propTypes = {
  options: PropTypes.array.isRequired,
  value: PropTypes.any,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  header: PropTypes.string,
}

export default Select
