import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'
import map from 'lodash/map'

import Icon from 'components/ui-elements/Icon'

const Select = ({ options, value, onChange }) => (
  <div className="Select">
    <select
      className="Select-select"
      value={value}
      onChange={e => onChange(e.target.value)}
    >
      {map(options, ({ value, label }) => (
        <option key={value} value={value}>
          {label}
        </option>
      ))}
    </select>
    <div className="Select-caret">
      <Icon glyph="caret-down" />
    </div>
  </div>
)

Select.propTypes = {
  options: PropTypes.array.isRequired,
  value: PropTypes.any,
  onChange: PropTypes.func,
}

export default Select
