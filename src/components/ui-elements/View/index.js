import React from 'react'
import PropTypes from 'prop-types'

import { formatModifiers } from 'helpers/components'

import './styles.css'

const View = ({
  children,
  flexDirection,
  flexGrow,
  align,
  justify,
  padding,
  border,
  frame,
  background,
  style,
  touchable,
  onClick,
}) => {
  let className = 'View '
  let extraStyle

  className += formatModifiers('View--direction', flexDirection)

  if (flexGrow === 1) {
    className += ' View--grow'
  } else if (flexGrow > 1) {
    extraStyle = { flex: `${flexGrow} 1 0%` }
  }

  className += formatModifiers('View--align', align)
  className += formatModifiers('View--justify', justify)

  className += formatModifiers('View--padding', padding)
  className += formatModifiers('View--border', border)

  if (frame) {
    className += ' View--frame'
  }

  if (touchable) {
    className += ' View--touchable'
  }

  if (background) {
    className += ` View--background${background}`
  }

  return (
    <div
      className={className}
      onClick={touchable ? onClick : undefined}
      style={{ ...style, ...extraStyle }}
    >
      {children}
    </div>
  )
}

View.propTypes = {
  children: PropTypes.node,
  flexGrow: PropTypes.number.isRequired,
  flexDirection: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  align: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  justify: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  padding: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  border: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  background: PropTypes.string,
  touchable: PropTypes.bool,
  frame: PropTypes.bool,
  style: PropTypes.object,
  onClick: PropTypes.func,
}

View.defaultProps = {
  flexDirection: 'column',
  flexGrow: 0,
  align: 'stretch',
  justify: 'stretch',
}

export default View
