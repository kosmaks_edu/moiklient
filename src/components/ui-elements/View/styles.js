import { StyleSheet } from 'react-native'

import {
  BORDER_RADIUS,
  COLOR_BACKGROUND_LIGHT,
  COLOR_BORDER,
} from 'constants/styles'

const border = (pos = '', color = COLOR_BORDER) => ({
  [`border${pos}Width`]: 1,
  [`border${pos}Color`]: color,
})

const genBorder = (color, name) => ({
  [`border${name}`]: border('', color),
  [`borderv${name}`]: {
    ...border('Top', color),
    ...border('Bottom', color),
  },
  [`borderh${name}`]: {
    ...border('Left', color),
    ...border('Right', color),
  },
  [`borderb${name}`]: border('Bottom', color),
  [`borderl${name}`]: border('Left', color),
  [`borderu${name}`]: border('Top', color),
  [`borderr${name}`]: border('Right', color),
})

const genPadding = (size, name) => ({
  [`padding${name}`]: { padding: size },
  [`paddingv${name}`]: { paddingTop: size, paddingBottom: size },
  [`paddingh${name}`]: { paddingLeft: size, paddingRight: size },
  [`paddingt${name}`]: { paddingTop: size },
  [`paddingr${name}`]: { paddingRight: size },
  [`paddingb${name}`]: { paddingBottom: size },
  [`paddingl${name}`]: { paddingLeft: size },
})

const common = {
  directionrow: { flexDirection: 'row' },
  directioncolumn: { flexDirection: 'column' },

  aligncenter: { alignItems: 'center' },
  alignstart: { alignItems: 'flex-start' },
  alignend: { alignItems: 'flex-end' },
  justifycenter: { justifyContent: 'center' },
  justifystart: { justifyContent: 'flex-start' },
  justifyend: { justifyContent: 'flex-end' },

  grow: { flex: 1 },

  frame: {
    backgroundColor: '#F9F9F9',
    borderRadius: BORDER_RADIUS,
  },

  ...genBorder(COLOR_BORDER, 'normal'),

  backgroundlight: {
    backgroundColor: COLOR_BACKGROUND_LIGHT,
  },

  ...genPadding(5, 'small'),
  ...genPadding(10, 'medium'),
  ...genPadding(15, 'normal'),
  ...genPadding(30, 'large'),
}

const mobile = {
  directionmrow: { flexDirection: 'row' },
  directionmcolumn: { flexDirection: 'column' },

  alignmcenter: { alignItems: 'center' },
  alignmstart: { alignItems: 'flex-start' },
  alignmend: { alignItems: 'flex-end' },
  justifymcenter: { justifyContent: 'center' },
  justifymstart: { justifyContent: 'flex-start' },
  justifymend: { justifyContent: 'flex-end' },

  ...genBorder(COLOR_BORDER, 'mnormal'),

  ...genPadding(5, 'msmall'),
  ...genPadding(10, 'mmedium'),
  ...genPadding(15, 'mnormal'),
  ...genPadding(30, 'mlarge'),
}

export const desktopStyles = StyleSheet.create(common)
export const mobileStyles = StyleSheet.create({ ...common, ...mobile })
