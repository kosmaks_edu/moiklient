import {
  KeyboardAvoidingView,
  View as RNView,
  ScrollView,
  TouchableOpacity,
} from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'

import { deviceSelect } from 'helpers/device'
import { formatGetters } from 'helpers/components'

import { mobileStyles, desktopStyles } from './styles'

const View = ({
  children,
  flexDirection,
  flexGrow,
  align,
  frame,
  justify,
  padding,
  border,
  scrollable,
  noScrollPadding,
  touchable,
  background,
  style,
  onClick,
}) => {
  const styles = deviceSelect(mobileStyles, desktopStyles)

  const localStyle = [
    ...formatGetters(styles, 'direction', flexDirection),
    flexGrow === 1 ? styles.grow : { flex: flexGrow },
    ...formatGetters(styles, 'align', align),
    ...formatGetters(styles, 'justify', justify),
    ...formatGetters(styles, 'padding', padding),
    ...formatGetters(styles, 'border', border),
    ...(background ? [styles[`background${background}`]] : []),
    ...(frame ? [styles.frame] : []),
    style,
  ]

  if (touchable) {
    return (
      <TouchableOpacity style={localStyle} onPress={onClick}>
        {children}
      </TouchableOpacity>
    )
  }

  if (scrollable) {
    return (
      <KeyboardAvoidingView
        keyboardVerticalOffset={noScrollPadding ? 0 : 70}
        behavior="padding"
        style={{ flex: flexGrow }}
      >
        <ScrollView style={localStyle}>{children}</ScrollView>
      </KeyboardAvoidingView>
    )
  }

  return <RNView style={localStyle}>{children}</RNView>
}

View.propTypes = {
  children: PropTypes.node,
  flexGrow: PropTypes.number.isRequired,
  flexDirection: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  align: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  justify: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  padding: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  border: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.array.isRequired,
  ]),
  background: PropTypes.string,
  style: PropTypes.object,
  scrollable: PropTypes.bool,
  frame: PropTypes.bool,
  noScrollPadding: PropTypes.bool,
  touchable: PropTypes.bool,
  onClick: PropTypes.func,
}

View.defaultProps = {
  flexDirection: 'column',
  flexGrow: 0,
  align: 'stretch',
  justify: 'stretch',
}

export default View
