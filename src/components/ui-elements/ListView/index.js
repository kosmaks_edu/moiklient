import PropTypes from 'prop-types'
import React from 'react'
import map from 'lodash/map'

const ListView = ({ data, renderItem, keyExtractor }) => (
  <div>
    {map(data, (item, index) => (
      <div key={keyExtractor ? keyExtractor(item, index) : item.key}>
        {renderItem({ item, index })}
      </div>
    ))}
  </div>
)

ListView.propTypes = {
  data: PropTypes.array.isRequired,
  renderItem: PropTypes.func.isRequired,
  keyExtractor: PropTypes.func,
}

export default ListView
