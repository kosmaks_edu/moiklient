import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'
import cx from 'classnames'
import map from 'lodash/map'

const Tabs = ({ tabs, activeTab, onChange }) => (
  <div className="Tabs">
    {map(tabs, tab => (
      <div
        key={tab.id}
        className={cx('Tabs-tab', { 'Tabs-tab--active': tab.id === activeTab })}
        onClick={() => onChange && onChange(tab.id)}
      >
        {tab.title}
      </div>
    ))}
  </div>
)

Tabs.propTypes = {
  tabs: PropTypes.array.isRequired,
  activeTab: PropTypes.any,
  onChange: PropTypes.func,
}

export default Tabs
