import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'
import cx from 'classnames'

const Spinner = ({ color, size }) => (
  <div
    className={cx('Spinner', { [`Spinner--${size}`]: size })}
    style={color ? { borderColor: color } : {}}
  />
)

Spinner.propTypes = {
  color: PropTypes.string,
  size: PropTypes.string,
}

export default Spinner
