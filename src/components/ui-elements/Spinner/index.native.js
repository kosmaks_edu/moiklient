import { Animated, Easing, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'

import { COLOR_PRIMARY } from 'constants/styles'

const styles = StyleSheet.create({
  circle: {
    width: 30,
    height: 30,
    borderRadius: 15,
    borderWidth: 3,
    borderTopColor: COLOR_PRIMARY,
    borderRightColor: 'transparent',
    borderBottomColor: COLOR_PRIMARY,
    borderLeftColor: COLOR_PRIMARY,
  },

  circle__small: {
    width: 16,
    height: 16,
    borderRadius: 8,
  },
})

class Spinner extends PureComponent {
  constructor() {
    super()
    this.state = {
      rotation: new Animated.Value(0),
    }
  }

  componentDidMount() {
    this.animate()
  }

  animate() {
    this.state.rotation.setValue(0)
    Animated.timing(this.state.rotation, {
      toValue: 359,
      duration: 800,
      easing: Easing.easeInOut,
      isInteraction: false,
    }).start(() => this.animate())
  }

  render() {
    const { rotation } = this.state
    const { color, size } = this.props

    return (
      <Animated.View
        style={[
          styles.circle,
          {
            transform: [
              {
                rotate: rotation.interpolate({
                  inputRange: [0, 359],
                  outputRange: ['0deg', '359deg'],
                }),
              },
            ],
          },
          ...(size ? [styles[`circle__${size}`]] : []),
          ...(color
            ? [
                {
                  borderTopColor: color,
                  borderBottomColor: color,
                  borderLeftColor: color,
                },
              ]
            : []),
        ]}
      />
    )
  }
}

Spinner.propTypes = {
  color: PropTypes.string,
  size: PropTypes.string,
}

export default Spinner
