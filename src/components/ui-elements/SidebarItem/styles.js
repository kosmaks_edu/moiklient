import { StyleSheet } from 'react-native'

import {
  BORDER_RADIUS,
  COLOR_FONT_LIGHTEN,
  COLOR_PRIMARY,
  COLOR_WHITE,
} from 'constants/styles'

export default StyleSheet.create({
  main: {
    borderRadius: BORDER_RADIUS,
    marginBottom: 6,
  },

  main__active: {
    backgroundColor: COLOR_PRIMARY,
  },

  mainContent: {
    flexDirection: 'row',
    height: 44,
    alignItems: 'center',
    paddingLeft: 14,
    paddingRight: 14,
  },

  text: {
    color: COLOR_FONT_LIGHTEN,
    marginLeft: 15,
  },

  text__active: {
    color: COLOR_WHITE,
  },

  count: {
    minWidth: 22,
    height: 22,
    borderRadius: 11,
    backgroundColor: '#FF2626',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 5,
    marginLeft: 8,
  },

  countText: {
    color: COLOR_WHITE,
  },
})
