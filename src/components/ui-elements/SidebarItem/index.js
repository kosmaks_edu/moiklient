import React from 'react'
import PropTypes from 'prop-types'

import Icon from 'components/ui-elements/Icon'

import './styles.css'

const SidebarItem = ({ icon, text, count, isActive, onClick }) => (
  <div
    className={`SidebarItem ${isActive ? 'is-active' : ''}`}
    onClick={onClick}
  >
    {icon && (
      <div className="SidebarItem-icon">
        <Icon glyph={icon} />
      </div>
    )}
    {text}
    {count && <div className="SidebarItem-count">{count}</div>}
  </div>
)

SidebarItem.propTypes = {
  icon: PropTypes.string,
  text: PropTypes.string.isRequired,
  count: PropTypes.number,
  isActive: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
}

SidebarItem.defaultProps = {
  isActive: false,
}

export default SidebarItem
