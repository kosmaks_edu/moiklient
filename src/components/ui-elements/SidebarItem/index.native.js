import { View as RNView, TouchableHighlight } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'
import compact from 'lodash/compact'
import noop from 'lodash/noop'

import {
  COLOR_FONT_LIGHTEN,
  COLOR_PRIMARY_ACTIVE,
  COLOR_PRIMARY_LIGHTEN,
  COLOR_WHITE,
} from 'constants/styles'
import Icon from 'components/ui-elements/Icon'
import Text from 'components/ui-elements/Text'

import styles from './styles'

const SidebarItem = ({ icon, text, count, isActive, onClick }) => (
  <TouchableHighlight
    style={compact([styles.main, isActive && styles.main__active])}
    underlayColor={isActive ? COLOR_PRIMARY_ACTIVE : COLOR_PRIMARY_LIGHTEN}
    onPress={onClick || noop}
  >
    <RNView style={styles.mainContent}>
      {icon && (
        <RNView>
          <Icon
            glyph={icon}
            style={{
              fontSize: 18,
              color: isActive ? COLOR_WHITE : COLOR_FONT_LIGHTEN,
            }}
          />
        </RNView>
      )}
      <Text style={compact([styles.text, isActive && styles.text__active])}>
        {text}
      </Text>
      {count && (
        <RNView style={styles.count}>
          <Text style={styles.countText}>{count}</Text>
        </RNView>
      )}
    </RNView>
  </TouchableHighlight>
)

SidebarItem.propTypes = {
  icon: PropTypes.string,
  text: PropTypes.string.isRequired,
  count: PropTypes.number,
  isActive: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
}

SidebarItem.defaultProps = {
  isActive: false,
}

export default SidebarItem
