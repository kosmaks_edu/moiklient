import { StyleSheet } from 'react-native'

import { COLOR_BORDER, COLOR_FONT_DEFAULT } from 'constants/styles'

export default StyleSheet.create({
  main: {
    height: 69,
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: COLOR_BORDER,
    alignItems: 'stretch',
    justifyContent: 'center',
  },

  main__tablet: {
    height: 99,
  },

  main__lastColumn: {
    borderRightWidth: 0,
  },

  main__weekend: {
    backgroundColor: '#f9f9f9',
  },

  day: {
    marginTop: 4,
  },

  dayText: {
    fontSize: 12,
    textAlign: 'center',
    height: 16,
    borderWidth: 0,
    padding: 0,
    margin: 0,
    color: COLOR_FONT_DEFAULT,
  },

  progress: {
    alignSelf: 'stretch',
    marginTop: 10,
    paddingLeft: 5,
    paddingRight: 5,
    opacity: 0,
  },
})
