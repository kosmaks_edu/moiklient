import { View as RNView, TextInput } from 'react-native'
import React, { PureComponent } from 'react'
import compact from 'lodash/compact'

import { MOBILE, getDevice } from 'helpers/device'
import ProgressBar from 'components/ui-elements/ProgressBar'

import styles from './styles'

class MonthCell extends PureComponent {
  putData({ currentMonth, today, text, holiday }) {
    this.putRefProps('_main', {
      style: {
        backgroundColor: holiday ? '#f9f9f9' : '#fff',
      },
    })

    if (currentMonth) {
      const value = text.length > 1 ? 0.9 : 0.3
      this.putRefProps('_day', { style: { opacity: 1 }, text })
      this.putRefProps('_progressWrap', {
        style: { opacity: value > 0 ? 1 : 0 },
      })
      this.putRefProps('_progress', { value })
    } else {
      this.putRefProps('_day', { style: { opacity: 0 } })
      this.putRefProps('_progressWrap', { style: { opacity: 0 } })
    }
  }

  makeRef(selector) {
    return el => (this[selector] = el)
  }

  putRefProps(selector, props) {
    if (this[selector]) {
      this[selector].setNativeProps(props)
    }
  }

  render() {
    return (
      <RNView
        ref={this.makeRef('_main')}
        style={compact([
          styles.main,
          getDevice() !== MOBILE && styles.main__tablet,
        ])}
      >
        <RNView style={styles.day}>
          <TextInput
            ref={this.makeRef('_day')}
            editable={false}
            style={styles.dayText}
          />
        </RNView>
        <RNView ref={this.makeRef('_progressWrap')} style={styles.progress}>
          <ProgressBar ref={this.makeRef('_progress')} value={0} />
        </RNView>
      </RNView>
    )
  }
}

export default MonthCell
