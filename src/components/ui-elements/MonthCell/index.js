import './styles.css'

import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import cx from 'classnames'
import map from 'lodash/map'

import Icon from 'components/ui-elements/Icon'
import ProgressBar from 'components/ui-elements/ProgressBar'

class MonthCell extends PureComponent {
  renderContent() {
    const { items, isTabletOrLarger } = this.props

    if (!isTabletOrLarger) {
      if (items.length > 0) {
        return <div className="MonthCell-marker" />
      }

      return null
    }

    return (
      <div className="MonthCell-content">
        {map(items.slice(0, 2), item => (
          <div className="MonthCell-entry" key={item.time}>
            <div className="MonthCell-entry-text">
              <div className="MonthCell-entry-text-overflow">{item.text}</div>
            </div>
            <div className="MonthCell-entry-time">{item.time}</div>
          </div>
        ))}
        {items.length > 2 && (
          <div className="MonthCell-spoiler">{`+ еще ${items.length - 2}`}</div>
        )}
      </div>
    )
  }

  renderMenu() {
    const { isSelected, onAddClient, onView, onDeselect } = this.props

    if (!isSelected) return null

    return (
      <div
        className={cx('MonthCell-menu', {
          'is-open': isSelected,
        })}
      >
        <div className="MonthCell-menu-space" onClick={onDeselect} />
        <div className="MonthCell-menuItem" onClick={onAddClient}>
          <Icon glyph="plus" />
          <div className="MonthCell-menuItem-text">{' Клиент'}</div>
        </div>
        <div className="MonthCell-menuItem" onClick={onView}>
          <Icon glyph="eye" />
          <div className="MonthCell-menuItem-text">{' Подробнее'}</div>
        </div>
      </div>
    )
  }

  render() {
    const {
      text,
      isToday,
      isLast,
      isCurrentMonth,
      isWeekend,
      isSelected,
      isTabletOrLarger,
      onSelect,
    } = this.props

    if (!isCurrentMonth) {
      return (
        <div
          className={cx('MonthCell', {
            'MonthCell--last': isLast,
            'MonthCell--weekend': isWeekend,
          })}
        />
      )
    }

    return (
      <div
        className={cx('MonthCell', {
          'MonthCell--last': isLast,
          'MonthCell--weekend': isWeekend,
          'MonthCell--notSelected': !isSelected,
        })}
      >
        <div className="MonthCell-dayWrap">
          <div
            className={cx('MonthCell-day', {
              'MonthCell-day--today': isToday,
              'MonthCell-day--otherMonth': !isCurrentMonth,
              'MonthCell-day--weekend': isWeekend,
            })}
          >
            {text}
          </div>
        </div>
        <div className="MonthCell-selectArea" onClick={onSelect} />
        {isTabletOrLarger ? this.renderContent() : <ProgressBar value={0.5} />}
        {isTabletOrLarger && this.renderMenu()}
      </div>
    )
  }
}

MonthCell.propTypes = {
  text: PropTypes.node,
  isToday: PropTypes.bool,
  isLast: PropTypes.bool,
  isCurrentMonth: PropTypes.bool,
  isWeekend: PropTypes.bool,
  isTabletOrLarger: PropTypes.bool,
  isSelected: PropTypes.bool,
  items: PropTypes.array,
  onAddClient: PropTypes.func,
  onView: PropTypes.func,
  onSelect: PropTypes.func,
  onDeselect: PropTypes.func,
}

export default MonthCell
