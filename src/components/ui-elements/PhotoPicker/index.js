import { connect } from 'react-redux'

import { uploadImage } from 'actions/images'

import PhotoPicker from './PhotoPicker'

const actions = {
  onUploadImage: uploadImage,
}

export default connect(null, actions)(PhotoPicker)
