import {
  View as RNView,
  Image as RNImage,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'

import Icon from 'components/ui-elements/Icon'
import Link from 'components/ui-elements/Link'
import Spinner from 'components/ui-elements/Spinner'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {
    width: 170,
  },

  picker: {
    width: 170,
    height: 170,
    borderRadius: 170 / 2,
    backgroundColor: '#eeeeee',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },

  pickerTextContainer: {
    marginTop: 19,
  },

  pickerText: {
    width: 120,
    textAlign: 'center',
  },

  image: {
    width: 170,
    height: 170,
    borderRadius: 170 / 2,
  },

  cover: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 170,
    height: 170,
    backgroundColor: 'rgba(0, 0, 0, .3)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 170 / 2,
  },

  actions: {
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  errorCont: {
    marginTop: 10,
  },

  error: {
    color: '#FF2244',
    textAlign: 'center',
  },
})

class PhotoPicker extends Component {
  constructor(props, context) {
    super(props, context)

    this.onPick = bind(this.onPick, this)
    this.onReset = bind(this.onReset, this)

    this.state = {
      tempSource: props.value || null,
      error: null,
      isLoading: false,
    }
  }

  componentWillUnmount() {
    if (this.request) {
      this.request.unsubscribe()
    }
  }

  onPick() {
    const { onUploadImage } = this.props

    const options = {
      title: 'Выбор фото',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    }

    ImagePicker.showImagePicker(options, response => {
      if (response.error) {
        this.setState({ error: 'Не удалось выбрать изображение' })
      } else if (!response.didCancel && !response.customButton) {
        this.setState({ isLoading: true, tempSource: response.uri }, () => {
          this.request = onUploadImage(response.uri)
            .then(result => {
              this.setState({ isLoading: false })
              this.onChange(result)
            })
            .catch(() =>
              this.setState({
                isLoading: false,
                error: 'Не удалось загрузить фото',
              })
            )
        })
      }
    })
  }

  onReset() {
    this.setState({ tempSource: null })
    this.onChange(null)
  }

  onChange(image) {
    const { onChange } = this.props

    if (onChange) {
      onChange(image)
    }
  }

  render() {
    const { isLoading, tempSource, error } = this.state
    const { value } = this.props
    const image = tempSource || value
    const hasImage = image && !error

    return (
      <RNView style={styles.main}>
        {hasImage ? (
          <TouchableOpacity style={styles.picker} onPress={this.onPick}>
            <RNImage source={{ uri: image }} style={styles.image} />

            {isLoading && (
              <RNView style={styles.cover}>
                <Spinner />
              </RNView>
            )}
          </TouchableOpacity>
        ) : (
          <TouchableOpacity style={styles.picker} onPress={this.onPick}>
            <Icon glyph="camera" style={{ fontSize: 24 }} />
            <RNView style={styles.pickerTextContainer}>
              <Text style={styles.pickerText}>Нажмите, чтобы выбрать фото</Text>
            </RNView>
          </TouchableOpacity>
        )}

        {error && (
          <RNView style={styles.errorCont}>
            <Text style={styles.error}>{error}</Text>
          </RNView>
        )}

        {hasImage && (
          <RNView style={styles.actions}>
            <Link onClick={this.onReset}>Удалить фото</Link>
          </RNView>
        )}
      </RNView>
    )
  }
}

PhotoPicker.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  onUploadImage: PropTypes.func.isRequired,
}

export default PhotoPicker
