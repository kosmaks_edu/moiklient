import './styles.css'

import Dropzone from 'react-dropzone'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'

import Icon from 'components/ui-elements/Icon'
import Link from 'components/ui-elements/Link'
import Spinner from 'components/ui-elements/Spinner'

class PhotoPicker extends Component {
  constructor(props, context) {
    super(props, context)

    this.onDrop = bind(this.onDrop, this)
    this.onReset = bind(this.onReset, this)

    this.state = {
      tempFile: null,
      error: null,
      isLoading: false,
    }
  }

  componentWillUnmount() {
    if (this.request) {
      this.request.unsubscribe()
    }
  }

  readAsDataURL(file) {
    const reader = new FileReader()
    reader.onload = ev => {
      const image = ev.target.result
      this.setState({ tempFile: image })
    }
    reader.onabort = () =>
      this.setState({ error: 'Не удалось прочитать файл.' })
    reader.onerror = () =>
      this.setState({ error: 'Не удалось прочитать файл.' })
    reader.readAsDataURL(file)
  }

  readAsBinaryString(file) {
    const { onUploadImage } = this.props
    const reader = new FileReader()
    reader.onload = ev => {
      const binaryString = reader.result

      this.setState({ isLoading: true }, () => {
        this.request = onUploadImage(binaryString).then(file => {
          this.setState({ isLoading: false })
          this.onChange(file)
        })
      })
    }
    reader.onabort = () =>
      this.setState({ error: 'Не удалось прочитать файл.' })
    reader.onerror = () =>
      this.setState({ error: 'Не удалось прочитать файл.' })
    reader.readAsDataURL(file)
  }

  onDrop(acceptedFiles, rejectedFiles) {
    if (acceptedFiles.length > 0) {
      this.setState({ error: null })
      const file = acceptedFiles[0]
      this.readAsDataURL(file)
      this.readAsBinaryString(file)
    } else {
      this.setState({
        error: 'Неверный формат файла. Требуется изображение размером до 5 Мб.',
      })
    }
  }

  onReset() {
    this.setState({ tempFile: null })
    this.onChange(null)
  }

  onChange(image) {
    const { onChange } = this.props

    if (onChange) {
      onChange(image)
    }
  }

  renderCircle() {
    const { isLoading, error, tempFile } = this.state
    const { value } = this.props
    const hasImage = (value || tempFile) && !error

    return hasImage ? (
      <div
        className="PhotoPicker-image"
        style={{ backgroundImage: `url('${tempFile || value}')` }}
      >
        {isLoading && (
          <div className="PhotoPicker-image-cover">
            <Spinner />
          </div>
        )}
      </div>
    ) : (
      <div className="PhotoPicker-circle">
        <Icon glyph="camera" style={{ fontSize: 24 }} />
        <div className="PhotoPicker-circle-text">
          Нажмите, чтобы выбрать фото
        </div>
      </div>
    )
  }

  renderActions() {
    const { error, tempFile } = this.state
    const { value } = this.props
    const hasImage = (value || tempFile) && !error

    return hasImage ? (
      <div className="PhotoPicker-actions">
        <Link onClick={this.onReset}>Удалить фото</Link>
      </div>
    ) : null
  }

  render() {
    const { error, isLoading } = this.state

    return (
      <div className="PhotoPicker">
        {isLoading ? (
          this.renderCircle()
        ) : (
          <Dropzone
            accept="image/*"
            className="PhotoPicker-dropzone"
            acceptClassName="PhotoPicker-dropzone--accept"
            rejectClassName="PhotoPicker-dropzone--reject"
            maxSize={1024 * 1024 * 5}
            onDrop={this.onDrop}
          >
            {this.renderCircle()}
          </Dropzone>
        )}
        {error && <div className="PhotoPicker-error">{error}</div>}
        {this.renderActions()}
      </div>
    )
  }
}

PhotoPicker.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  onUploadImage: PropTypes.func.isRequired,
}

export default PhotoPicker
