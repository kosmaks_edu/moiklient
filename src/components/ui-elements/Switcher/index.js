import React from 'react'
import PropTypes from 'prop-types'

import cx from 'classnames'
import partial from 'lodash/partial'
import map from 'lodash/map'

import './styles.css'

const Switcher = ({ items, active, onChange }) => (
  <div className="Switcher">
    {map(items, ({ value, label }) => (
      <div
        className={cx('Switcher-item', {
          'Switcher-item--active': value === active,
        })}
        key={value}
        onClick={onChange && partial(onChange, value)}
      >
        {label}
      </div>
    ))}
  </div>
)

Switcher.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
  active: PropTypes.string,
  onChange: PropTypes.func,
}

export default Switcher
