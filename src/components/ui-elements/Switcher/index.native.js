import { View as RNView, StyleSheet, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React from 'react'
import compact from 'lodash/compact'
import map from 'lodash/map'

import {
  BORDER_RADIUS,
  COLOR_FONT_DEFAULT,
  COLOR_PRIMARY,
  COLOR_SECONDARY,
  COLOR_SHADOW,
  COLOR_WHITE,
} from 'constants/styles'
import Text from 'components/ui-elements/Text'
import selectors from 'store/reducers/selectors'

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    height: 30,
  },

  main__desktop: {
    height: 34,
  },

  button: {
    borderColor: COLOR_PRIMARY,
    borderWidth: 1,
  },

  button__desktop: {
    borderColor: COLOR_SECONDARY,
  },

  button__first: {
    borderTopLeftRadius: BORDER_RADIUS,
    borderBottomLeftRadius: BORDER_RADIUS,
  },

  button__last: {
    borderRightWidth: 1,
    borderTopRightRadius: BORDER_RADIUS,
    borderBottomRightRadius: BORDER_RADIUS,
  },

  button__active: {
    backgroundColor: COLOR_PRIMARY,
  },

  button__active__desktop: {
    backgroundColor: COLOR_SECONDARY,
    shadowColor: COLOR_SHADOW,
    shadowRadius: 2,
    shadowOpacity: 0.7,
    shadowOffset: { width: 0, height: 1 },
  },

  buttonContent: {
    width: 75,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonContent__desktop: {
    width: 125,
    height: 34,
  },

  text: {
    color: COLOR_PRIMARY,
  },

  text__desktop: {
    color: COLOR_FONT_DEFAULT,
  },

  text__active: {
    color: COLOR_WHITE,
  },
})

const Switcher = ({ items, active, isTabletOrLarger, onChange }) => (
  <RNView
    style={compact([styles.main, isTabletOrLarger && styles.main__desktop])}
  >
    {map(items, ({ value, label }, i) => (
      <TouchableOpacity
        key={value}
        style={compact([
          styles.button,
          isTabletOrLarger && styles.button__desktop,
          active === value && styles.button__active,
          isTabletOrLarger &&
            active === value &&
            styles.button__active__desktop,
          i === 0 && styles.button__first,
          i === items.length - 1 && styles.button__last,
        ])}
        onPress={() => onChange(value)}
      >
        <RNView
          style={compact([
            styles.buttonContent,
            isTabletOrLarger && styles.buttonContent__desktop,
          ])}
        >
          <Text
            style={compact([
              styles.text,
              isTabletOrLarger && styles.text__desktop,
              active === value && styles.text__active,
            ])}
          >
            {label}
          </Text>
        </RNView>
      </TouchableOpacity>
    ))}
  </RNView>
)

Switcher.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
  active: PropTypes.string,
  isTabletOrLarger: PropTypes.bool.isRequired,
  onChange: PropTypes.func,
}

const selector = createStructuredSelector({
  isTabletOrLarger: selectors.isTabletOrLarger,
})

export default connect(selector)(Switcher)
