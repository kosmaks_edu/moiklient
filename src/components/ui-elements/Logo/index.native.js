import { Image as RNImage } from 'react-native'
import React from 'react'

import logoImage from './logo.png'

const Logo = () => <RNImage source={logoImage} />

export default Logo
