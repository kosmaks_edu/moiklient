import React from 'react'

import logoImage from './logo.png'
import logo2xImage from './logo@2x.png'

const Logo = () => <img src={logoImage} srcSet={`${logo2xImage} 2x`} />

export default Logo
