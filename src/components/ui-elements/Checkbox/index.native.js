import { View as RNView, StyleSheet, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'

import { BORDER_RADIUS, COLOR_PRIMARY, COLOR_WHITE } from 'constants/styles'
import Icon from 'components/ui-elements/Icon'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    margin: -8,
  },

  checkbox: {
    width: 19,
    height: 19,
    borderRadius: BORDER_RADIUS,
    borderWidth: 1,
    borderColor: '#797979',
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR_WHITE,
  },
})

const Checkbox = ({ text, value, onChange }) => (
  <TouchableOpacity
    style={styles.main}
    onPress={() => onChange && onChange(!value)}
  >
    <RNView style={styles.checkbox}>
      {value && (
        <Icon glyph="check" style={{ color: COLOR_PRIMARY, fontSize: 14 }} />
      )}
    </RNView>
    <Text>{text}</Text>
  </TouchableOpacity>
)

Checkbox.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  value: PropTypes.bool,
  onChange: PropTypes.func,
}

export default Checkbox
