import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'

import { COLOR_PRIMARY } from 'constants/styles'
import Icon from 'components/ui-elements/Icon'

const Checkbox = ({ text, value, onChange }) => (
  <div
    className="Checkbox"
    onClick={() => onChange && onChange(!value)}
  >
    <div className="Checkbox-fill">
      {value && (
        <Icon glyph="check" style={{ color: COLOR_PRIMARY, fontSize: 14 }} />
      )}
    </div>
    {text}
  </div>
)

Checkbox.propTypes = {
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  value: PropTypes.bool,
  onChange: PropTypes.func,
}

export default Checkbox
