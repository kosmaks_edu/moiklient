import { Icons } from 'react-native-fontawesome'
import { Text as RNText } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'
import kebabCase from 'lodash/kebabCase'
import transform from 'lodash/transform'

import { platformPick } from 'helpers/platform'

const IconsKebab = transform(
  Icons,
  (icons, icon, key) => {
    icons[kebabCase(key)] = icon
  },
  {}
)

const sizes = {
  // TODO fill styles
}

const Icon = ({ glyph, style, size }) => (
  <RNText
    style={{
      fontFamily: platformPick({ ios: () => 'FontAwesome', android: () => 'fontawesome' }),
      fontSize: sizes[size] || 14,
      backgroundColor: 'transparent',
      color: 'black',
      ...style,
    }}
  >
    {IconsKebab[glyph]}
  </RNText>
)

Icon.propTypes = {
  glyph: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['lg', '2x', '3x', '4x', '5x']),
  style: PropTypes.object,
}

export default Icon
