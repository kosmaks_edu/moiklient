import React from 'react'
import PropTypes from 'prop-types'

const Icon = ({ glyph, size, style }) => {
  const sizeClass = size ? `fa-${size}` : ''
  return <i style={style} className={`fa fa-${glyph} ${sizeClass}`} />
}

Icon.propTypes = {
  glyph: PropTypes.string.isRequired,
  size: PropTypes.oneOf(['lg', '2x', '3x', '4x', '5x']),
  style: PropTypes.object,
}

export default Icon
