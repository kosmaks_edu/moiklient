import { View as RNView, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'

import { COLOR_EVENT } from 'constants/styles'

const styles = StyleSheet.create({
  main: {
    height: 5,
    borderRadius: 2.5,
    borderColor: COLOR_EVENT,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
  },

  fill: {
    height: 3,
    backgroundColor: COLOR_EVENT,
    borderRadius: 1.5,
  },
})

class ProgressBar extends PureComponent {
  setNativeProps(props) {
    const value = props.value || this.props.value

    if (this._fill) {
      this._fill.setNativeProps({ style: { flex: value } })
    }

    if (this._empty) {
      this._empty.setNativeProps({ style: { flex: 1 - value } })
    }
  }

  render() {
    const { value } = this.props
    return (
      <RNView style={styles.main}>
        <RNView
          ref={el => (this._fill = el)}
          style={[styles.fill, { flex: value }]}
        />
        <RNView
          ref={el => (this._empty = el)}
          style={[styles.empty, { flex: 1 - value }]}
        />
      </RNView>
    )
  }
}

ProgressBar.propTypes = {
  value: PropTypes.number.isRequired,
}

ProgressBar.defaultProps = {
  value: 0,
}

export default ProgressBar
