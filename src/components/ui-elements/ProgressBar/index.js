import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'

const ProgressBar = ({ value }) => (
  <div className="ProgressBar">
    <div className="ProgressBar-fill" style={{ width: `${value * 100}%` }} />
  </div>
)

ProgressBar.propTypes = {
  value: PropTypes.number.isRequired,
}

ProgressBar.defaultProps = {
  value: 0,
}

export default ProgressBar
