import { View as RNView, StyleSheet, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'

import { COLOR_PRIMARY, COLOR_WHITE } from 'constants/styles'
import Text from 'components/ui-elements/Text'

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    margin: -8,
  },

  checkbox: {
    width: 20,
    height: 20,
    borderRadius: 20 / 2,
    borderWidth: 1,
    borderColor: '#797979',
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR_WHITE,
  },

  fill: {
    width: 15,
    height: 15,
    borderRadius: 15 / 2,
    backgroundColor: COLOR_PRIMARY,
  },
})

const Radio = ({ text, value, onChange }) => (
  <TouchableOpacity
    style={styles.main}
    onPress={() => onChange && onChange(!value)}
  >
    <RNView style={styles.checkbox}>
      {value && <RNView style={styles.fill} />}
    </RNView>
    <Text>{text}</Text>
  </TouchableOpacity>
)

Radio.propTypes = {
  text: PropTypes.string,
  value: PropTypes.bool,
  onChange: PropTypes.func,
}

export default Radio
