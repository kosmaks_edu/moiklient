import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'

const Radio = ({ text, value, onChange }) => (
  <div className="Radio" onClick={() => onChange && onChange(!value)}>
    <div className="Radio-fill">
      {value && (
        <div className="Radio-fill-check" />
      )}
    </div>
    {text}
  </div>
)

Radio.propTypes = {
  text: PropTypes.string,
  value: PropTypes.bool,
  onChange: PropTypes.func,
}

export default Radio
