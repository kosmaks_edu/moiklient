import React from 'react'
import PropTypes from 'prop-types'

import Link from 'components/ui-elements/Link'

import './styles.css'

const Footer = () => (
  <div className="Footer">
    <div className="Footer-copyright">Мой клиент © 2017</div>
    <div className="Footer-links">
      <Link href="#">Политика приватности</Link>
      <Link href="#">Контакты</Link>
      <Link href="#">О сервисе</Link>
    </div>
  </div>
)

const Main = ({ children }) => (
  <div className="MainLayout">
    <div className="MainLayout-content">{children}</div>
    <Footer />
  </div>
)

Main.propTypes = {
  children: PropTypes.node,
}

export default Main
