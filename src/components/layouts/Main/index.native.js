import React from 'react'
import { View as RNView, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'

import selectors from 'store/reducers/selectors'

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
})

const Main = ({ children }) => <RNView style={styles.main}>{children}</RNView>

Main.propTypes = {
  children: PropTypes.node,
}

const selector = createStructuredSelector({
  isTabletOrLarger: selectors.isTabletOrLarger,
})

export default connect(selector)(Main)
