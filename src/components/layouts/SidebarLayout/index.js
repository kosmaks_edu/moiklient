import './styles.css'

import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import cx from 'classnames'

import { closeSidebar } from 'actions/sidebar'
import Icon from 'components/ui-elements/Icon'
import Logo from 'components/ui-elements/Logo'
import Sidebar from 'components/blocks/Sidebar'
import selectors from 'store/reducers/selectors'

class SidebarLayout extends Component {
  render() {
    const {
      navbar,
      isDesktop,
      isSidebarOpen,
      noDrawer,
      children,
      onCloseSidebar,
    } = this.props

    const navigation = !isDesktop && (
      <div className="SidebarLayout-navbar">{navbar}</div>
    )

    const drawer = !noDrawer && (
      <div className="SidebarLayout-sidebar-content">
        <div className="SidebarLayout-sidebar-heading">
          <div className="SidebarLayout-sidebar-bars" onClick={onCloseSidebar}>
            <Icon glyph="chevron-left" size="lg" />
          </div>
          <div className="SidebarLayout-logo">
            <Logo />
          </div>
        </div>
        <div className="SidebarLayout-sidebar-content-children">
          <Sidebar />
        </div>
      </div>
    )

    return (
      <div className="SidebarLayout">
        {navigation}
        <div
          className={cx('SidebarLayout-fade', {
            'is-open': isSidebarOpen,
          })}
          onClick={onCloseSidebar}
        />
        <div
          className={cx('SidebarLayout-sidebar', {
            'is-open': isSidebarOpen,
          })}
        >
          {drawer}
        </div>
        <div className="SidebarLayout-content">{children}</div>
      </div>
    )
  }
}

SidebarLayout.propTypes = {
  children: PropTypes.node,
  isDesktop: PropTypes.bool,
  isSidebarOpen: PropTypes.bool,
  noDrawer: PropTypes.bool,
  navbar: PropTypes.node,
  onCloseSidebar: PropTypes.func.isRequired,
}

const selector = createStructuredSelector({
  isDesktop: selectors.isDesktop,
  isSidebarOpen: selectors.isSidebarOpen,
})

const actions = {
  onCloseSidebar: closeSidebar,
}

export default connect(selector, actions)(SidebarLayout)
