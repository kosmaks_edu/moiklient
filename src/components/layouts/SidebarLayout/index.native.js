import { View as RNView, StatusBar, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import Drawer from 'react-native-drawer'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'
import compact from 'lodash/compact'

import { COLOR_BACKGROUND_LIGHT } from 'constants/styles'
import { closeSidebar, openSidebar } from 'actions/sidebar'
import Icon from 'components/ui-elements/Icon'
import Logo from 'components/ui-elements/Logo'
import Sidebar from 'components/blocks/Sidebar'
import selectors from 'store/reducers/selectors'

import styles from './styles'

class SidebarLayout extends Component {
  constructor(props) {
    super(props)

    this.onGestureOpen = bind(this.onGestureOpen, this)
    this.onGestureClose = bind(this.onGestureClose, this)
    this.isSidebarOpen = false
  }

  componentWillReceiveProps(props) {
    if (props.isSidebarOpen !== this.isSidebarOpen) {
      this.isSidebarOpen = props.isSidebarOpen

      if (props.isSidebarOpen) {
        this._drawerRef.open()
      } else {
        this._drawerRef.close()
      }
    }
  }

  onGestureOpen() {
    this.isSidebarOpen = true

    if (this.props.isSidebarOpen !== this.isSidebarOpen) {
      this.props.onOpenSidebar()
    }
  }

  onGestureClose() {
    this.isSidebarOpen = false

    if (this.props.isSidebarOpen !== this.isSidebarOpen) {
      this.props.onCloseSidebar()
    }
  }

  render() {
    const {
      navbar,
      isDesktop,
      isLandscape,
      children,
      onCloseSidebar,
    } = this.props

    const menu = (
      <RNView
        style={compact([styles.drawer, isLandscape && styles.drawer__vertical])}
      >
        <RNView
          style={compact([
            styles.drawerHeading,
            isDesktop && styles.drawerHeading__desktop,
          ])}
        >
          {!isDesktop && (
            <TouchableOpacity
              style={styles.drawerHeadingArrow}
              onPress={onCloseSidebar}
            >
              <Icon
                glyph="chevron-left"
                style={{ fontSize: 20, color: '#777' }}
              />
            </TouchableOpacity>
          )}
          <RNView
            style={compact([
              styles.drawerHeadingLogo,
              isDesktop && styles.drawerHeadingLogo__desktop,
            ])}
          >
            <Logo />
          </RNView>
        </RNView>
        <Sidebar />
      </RNView>
    )

    const content = (
      <RNView style={compact([styles.main, isDesktop && styles.main__desktop])}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={COLOR_BACKGROUND_LIGHT}
        />
        {!isDesktop && navbar}
        {isDesktop && <RNView style={styles.sideMenu}>{menu}</RNView>}
        <RNView
          style={compact([
            styles.content,
            isDesktop && styles.content__desktop,
          ])}
        >
          {isDesktop && <RNView style={styles.contentPadding} />}
          {children}
        </RNView>
      </RNView>
    )

    return (
      <Drawer
        ref={el => {
          this._drawerRef = el
        }}
        panOpenMask={5}
        tapToClose
        type="overlay"
        captureGestures
        openDrawerOffset={viewport => viewport.width - 270}
        content={menu}
        tweenHandler={ratio => ({
          drawer: { shadowRadius: ratio * 3 },
          mainOverlay: { opacity: ratio * 0.3 },
        })}
        styles={{
          drawer: {
            shadowColor: '#000000',
            shadowOpacity: 0.2,
            shadowRadius: 0,
          },
          mainOverlay: {
            backgroundColor: 'black',
            opacity: 0,
          },
        }}
        onOpen={this.onGestureOpen}
        onClose={this.onGestureClose}
      >
        {content}
      </Drawer>
    )
  }
}

SidebarLayout.propTypes = {
  children: PropTypes.node,
  isLandscape: PropTypes.bool.isRequired,
  isDesktop: PropTypes.bool.isRequired,
  isSidebarOpen: PropTypes.bool.isRequired,
  navbar: PropTypes.node,
  onOpenSidebar: PropTypes.func.isRequired,
  onCloseSidebar: PropTypes.func.isRequired,
}

const selector = createStructuredSelector({
  isLandscape: selectors.isLandscape,
  isDesktop: selectors.isDesktop,
  isSidebarOpen: selectors.isSidebarOpen,
})

const actions = {
  onOpenSidebar: openSidebar,
  onCloseSidebar: closeSidebar,
}

export default connect(selector, actions)(SidebarLayout)
