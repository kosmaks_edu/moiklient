import { StyleSheet } from 'react-native'

import {
  COLOR_BACKGROUND_LIGHT,
  COLOR_BORDER,
  COLOR_SHADOW,
  COLOR_WHITE,
} from 'constants/styles'
import { platformPick } from 'helpers/platform'

export default StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: COLOR_WHITE,
    position: 'relative',
  },

  main__desktop: {
    flexDirection: 'row',
  },

  content: {
    flex: 1,
  },

  content__desktop: {
  },

  contentPadding: {
    backgroundColor: COLOR_BACKGROUND_LIGHT,
    height: platformPick({ ios: () => 20, android: 0 }),
  },

  drawer: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND_LIGHT,
    paddingTop: platformPick({ ios: () => 20, android: 0 }),
    paddingLeft: 13,
    paddingRight: 13,
    paddingBottom: 19,
  },

  drawer__vertical: {
    paddingTop: 0,
  },

  mainOverlay: {
    backgroundColor: COLOR_SHADOW,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  drawerHeading: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: COLOR_BORDER,
    height: 50,
    alignItems: 'center',
    marginBottom: 13,
  },

  drawerHeading__desktop: {
    height: 83,
    justifyContent: 'center',
  },

  drawerHeadingArrow: {
    marginTop: 3,
  },

  drawerHeadingLogo: {
    marginLeft: 20,
  },

  drawerHeadingLogo__desktop: {
    marginLeft: 0,
  },

  sideMenu: {
    width: 270,
    paddingTop: platformPick({ ios: () => 20, android: 0 }),
    backgroundColor: COLOR_BACKGROUND_LIGHT,
    borderRightWidth: 1,
    borderRightColor: COLOR_BORDER,
  },
})
