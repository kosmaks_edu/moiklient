import { StyleSheet } from 'react-native'

import { platformPick } from 'helpers/platform'

import { COLOR_BORDER, COLOR_BACKGROUND_LIGHT } from 'constants/styles'

export default StyleSheet.create({
  main: {
    height: platformPick({
      ios: () => 70,
      android: () => 50,
    }),
    backgroundColor: COLOR_BACKGROUND_LIGHT,
    borderBottomWidth: 1,
    borderBottomColor: COLOR_BORDER,
    paddingTop: platformPick({ ios: () => 20, android: () => 0 }),
    flexDirection: 'row',
  },

  main__vertical: {
    height: 45,
    paddingTop: 0,
  },

  left: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 10,
    paddingRight: 10,
    flex: 2,
  },

  right: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingLeft: 10,
    paddingRight: 10,
    flex: 2,
  },

  center: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 3,
  },
})
