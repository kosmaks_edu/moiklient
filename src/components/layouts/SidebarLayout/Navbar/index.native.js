import { View as RNView } from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import React from 'react'
import compact from 'lodash/compact'

import PropTypes from 'prop-types'
import Text from 'components/ui-elements/Text'
import selectors from 'store/reducers/selectors'

import styles from './styles'

const Navbar = ({ center, centerText, left, right, isLandscape }) => (
  <RNView style={compact([styles.main, isLandscape && styles.main__vertical])}>
    <RNView style={styles.left}>{left}</RNView>
    {(center || centerText) && (
      <RNView style={styles.center}>
        {center}
        {centerText && <Text nowrap size="medium">{centerText}</Text>}
      </RNView>
    )}
    <RNView style={styles.right}>{right}</RNView>
  </RNView>
)

Navbar.propTypes = {
  left: PropTypes.node,
  center: PropTypes.node,
  centerText: PropTypes.string,
  right: PropTypes.node,
  isLandscape: PropTypes.bool.isRequired,
}

const selector = createStructuredSelector({
  isLandscape: selectors.isLandscape,
})

export default connect(selector)(Navbar)
