import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'

import Text from 'components/ui-elements/Text'

const Navbar = ({ left, right, center, centerText }) => (
  <div className="Navbar">
    <div className="Navbar-left">
      {left}
    </div>
    <div className="Navbar-center">
      {center}
      {centerText && <Text size="medium">{centerText}</Text>}
    </div>
    <div className="Navbar-right">{right}</div>
  </div>
)

Navbar.propTypes = {
  left: PropTypes.node,
  center: PropTypes.node,
  centerText: PropTypes.string,
  right: PropTypes.node,
}

export default Navbar
