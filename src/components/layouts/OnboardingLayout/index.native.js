import { View as RNView, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import React from 'react'

import { platformPick } from 'helpers/platform'
import Logo from 'components/ui-elements/Logo'
import View from 'components/ui-elements/View'

const styles = StyleSheet.create({
  logo: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

const OnboardingLayout = ({ children }) => (
  <View
    flexGrow={1}
    style={{ paddingTop: platformPick({ ios: () => 30, android: () => 10 }) }}
  >
    <RNView style={styles.logo}>
      <Logo />
    </RNView>
    <View flexGrow={1}>
      {children}
    </View>
  </View>
)

OnboardingLayout.propTypes = {
  children: PropTypes.node,
}

export default OnboardingLayout
