import './styles.css'

import PropTypes from 'prop-types'
import React from 'react'

import Logo from 'components/ui-elements/Logo'

const OnboardingLayout = ({ children }) => (
  <div className="OnboardingLayout">
    <div className="OnboardingLayout-logo">
      <Logo />
    </div>
    <div className="OnboardingLayout-content">
      {children}
    </div>
  </div>
)

OnboardingLayout.propTypes = {
  children: PropTypes.node,
}

export default OnboardingLayout
