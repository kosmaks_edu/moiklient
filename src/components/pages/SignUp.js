import React from 'react'

import AuthForm from 'components/blocks/AuthForm'

const SignUp = () => <AuthForm />

export default SignUp
