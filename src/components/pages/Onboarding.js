import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React from 'react'
import keys from 'lodash/keys'

import { setOnboardingStep } from 'actions/onboarding'
import AdditionalStep from 'components/blocks/AdditionalStep'
import Button from 'components/ui-elements/Button'
import FinishStep from 'components/blocks/FinishStep'
import Link from 'components/ui-elements/Link'
import OnboardingLayout from 'components/layouts/OnboardingLayout'
import PaymentsStep from 'components/blocks/PaymentsStep'
import ProfileStep from 'components/blocks/ProfileStep'
import ScheduleStep from 'components/blocks/ScheduleStep'
import ServicesStep from 'components/blocks/ServicesStep'
import SplitLine from 'components/ui-elements/SplitLine'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import selectors from 'store/reducers/selectors'
import * as stepsIds from 'constants/onboardingSteps'

const steps = {
  [stepsIds.PROFILE]: {
    index: 1,
    title: 'Добро пожаловать!',
    description: 'Давайте заполним Ваш профиль.',
    component: ProfileStep,
    nextStep: stepsIds.SCHEDULE,
  },

  [stepsIds.SCHEDULE]: {
    index: 2,
    title: 'Расписание работы',
    description: 'Нам нужно знать когда к Вам записывать клиентов.',
    component: ScheduleStep,
    previousStep: stepsIds.PROFILE,
    nextStep: stepsIds.SERVICES,
  },

  [stepsIds.SERVICES]: {
    index: 3,
    title: 'Услуги',
    description: 'Укажите виды услуг, которые Вы предоставляете.',
    component: ServicesStep,
    previousStep: stepsIds.SCHEDULE,
    nextStep: stepsIds.ADDITIONAL,
  },

  [stepsIds.ADDITIONAL]: {
    index: 4,
    title: 'Настройка нотификаций',
    description: 'Все почти готово.',
    component: AdditionalStep,
    previousStep: stepsIds.SERVICES,
    nextStep: stepsIds.PAYMENTS,
  },

  [stepsIds.PAYMENTS]: {
    index: 5,
    title: 'Оплата',
    description: 'Введите данные банковской карты.',
    component: PaymentsStep,
    previousStep: stepsIds.ADDITIONAL,
    nextStep: stepsIds.FINISH,
  },

  [stepsIds.FINISH]: {
    index: 6,
    title: 'Готово!',
    description: 'Все готово, чтобы начать использовать “Мой Клиент”.',
    component: FinishStep,
    previousStep: stepsIds.PAYMENTS,
  },
}

const totalSteps = keys(steps).length

const Onboarding = ({ onboardingStep, isTabletOrLarger, onSetStep }) => {
  const step = steps[onboardingStep]
  const isLast = totalSteps === step.index
  const showBackButton = step.previousStep && !isTabletOrLarger
  const showBackLink = step.previousStep && isTabletOrLarger

  return (
    <OnboardingLayout>
      <View padding={['hlarge', 'hmnormal']}>
        <SplitLine />

        {isTabletOrLarger && (
          <View padding={['vnormal', 'vmsmall', 'bnormal']}>
            <Text align="center" color="lighten">
              Шаг {step.index} / {totalSteps}
            </Text>
          </View>
        )}

        <View padding="bnormal">
          <Text size={['large', 'mmedium']} align="center" color="secondary">
            {step.title}
          </Text>
        </View>

        <View padding={['bnormal', 'bmsmall']}>
          <Text size={['medium', 'mnormal']} align="center">
            {step.description}
          </Text>
        </View>

        <SplitLine />
      </View>

      <View flexGrow={1}>{React.createElement(step.component)}</View>

      <View flexDirection="row" justify="center">
        <View
          flexGrow={1}
          flexDirection={isTabletOrLarger ? 'column' : 'row'}
          style={isTabletOrLarger ? { maxWidth: 200 } : {}}
        >
          {showBackButton && (
            <View flexGrow={1}>
              <Button
                footer
                outline
                colorless
                onClick={() => onSetStep(step.previousStep)}
              >
                Назад
              </Button>
            </View>
          )}
          <View flexGrow={1}>
            <Button
              footer={!isTabletOrLarger}
              onClick={() => !isLast && onSetStep(step.nextStep)}
            >
              {isLast ? 'Перейти к календарю' : 'Далее'}
            </Button>
          </View>
          {showBackLink && <View padding="bnormal" />}
          {showBackLink && (
            <View align="center">
              <Link onClick={() => onSetStep(step.previousStep)}>Назад</Link>
            </View>
          )}
        </View>
      </View>
    </OnboardingLayout>
  )
}

Onboarding.propTypes = {
  isTabletOrLarger: PropTypes.bool.isRequired,
  onboardingStep: PropTypes.string.isRequired,
  onSetStep: PropTypes.func.isRequired,
}

const selector = createStructuredSelector({
  isTabletOrLarger: selectors.isTabletOrLarger,
  onboardingStep: selectors.getOnboardingStep,
})

const actions = {
  onSetStep: setOnboardingStep,
}

export default connect(selector, actions)(Onboarding)
