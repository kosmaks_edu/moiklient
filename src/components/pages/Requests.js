import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'
import filter from 'lodash/filter'

import { events } from 'components/blocks/DayView/mocks'
import { timeToStr } from 'helpers/time'
import Button from 'components/ui-elements/Button'
import Icon from 'components/ui-elements/Icon'
import Link from 'components/ui-elements/Link'
import ListView from 'components/ui-elements/ListView'
import MenuButton from 'components/ui-elements/MenuButton'
import Navbar from 'components/layouts/SidebarLayout/Navbar'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import selectors from 'store/reducers/selectors'

class Requests extends Component {
  constructor(props, context) {
    super(props, context)

    this.renderItem = bind(this.renderItem, this)
  }

  renderItem({ item }) {
    const { isTabletOrLarger } = this.props

    return (
      <View
        touchable={!isTabletOrLarger}
        padding="normal"
        border="bnormal"
        flexDirection="row"
      >
        <View
          align="center"
          justify="center"
          padding="rnormal"
          border="rnormal"
        >
          <View padding="bsmall">
            <Text size="large">{timeToStr(item.time)}</Text>
          </View>
          <Text color="secondary">28.08</Text>
        </View>

        <View padding="hnormal" justify="center" flexGrow={1}>
          <View padding="bsmall">
            <Text size="medium">{item.client.name}</Text>
          </View>
          <Text>{item.service.name}</Text>
        </View>

        {isTabletOrLarger && (
          <View
            padding="hnormal"
            justify="center"
            border="lnormal"
            flexGrow={1}
          >
            {item.client.phone && (
              <View padding="bsmall" flexDirection="row" align="center">
                <View padding="rmedium">
                  <Icon glyph="phone" />
                </View>
                <Link href={`tel:${item.client.phone}`}>
                  {item.client.phone}
                </Link>
              </View>
            )}
            {item.client.email && (
              <View flexDirection="row" align="center">
                <View padding="rmedium">
                  <Icon glyph="envelope" />
                </View>
                <Link href={`mailto:${item.client.email}`}>
                  {item.client.email}
                </Link>
              </View>
            )}
          </View>
        )}

        {isTabletOrLarger ? (
          <View justify="center">
            <Button thin>Подтвердить</Button>
            <View padding="bsmall" />
            <Button thin outline colorless>
              Отклонить
            </Button>
          </View>
        ) : (
          <View justify="center">
            <Icon glyph="chevron-right" />
          </View>
        )}
      </View>
    )
  }

  render() {
    const { requests, isDesktop, isTabletOrLarger } = this.props

    return (
      <View flexGrow={1}>
        {isDesktop && (
          <View
            background="light"
            border="bnormal"
            padding={['tlarge', 'hnormal', 'bnormal']}
            flexDirection="row"
            align="center"
          >
            <Icon glyph="user-plus" style={{ fontSize: 20 }} />
            <View padding="rnormal" />
            <Text size="large">Заявки</Text>
          </View>
        )}
        <ListView
          data={requests}
          renderItem={this.renderItem}
          keyExtractor={x => x.id}
          extraData={{ isTabletOrLarger }}
        />
      </View>
    )
  }
}

Requests.propTypes = {
  requests: PropTypes.array.isRequired,
  isTabletOrLarger: PropTypes.bool.isRequired,
  isDesktop: PropTypes.bool.isRequired,
}

Requests.navbar = function RequestsNavbar() {
  return <Navbar left={<MenuButton />} centerText="Заявки" />
}

const selector = createStructuredSelector({
  requests: () => filter(events, ev => ev.isPending),
  isTabletOrLarger: selectors.isTabletOrLarger,
  isDesktop: selectors.isDesktop,
})

export default connect(selector)(Requests)
