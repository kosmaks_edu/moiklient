import './styles.css'

import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import PropTypes from 'prop-types'
import React from 'react'

import Button from 'components/ui-elements/Button'
import selectors from 'store/reducers/selectors'

const suit = (sel = '') => `ClientRequestDateSelector${sel}`

const ClientRequestDateSelector = ({ isTabletOrLarger }) => (
  <div className={suit()}>
    <div className={suit('-date')}>
      <b>Август</b> 2017
    </div>

    <div className={suit('-selector')}>
      <Button secondary icon="chevron-left" />

      {!isTabletOrLarger && (
        <div className={suit('-grid')}>
          <div className={suit('-grid-headings')}>
            <div className={suit('-heading')}>Пн, 1</div>
          </div>
          <div className={suit('-grid-body')}>
            <div className={suit('-cell')}>
              <div className={suit('-event')}>8:00 &ndash; 9:00</div>
              <div className={suit('-event')}>8:00 &ndash; 9:00</div>
              <div className={suit('-event')}>8:00 &ndash; 9:00</div>
            </div>
          </div>
        </div>
      )}

      {isTabletOrLarger && (
        <div className={suit('-grid')}>
          <div className={suit('-grid-headings')}>
            <div className={suit('-heading')}>Пн, 1</div>
            <div className={suit('-heading')}>Вт, 2</div>
            <div className={suit('-heading')}>Ср, 3</div>
            <div className={suit('-heading')}>Чт, 4</div>
            <div className={suit('-heading')}>Пт, 5</div>
            <div className={suit('-heading')}>Сб, 6</div>
            <div className={suit('-heading')}>Вс, 7</div>
          </div>
          <div className={suit('-grid-body')}>
            <div className={suit('-cell')}>
              <div className={suit('-event')}>8:00 &ndash; 9:00</div>
              <div className={suit('-event')}>8:00 &ndash; 9:00</div>
              <div className={suit('-event')}>8:00 &ndash; 9:00</div>
            </div>
            <div className={suit('-cell')} />
            <div className={suit('-cell')} />
            <div className={suit('-cell')} />
            <div className={suit('-cell')} />
            <div className={suit('-cell')} />
            <div className={suit('-cell')} />
          </div>
        </div>
      )}
      <Button secondary icon="chevron-right" />
    </div>
  </div>
)

ClientRequestDateSelector.propTypes = {
  isTabletOrLarger: PropTypes.bool.isRequired,
}

const selector = createStructuredSelector({
  isTabletOrLarger: selectors.isTabletOrLarger,
})

export default connect(selector)(ClientRequestDateSelector)
