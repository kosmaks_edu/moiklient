import './styles.css'

import React from 'react'

import Button from 'components/ui-elements/Button'
import Checkbox from 'components/ui-elements/Checkbox'
import ClientRequestDateSelector from
  'components/pages/ClientRequest/DateSelector'
import Image from 'components/ui-elements/Image'
import Input from 'components/ui-elements/Input'
import Link from 'components/ui-elements/Link'
import Logo from 'components/ui-elements/Logo'
import Radio from 'components/ui-elements/Radio'
import SplitLine from 'components/ui-elements/SplitLine'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'

const ClientRequestHeader = () => (
  <div className="ClientRequestHeader">
    <div className="ClientRequestHeader-logo">
      <Logo />
    </div>
    <div className="ClientRequestHeader-avatar">
      <Image
        src="https://s3.amazonaws.com/37assets/svn/1065-IMG_2529.jpg"
        src2x="https://s3.amazonaws.com/37assets/svn/1065-IMG_2529.jpg"
        circle
        width={108}
        height={108}
      />
    </div>
    <div className="ClientRequestHeader-master">
      Ваш мастер: <b>Светлана</b>
    </div>
  </div>
)

const ClientRequestServicePicker = () => (
  <div className="ClientRequestServicePicker">
    <div className="ClientRequestServicePicker-radio">
      <Radio text="Маникюр (600 р.)" value />
    </div>
    <div className="ClientRequestServicePicker-radio">
      <Radio text="Педикюр (300 р.)" />
    </div>
  </div>
)

const ClientRequestForm = () => (
  <div>
    <div className="ClientRequestForm">
      <div className="ClientRequestForm-column">
        <View padding="bsmall">Имя*</View>
        <View padding="bnormal">
          <Input placeholder="Ваше имя" />
        </View>

        <View padding="bsmall">Email*</View>
        <View padding="bnormal">
          <Input placeholder="mail@example.com" />
        </View>

        <View padding="bsmall">Телефон*</View>
        <View padding="bnormal">
          <Input placeholder="7 (999) 999-99-99" />
        </View>
      </div>
      <div className="ClientRequestForm-column">
        <View padding="bsmall">Комментарий мастеру</View>
        <View padding="bnormal">
          <Input multiline />
        </View>
        <Checkbox text="Согласие на обработку персональных данных*" />
        <View padding={['tsmall', 'llarge']}>
          <Text size="small">
            Ваши данные будут переданы только мастеру.{' '}
            <Link>Читать подробнее.</Link>
          </Text>
        </View>
      </div>
    </div>
    <View padding="tnormal" align="end">
      <Text color="lighten">* Обязательно для заполнения</Text>
    </View>
  </div>
)

const ClientRequest = () => (
  <div className="ClientRequest">
    <ClientRequestHeader />

    <div className="ClientRequest-blockHeading">
      <div className="ClientRequest-blockHeading-num">1.</div>
      Выберите услугу
    </div>
    <div className="ClientRequest-block">
      <ClientRequestServicePicker />
    </div>

    <div className="ClientRequest-blockHeading">
      <div className="ClientRequest-blockHeading-num">2.</div>
      Выберите дату и время
    </div>
    <div className="ClientRequest-block">
      <ClientRequestDateSelector />
    </div>

    <div className="ClientRequest-blockHeading">
      <div className="ClientRequest-blockHeading-num">3.</div>
      Заполните информацию о себе
    </div>
    <div className="ClientRequest-block">
      <ClientRequestForm />
    </div>

    <SplitLine />

    <div className="ClientRequest-footer">
      <Button large>Записаться</Button>
    </div>
  </div>
)

export default ClientRequest
