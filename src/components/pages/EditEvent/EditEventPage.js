import { connect } from 'react-redux'
import { createSelector, createStructuredSelector } from 'reselect'
import React from 'react'

import EditEvent from 'components/blocks/EditEvent'
import Navbar from 'components/layouts/SidebarLayout/Navbar'
import NavbarButton from 'components/ui-elements/NavbarButton'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

const EditEventPage = () => (
  <View scrollable flexGrow={1} padding="normal">
    <EditEvent />
  </View>
)

EditEventPage.navbar = function EditEventPageNavbar() {
  return (
    <Navbar
      left={<NavbarButton text="Отмена" />}
      right={<NavbarButton text="Сохранить" />}
    />
  )
}

const selector = createStructuredSelector({
  isEditing: createSelector(
    selectors.getPathname,
    x => x.pathname !== routes.newEventPath()
  ),
})

EditEventPage.navbar = connect(selector)(EditEventPage.navbar)
export default connect(selector)(EditEventPage)
