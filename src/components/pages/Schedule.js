import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { Switch, Route } from 'react-router'
import DayView from 'components/blocks/DayView'
import MenuButton from 'components/ui-elements/MenuButton'
import MonthGrid from 'components/blocks/MonthGrid'
import Navbar from 'components/layouts/SidebarLayout/Navbar'
import NavbarButton from 'components/ui-elements/NavbarButton'
import ScheduleHeader from 'components/blocks/ScheduleHeader'
import Switcher from 'components/ui-elements/Switcher'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

const Schedule = ({ isDesktop }) => (
  <View flexGrow={1}>
    {isDesktop && <ScheduleHeader />}
    <Switch>
      <Route path={routes.dailySchedule()} component={DayView} />
      <Route path={`(/|${routes.monthlySchedule()})`} component={MonthGrid} />
    </Switch>
  </View>
)

Schedule.propTypes = {
  isDesktop: PropTypes.bool.isRequired,
}

const selector = createStructuredSelector({
  isDesktop: selectors.isDesktop,
  pathname: selectors.getPathname,
})

const actions = {
  onPush: push,
}

Schedule.navbar = connect(selector, actions)(function ScheduleNavbar({
  pathname,
  onPush,
}) {
  const isMonth = pathname === '/' || pathname === routes.monthlySchedule()
  const onSwitcherChange = route =>
    onPush(
      {
        month: routes.monthlySchedule(),
        day: routes.dailySchedule(),
      }[route]
    )

  return (
    <Navbar
      left={<MenuButton />}
      center={
        <Switcher
          active={isMonth ? 'month' : 'day'}
          items={[
            { value: 'month', label: 'Месяц' },
            { value: 'day', label: 'День' },
          ]}
          onChange={onSwitcherChange}
        />
      }
      right={<NavbarButton icon="plus" />}
    />
  )
})

export default connect(selector)(Schedule)
