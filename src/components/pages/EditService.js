import { connect } from 'react-redux'
import { createSelector, createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'
import bind from 'lodash/bind'

import { createService, deleteService, updateService } from 'actions/services'
import Button from 'components/ui-elements/Button'
import DurationPicker from 'components/blocks/DurationPicker'
import Form from 'components/blocks/Form'
import Input from 'components/ui-elements/Input'
import Navbar from 'components/layouts/SidebarLayout/Navbar'
import NavbarButton from 'components/ui-elements/NavbarButton'
import SidebarLayout from 'components/layouts/SidebarLayout'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

class EditService extends Form {
  constructor(props) {
    super(props)

    this.delete = bind(this.delete, this)

    const service = props.service || {}

    this.state = {
      ...this.state,
      fields: {
        title: service.title || '',
        duration: service.duration || 60,
        price: service.price ? `${service.price / 100}` : null,
      },
    }
  }

  submit() {
    const {
      service,
      onboardingIsFinished,
      onCreateService,
      onUpdateService,
      onPush,
    } = this.props
    const { fields } = this.state
    const endRoute = onboardingIsFinished ? routes.services() : routes.root()

    const method = service
      ? data => onUpdateService(service.id, data)
      : data => onCreateService(data)

    return method({
      ...fields,
      price: Math.round(parseFloat(fields.price) * 100),
    }).do(() => onPush(endRoute))
  }

  delete() {
    const {
      onDeleteService,
      onboardingIsFinished,
      onPush,
      service,
    } = this.props
    const endRoute = onboardingIsFinished ? routes.services() : routes.root()

    onDeleteService(service.id)
    onPush(endRoute)
  }

  render() {
    const {
      onboardingIsFinished,
      service,
      onPush,
    } = this.props
    const { isSaving } = this.state
    const endRoute = onboardingIsFinished ? routes.services() : routes.root()

    return (
      <SidebarLayout
        navbar={
          <Navbar
            left={
              <NavbarButton
                disabled={isSaving}
                text="Отмена"
                onClick={() => onPush(endRoute)}
              />
            }
            right={
              <NavbarButton
                loading={isSaving}
                text="Сохранить"
                onClick={this.handleSubmit}
              />
            }
          />
        }
      >
        <View flexGrow={1}>
          <View flexGrow={1} padding="normal" scrollable>
            <View padding="bnormal">
              <Text>Название:</Text>
              <View padding="bsmall" />
              <Input {...this.getInputProps('title')} />
            </View>

            <View padding="bnormal">
              <Text>Цена (р.):</Text>
              <View padding="bsmall" />
              <Input {...this.getInputProps('price')} />
            </View>

            <View padding="bnormal">
              <Text>Время сеанса:</Text>
              <View padding="bsmall" />
              <DurationPicker {...this.getInputProps('duration')} />
            </View>
          </View>

          {service && (
            <View>
              <Button outline footer onClick={this.delete}>
                Удалить
              </Button>
            </View>
          )}
        </View>
      </SidebarLayout>
    )
  }
}

EditService.propTypes = {
  onboardingIsFinished: PropTypes.bool.isRequired,
  onPush: PropTypes.func.isRequired,
  onCreateService: PropTypes.func.isRequired,
  onUpdateService: PropTypes.func.isRequired,
  onDeleteService: PropTypes.func.isRequired,
}

const selector = createStructuredSelector({
  onboardingIsFinished: selectors.onboardingIsFinished,
  service: createSelector(
    selectors.getServices,
    (state, props) => props.match.params,
    (services, params) => services[params.id]
  ),
})

const actions = {
  onPush: push,
  onCreateService: createService,
  onUpdateService: updateService,
  onDeleteService: deleteService,
}

export default connect(selector, actions)(EditService)
