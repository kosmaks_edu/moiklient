import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { editEventPath } from 'helpers/routes/events'
import Button from 'components/ui-elements/Button'
import EventView from 'components/blocks/EventView'
import Navbar from 'components/layouts/SidebarLayout/Navbar'
import NavbarButton from 'components/ui-elements/NavbarButton'
import View from 'components/ui-elements/View'

const EventViewPage = () => (
  <View flexGrow={1}>
    <View scrollable flexGrow={1}>
      <View padding={['vlarge', 'hnormal']}>
        <EventView />
      </View>
    </View>

    <Button footer outline>
      Удалить запись
    </Button>
  </View>
)

EventViewPage.navbar = function EventViewPageNavbar({ onEdit }) {
  return (
    <Navbar
      left={<NavbarButton isBack text="28 сен." />}
      centerText="Запись клиента"
      right={<NavbarButton text="Изменить" onClick={onEdit} />}
    />
  )
}

EventViewPage.navbar.propTypes = {
  onEdit: PropTypes.func.isRequired,
}

const actions = {
  onEdit: () => push(editEventPath()),
}

EventViewPage.navbar = connect(null, actions)(EventViewPage.navbar)
export default EventViewPage
