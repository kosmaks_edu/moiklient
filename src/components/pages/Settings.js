import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React from 'react'

import AdditionalSettings from 'components/blocks/AdditionalSettings'
import Button from 'components/ui-elements/Button'
import Link from 'components/ui-elements/Link'
import MenuButton from 'components/ui-elements/MenuButton'
import Navbar from 'components/layouts/SidebarLayout/Navbar'
import NavbarButton from 'components/ui-elements/NavbarButton'
import PaymentsSettings from 'components/blocks/PaymentsSettings'
import ProfileSettings from 'components/blocks/ProfileSettings'
import ScheduleSettings from 'components/blocks/ScheduleSettings'
import SettingsList from 'components/blocks/SettingsList'
import Tabs from 'components/ui-elements/Tabs'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

const Settings = ({ isDesktop, pathname, onPush }) => (
  <View flexGrow={1}>
    {isDesktop && (
      <View background="light" border="bnormal" padding={['tlarge', 'hnormal']}>
        <Tabs
          tabs={[
            { id: routes.profileSettings(), title: 'Профиль' },
            { id: routes.scheduleSettings(), title: 'Расписание' },
            { id: routes.additionalSettings(), title: 'Уведомления' },
            { id: routes.paymentsSettings(), title: 'Платежи' },
          ]}
          activeTab={
            pathname === routes.settings() ? routes.profileSettings() : pathname
          }
          onChange={onPush}
        />
      </View>
    )}

    <Switch>
      {!isDesktop && (
        <Route exact path={routes.settings()} component={SettingsList} />
      )}

      <Route
        exact
        path={
          isDesktop ? (
            `(${routes.settings()}|${routes.profileSettings()})`
          ) : (
            routes.profileSettings()
          )
        }
        component={ProfileSettings}
      />

      <Route
        exact
        path={routes.scheduleSettings()}
        component={ScheduleSettings}
      />

      <Route
        exact
        path={routes.additionalSettings()}
        component={AdditionalSettings}
      />

      <Route
        exact
        path={routes.paymentsSettings()}
        component={PaymentsSettings}
      />
    </Switch>

    {isDesktop && (
      <View flexDirection="row" align="center" padding={['hlarge', 'blarge']}>
        <View style={{ width: 180 }}>
          <Button>Сохранить</Button>
        </View>

        <View padding="small" />

        <View style={{ width: 100 }} align="center">
          <Link>Отменить</Link>
        </View>
      </View>
    )}
  </View>
)

Settings.propTypes = {
  pathname: PropTypes.string.isRequired,
  onPush: PropTypes.func.isRequired,
  isDesktop: PropTypes.bool.isRequired,
}

Settings.navbar = function SettingsNavbar({ pathname, onPush }) {
  return (
    <Navbar
      left={
        pathname === routes.settings() ? (
          <MenuButton />
        ) : (
          <NavbarButton
            isBack
            onClick={() => onPush(routes.settings())}
            text="Настройки"
          />
        )
      }
      centerText={
        {
          [routes.settings()]: 'Настройки',
          [routes.profileSettings()]: 'Профиль',
          [routes.scheduleSettings()]: 'Расписание',
          [routes.additionalSettings()]: 'Дополнительно',
          [routes.paymentsSettings()]: 'Платежи',
        }[pathname]
      }
    />
  )
}

Settings.navbar.propTypes = {
  pathname: PropTypes.string.isRequired,
  onPush: PropTypes.func.isRequired,
}

const selector = createStructuredSelector({
  pathname: selectors.getPathname,
  isDesktop: selectors.isDesktop,
})

const actions = {
  onPush: push,
}

Settings.navbar = connect(selector, actions)(Settings.navbar)
export default connect(selector, actions)(Settings)
