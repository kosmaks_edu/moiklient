import React from 'react'

import AuthForm from 'components/blocks/AuthForm'

const SignIn = () => <AuthForm isSignIn />

export default SignIn
