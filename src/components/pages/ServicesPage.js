import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import bind from 'lodash/bind'

import { loadServices } from 'actions/services'
import Button from 'components/ui-elements/Button'
import Icon from 'components/ui-elements/Icon'
import MenuButton from 'components/ui-elements/MenuButton'
import Navbar from 'components/layouts/SidebarLayout/Navbar'
import NavbarButton from 'components/ui-elements/NavbarButton'
import Services from 'components/blocks/Services'
import Spinner from 'components/ui-elements/Spinner'
import Text from 'components/ui-elements/Text'
import View from 'components/ui-elements/View'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

class ServicesPage extends Component {
  constructor(props, context) {
    super(props, context)

    this.showEditor = bind(this.showEditor, this)
    this.hideEditor = bind(this.hideEditor, this)

    this.state = { showEditor: false, isLoading: true }
  }

  componentDidMount() {
    const { onLoadServices } = this.props

    this.request$ = onLoadServices().subscribe(
      () => this.setState({ isLoading: false }),
      () => this.setState({ error: true, isLoading: false })
    )
  }

  componentWillUnmount() {
    if (this.request$) {
      this.request$.unsubscribe()
    }
  }

  showEditor() {
    this.setState({ showEditor: true })
  }

  hideEditor() {
    this.setState({ showEditor: false })
  }

  render() {
    const { services, isDesktop } = this.props
    const { isLoading, showEditor } = this.state

    return (
      <View flexGrow={1}>
        {isDesktop && (
          <View
            background="light"
            border="bnormal"
            padding={['tlarge', 'hnormal', 'bnormal']}
            flexDirection="row"
            align="center"
          >
            <Icon glyph="th-list" style={{ fontSize: 20 }} />
            <View padding="rnormal" />
            <Text size="large">Услуги</Text>
            <View flexGrow={1} />
            <Button disabled={isLoading} onClick={this.showEditor} icon="plus">
              Добавить услугу
            </Button>
          </View>
        )}

        {isLoading ? (
          <View
            flexGrow={1}
            align="center"
            justify="center"
            style={{ minHeight: 300 }}
          >
            <Spinner />
          </View>
        ) : (
          <Services
            services={services}
            showEditor={showEditor}
            onAddSuccess={this.hideEditor}
            onAddCancel={this.hideEditor}
          />
        )}
      </View>
    )
  }
}

ServicesPage.propTypes = {
  services: PropTypes.array.isRequired,
  isDesktop: PropTypes.bool.isRequired,
  onLoadServices: PropTypes.func.isRequired,
}

ServicesPage.navbar = function ServicesPageNavbar({ onPush }) {
  return (
    <Navbar
      left={<MenuButton />}
      centerText="Услуги"
      right={
        <NavbarButton icon="plus" onClick={() => onPush(routes.newService())} />
      }
    />
  )
}

ServicesPage.navbar.propTypes = {
  onPush: PropTypes.func.isRequired,
}

const selector = createStructuredSelector({
  isDesktop: selectors.isDesktop,
  services: selectors.getSortedServices,
})

const actions = {
  onLoadServices: loadServices,
}

ServicesPage.navbar = connect(null, { onPush: push })(ServicesPage.navbar)
export default connect(selector, actions)(ServicesPage)
