import React from 'react'

import Button from 'components/ui-elements/Button'
import EventView from 'components/blocks/EventView'
import Navbar from 'components/layouts/SidebarLayout/Navbar'
import NavbarButton from 'components/ui-elements/NavbarButton'
import View from 'components/ui-elements/View'

const EventRequest = () => (
  <View flexGrow={1}>
    <View scrollable flexGrow={1}>
      <View padding={['vlarge', 'hnormal']}>
        <EventView />
      </View>
    </View>

    <View flexDirection="row">
      <View flexGrow={1}>
        <Button footer outline>
          Отклонить
        </Button>
      </View>
      <View flexGrow={1}>
        <Button footer>Подтвердить</Button>
      </View>
    </View>
  </View>
)

EventRequest.navbar = function EventRequestNavbar() {
  return (
    <Navbar
      left={<NavbarButton isBack text="28 сен." />}
      centerText="Заявка"
    />
  )
}

export default EventRequest
