export const nextMonth = ({ month, year }) => {
  if (month === 12) {
    return { month: 1, year: year + 1 }
  }

  return { month: month + 1, year }
}

export const prevMonth = ({ month, year }) => {
  if (month === 1) {
    return { month: 12, year: year - 1 }
  }

  return { month: month - 1, year }
}

export const addMonths = ({ month, year }, delta) => {
  if (delta === 0) return { month, year }

  let newMonth, yearDelta

  if (delta < 0) {
    newMonth = 12 - (12 - month - delta) % 12
    yearDelta = -Math.floor((12 - month - delta) / 12)
  } else {
    newMonth = (month + delta - 1) % 12 + 1
    yearDelta = Math.floor((month + delta - 1) / 12)
  }

  return { month: newMonth, year: year + yearDelta }
}

export const monthsDiff = (left, right) =>
  left.month - right.month + 12 * (left.year - right.year)
