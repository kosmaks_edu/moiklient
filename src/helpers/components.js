import isArray from 'lodash/isArray'
import map from 'lodash/map'

export const formatModifiers = (prefix, mod) => {
  if (!mod) return ''
  const arr = isArray(mod) ? mod : [mod]
  return map(arr, x => ` ${prefix}${x}`).join('')
}

export const formatGetters = (obj, prefix, mod) => {
  if (!mod) return []
  const arr = isArray(mod) ? mod : [mod]
  return map(arr, x => obj[`${prefix}${x}`])
}
