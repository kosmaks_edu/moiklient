import qs from 'qs'

export const eventViewPath = (id = ':id') => `/events/${id}`

export const eventRequestPath = (id = ':id') => `/requests/${id}`

export const editEventPath = id =>
  `/events/edit${id ? '?' + qs.stringify({ id }) : ''}`

export const newEventPath = () => `/events/new`
