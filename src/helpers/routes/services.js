export const services = () => '/services'

export const editService = (id = ':id') => `/services/${id}/edit`

export const newService = () => `/services/new`
