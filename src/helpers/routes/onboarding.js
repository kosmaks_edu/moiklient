export const signIn = () => '/signIn'

export const signUp = () => '/signup'

export const onboardingProfile = () => '/onboarding/profile'

export const onboardingSchedule = () => '/onboarding/schedule'

export const onboardingServices = () => '/onboarding/services'

export const onboardingAdditional = () => '/onboarding/additional'

export const onboardingPayments = () => '/onboarding/payments'

export const onboardingFinished = () => '/onboarding/finished'
