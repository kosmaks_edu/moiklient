import * as schedule from './schedule'
import * as events from './events'
import * as requests from './requests'
import * as services from './services'
import * as settings from './settings'
import * as onboarding from './onboarding'

export const multiRoute = (...routes) => `(${routes.join('|')})`

export default {
  ...schedule,
  ...events,
  ...requests,
  ...services,
  ...settings,
  ...onboarding,

  root: () => '/',
  clientRequest: () => '/client-request',
}
