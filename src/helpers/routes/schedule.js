import qs from 'qs'

export const monthlySchedule = ({ year, month } = {}) =>
  '/month' + (year || month ? '?' + qs.stringify({ year, month }) : '')

export const dailySchedule = ({ day, year, month } = {}) =>
  '/day' +
  (day || year || month ? '?' + qs.stringify({ day, year, month }) : '')

export const weeklySchedule = () => '/week'
