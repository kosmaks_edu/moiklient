export const profileSettings = () => '/settings/profile'

export const scheduleSettings = () => '/settings/schedule'

export const additionalSettings = () => '/settings/additional'

export const paymentsSettings = () => '/settings/payments'

export const settings = () => '/settings'
