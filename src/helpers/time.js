import compact from 'lodash/compact'

const twoDigit = num => (num < 10 ? `0${num}` : `${num}`)

export const timeToStr = time => {
  const hour = Math.floor(time / 60)
  const minute = time % 60

  return `${twoDigit(hour)}:${twoDigit(minute)}`
}

const decCases = [2, 0, 1, 1, 1, 2]

const decOfNum = (number, titles) => {
  const caseId =
    number % 100 > 4 && number % 100 < 20
      ? 2
      : decCases[Math.min(number % 10, 5)]
  return titles[caseId]
}

export const durationToStr = duration => {
  const hours = Math.floor(duration / 60)
  const minutes = duration % 60

  const hoursStr = hours
    ? `${hours} ${decOfNum(hours, ['час', 'часа', 'часов'])}`
    : null
  const minutesStr = minutes
    ? `${minutes} ${decOfNum(minutes, ['минута', 'минуты', 'минут'])}`
    : null

  return compact([hoursStr, minutesStr]).join(' ')
}
