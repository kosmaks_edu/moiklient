import transform from 'lodash/transform'

export const createReducer = (initialState, handlers) => (
  state = initialState,
  action
) => (handlers[action.type] ? handlers[action.type](state, action) : state)

export const createAsyncAction = type => ({
  REQUEST: `${type}.REQUEST`,
  SUCCESS: `${type}.SUCCESS`,
  FAILURE: `${type}.FAILURE`,
})

export const normalizeEntities = (entities, idKey = 'id') =>
  transform(
    entities,
    (acc, item) => {
      acc[item[idKey]] = item
    },
    {}
  )
