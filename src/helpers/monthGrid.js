import filter from 'lodash/filter'
import moment from 'moment'

export const getNumOfWeeks = ({ year, month }) => {
  const start = moment()
    .year(year)
    .month(month - 1)
    .startOf('month')
    .startOf('isoweek')

  const end = moment()
    .year(year)
    .month(month - 1)
    .endOf('month')
    .endOf('isoweek')

  return end.diff(start, 'week') + 1
}

export const formatDate = (cursor, month, today) => {
  const dayNum = cursor.date()
  const date = cursor.format('YYYY-MM-DD')
  const info = {
    date,
    text: dayNum === 1 ? cursor.format('MMM D') : cursor.format('D'),
    currentMonth: cursor.month() === month - 1,
    today: date === today,
    holiday: cursor.isoWeekday() > 5,
    cursor,
  }
  return info
}

export const getMonthGrid = (year, month) => {
  const date = moment()
    .year(year)
    .month(month - 1)
    .date(1)
    .utc()
  let cursor = date.startOf('isoweek')
  const grid = []
  const today = moment().format('YYYY-MM-DD')

  for (let week = 1; week <= 6; ++week) {
    const row = []

    for (let day = 1; day <= 7; ++day) {
      const info = formatDate(cursor, month, today)
      row.push(info)
      cursor = cursor.add(1, 'day')
    }

    grid.push(row)
  }

  return filter(grid, w => w[0].currentMonth || w[w.length - 1].currentMonth)
}
