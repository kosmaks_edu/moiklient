import { Dimensions } from 'react-native'

import { changeDevice } from 'actions/device'

export const DESKTOP_WIDTH = 1009
export const TABLET_WIDTH = 700

export const PORTRAIT = 'PORTRAIT'
export const LANDSCAPE = 'LANDSCAPE'

export const TABLET = 'TABLET'
export const DESKTOP = 'DESKTOP'
export const MOBILE = 'MOBILE'

export const getWidth = () => Dimensions.get('window').width

export const getDevice = () => {
  const width = getWidth()

  if (width >= DESKTOP_WIDTH) return DESKTOP
  if (width >= TABLET_WIDTH) return TABLET
  return MOBILE
}

export const getOrientation = () => {
  const { width, height } = Dimensions.get('window')
  return width < height ? PORTRAIT : LANDSCAPE
}

let cachedDevice = getDevice()
let cachedOrientation = getOrientation()

export const isTabletOrLarger = () => cachedDevice !== MOBILE

export const deviceSelect = (mobile, larger) =>
  cachedDevice === MOBILE ? mobile : larger

export default store => {
  Dimensions.addEventListener('change', () => {
    const device = getDevice()
    const orientation = getOrientation()

    if (cachedDevice !== device || cachedOrientation !== orientation) {
      cachedDevice = device
      cachedOrientation = orientation
      store.dispatch(changeDevice(device, orientation))
    }
  })
}
