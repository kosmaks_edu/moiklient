import get from 'lodash/get'

import { changeDevice } from 'actions/device'

export const DESKTOP_WIDTH = 1009
export const TABLET_WIDTH = 700

export const PORTRAIT = 'PORTRAIT'
export const LANDSCAPE = 'LANDSCAPE'

export const TABLET = 'TABLET'
export const DESKTOP = 'DESKTOP'
export const MOBILE = 'MOBILE'

const getWidth = () => {
  const document = window.document

  if (window.innerWidth) {
    return window.innerWidth
  }

  if (document.documentElement) {
    return document.documentElement.clientWidth
  }

  const body = document.getElementsByTagName('body')[0]
  return get(body, 'clientWidth')
}

export const getDevice = () => {
  const width = getWidth()

  if (width >= DESKTOP_WIDTH) return DESKTOP
  if (width >= TABLET_WIDTH) return TABLET
  return MOBILE
}

export const getOrientation = () => PORTRAIT

export default store => {
  let cachedDevice = getDevice()

  window.onresize = () => {
    const device = getDevice()
    if (cachedDevice !== device) {
      cachedDevice = device
      store.dispatch(changeDevice(device, getOrientation()))
    }
  }
}
