export const formatMoney = price => {
  const norm = (Math.round(price) / 100).toFixed(2).toString()
  const number = norm.replace(/\.[^0]*0+$/, '').replace(/\./g, ',')
  return `${number} р.`
}
