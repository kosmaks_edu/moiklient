import noop from 'lodash/noop'

export const platformPick = mapping =>
  mapping.browser ? mapping.browser() : (mapping.default || noop)()
