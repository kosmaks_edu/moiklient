import { Platform } from 'react-native'

export const platformPick = mapping => {
  const isIOS = Platform.OS === 'ios'
  const isAndroid = Platform.OS === 'android'

  if (mapping.ios && isIOS) {
    return mapping.ios()
  }

  if (mapping.android && isAndroid) {
    return mapping.android()
  }

  if (mapping.native) {
    return mapping.native()
  }

  if (mapping.default) {
    return mapping.default()
  }
}
