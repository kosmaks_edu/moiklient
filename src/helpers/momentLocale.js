import moment from 'moment'

import { MONTHS_SHORT, MONTHS } from 'constants/calendar'

moment.locale('ru', {
  monthsShort: MONTHS_SHORT,
  months: MONTHS,
})
