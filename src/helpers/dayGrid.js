export const getHourTicks = ([from, to]) => {
  const firstTick = {
    stamp: from,
  }

  const ticks = [firstTick]

  let nextStamp = Math.ceil(from / 60) * 60
  if (nextStamp === from) {
    nextStamp += 60
  }

  while (nextStamp < to) {
    ticks.push({
      stamp: nextStamp,
    })
    nextStamp += 60
  }

  if (ticks[ticks.length - 1].stamp < to) {
    ticks.push({
      stamp: to,
    })
  }

  return ticks
}
