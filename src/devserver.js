'use strict'

const path = require('path')
const express = require('express')
const webpack = require('webpack')
const webpackMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const config = require('../webpack.config')

const app = express()

const compiler = webpack(config)

const middleware = webpackMiddleware(compiler, {
  publicPath: config.output.publicPath,
  contentBase: 'src',
  stats: {
    color: true,
    noInfo: true,
    modules: false,
  },
})

app.use(middleware)
app.use(webpackHotMiddleware(compiler))

app.get('*', (req, res, next) => {
  const filename = path.join(compiler.outputPath, 'index.html')

  compiler.outputFileSystem.readFile(filename, (err, html) => {
    if (err) {
      return next(err)
    }
    res.setHeader('Content-Type', 'text/html; charset=UTF-8')
    res.end(html)
  })
})

app.listen(3000, () => console.log('Server started!')) // eslint-disable-line
