import { Subject } from 'rxjs/Subject'
import qs from 'qs'
import isEmpty from 'lodash/isEmpty'

import { API_QUERY } from 'actions/api'
import config from 'constants/config'
import selectors from 'store/reducers/selectors'

export default store => next => action => {
  if (action.type !== API_QUERY) {
    return next(action)
  }

  const defaultHeaders = {
    'Content-Type': 'application/json; charset=utf-8',
  }

  const state = store.getState()
  const isAuthorized = selectors.isAuthorized(state)
  if (isAuthorized) {
    const token = selectors.getToken(state)
    defaultHeaders.Authorization = `Bearer ${token}`
  }

  // Extracting options
  const { payload } = action
  const path = payload.url || `${config.api.url}${payload.path}`
  const method = payload.method || 'GET'
  const headers = {
    ...defaultHeaders,
    ...payload.headers,
  }
  const types = payload.types || {}
  const query = payload.query || {}
  const meta = payload.meta || {}

  const search = method === 'GET' ? '?' + qs.stringify(query) : ''
  const body = method !== 'GET' ? JSON.stringify(query) : ''

  // Validating
  if (process.env.NODE_ENV === 'development') {
    if (!payload.url && payload.path && payload.path[0] !== '/') {
      console.warn(`Not using absolute path for "${payload.path}"`) // eslint-disable-line
    }
    if (!method) {
      console.warn(`No method provided for "${path}". Falling back to GET.`) // eslint-disable-line
    }
    if (isEmpty(types)) {
      console.error(`No types defined for "${path}"`) // eslint-disable-line
    }
  }

  // Request
  const subject$ = new Subject()

  next({
    type: types.REQUEST,
    payload: { path, method },
  })

  fetch(path + search, {
    headers,
    method,
    ...(method === 'GET' ? {} : { body }),
  })
    .then(result =>
      result.json().then(data => {
        if (result.status === 200) {
          next({
            type: types.SUCCESS,
            meta,
            payload: data,
          })
          subject$.next(data)
          subject$.complete()
        } else {
          next({
            type: types.FAILURE,
            payload: data,
          })

          subject$.error(data)
        }
      })
    )
    .catch(error => {
      next({
        type: types.FAILURE,
        meta,
        payload: error,
      })

      subject$.error(error)
    })

  return subject$
}
