import { REHYDRATE } from 'redux-persist'
import { replace } from 'react-router-redux'

import { LOG_OUT, SIGN_IN, SIGN_UP, becomeUser } from 'actions/viewer'
import routes from 'helpers/routes'
import selectors from 'store/reducers/selectors'

export default store => next => action => {
  const { type } = action
  const result = next(action)

  if (
    type === REHYDRATE ||
    type === SIGN_UP.SUCCESS ||
    type === SIGN_IN.SUCCESS
  ) {
    store.dispatch(becomeUser())
  } else if (type === LOG_OUT) {
    store.dispatch(replace(routes.signIn()))
  }

  const state = store.getState()

  if (selectors.isAuthorized(state)) {
    const pathname = selectors.getPathname(state)
    if (pathname === routes.signIn() || pathname === routes.signUp()) {
      store.dispatch(replace(routes.root()))
    }
  }

  return result
}
