import { createStore, compose, applyMiddleware } from 'redux'
import { persistStore } from 'redux-persist'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'

import { platformPick } from 'helpers/platform'
import apiMiddleware from 'store/middlewares/api'
import authMiddleware from 'store/middlewares/auth'
import config from 'constants/config'
import deviceListener from 'helpers/device'
import logger from 'redux-logger'
import reducers from 'store/reducers'

export default ({ history }) => {
  const middlewares = [
    thunk,
    apiMiddleware,
    routerMiddleware(history),
    authMiddleware,
  ]

  let enhancers = []

  if (config.environment === 'development') {
    platformPick({
      browser: () => {
        if (window.devToolsExtension) enhancers.push(window.devToolsExtension())
      },
      native: () => middlewares.push(logger),
    })
  }

  enhancers = [applyMiddleware(...middlewares), ...enhancers]

  const store = createStore(reducers, compose(...enhancers))
  const persistor = persistStore(store)

  deviceListener(store)

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      store.replaceReducer(require('store/reducers').default)
    })
  }

  return { persistor, store }
}
