import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import { routerReducer } from 'react-router-redux'

import storage from 'store/reducers/storage'

import device from './device'
import services from './services'
import sidebar from './sidebar'
import viewer from './viewer'

export default combineReducers({
  device,
  viewer: persistReducer(
    {
      key: 'moiklient-viewer',
      storage: storage(),
    },
    viewer
  ),
  routing: routerReducer,
  sidebar,
  services,
})
