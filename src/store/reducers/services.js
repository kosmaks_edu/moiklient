import omit from 'lodash/omit'

import {
  CREATE_SERVICE,
  DELETE_SERVICE,
  LOAD_SERVICES,
  UPDATE_SERVICE,
} from 'actions/services'
import { createReducer, normalizeEntities } from 'helpers/redux'

const initialState = {
  entities: {},
}

const handlers = {
  [LOAD_SERVICES.SUCCESS]: (state, { payload }) => ({
    ...state,
    entities: normalizeEntities(payload.items),
  }),

  [CREATE_SERVICE.SUCCESS]: (state, { payload }) => ({
    ...state,
    entities: {
      ...state.entities,
      [payload.id]: payload,
    },
  }),

  [UPDATE_SERVICE.SUCCESS]: (state, { payload }) => ({
    ...state,
    entities: {
      ...state.entities,
      [payload.id]: payload,
    },
  }),

  [DELETE_SERVICE.SUCCESS]: (state, { meta }) => ({
    ...state,
    entities: omit(state.entities, meta.id),
  }),
}

export default createReducer(initialState, handlers)
