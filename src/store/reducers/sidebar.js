import { createReducer } from 'helpers/redux'

import { SET_SIDEBAR_VISIBILITY } from 'actions/sidebar'

const initialState = {
  sidebarIsOpen: false,
}

const handlers = {
  [SET_SIDEBAR_VISIBILITY]: (state, { payload }) => ({
    ...state,
    sidebarIsOpen: payload,
  }),
}

export default createReducer(initialState, handlers)
