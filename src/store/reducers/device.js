import { createReducer } from 'helpers/redux'
import { getDevice, getOrientation } from 'helpers/device'

import { CHANGE_DEVICE } from 'actions/device'

const initialState = {
  device: getDevice(),
  orientation: getOrientation(),
  test: 'ok',
}

const handlers = {
  [CHANGE_DEVICE]: (state, { payload }) => ({
    ...state,
    device: payload.device,
    orientation: payload.orientation,
  }),
}

export default createReducer(initialState, handlers)
