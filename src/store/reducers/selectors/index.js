import device from './device'
import * as routing from './routing'
import * as sidebar from './sidebar'
import * as viewer from './viewer'
import * as services from './services'

export default {
  ...device,
  ...routing,
  ...sidebar,
  ...viewer,
  ...services,
}
