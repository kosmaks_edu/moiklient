import { createSelector } from 'reselect'
import { PORTRAIT, LANDSCAPE, MOBILE, TABLET, DESKTOP } from 'helpers/device'

export const getDevice = state => state.device.device

export const isTablet = createSelector(getDevice, device => device === TABLET)

export const isDesktop = createSelector(getDevice, device => device === DESKTOP)

export const isMobile = createSelector(getDevice, device => device === MOBILE)

export const isTabletOrLarger = createSelector(isMobile, x => !x)

export const isLandscape = state => state.device.orientation === LANDSCAPE

export const isPortrait = state => state.device.orientation === PORTRAIT

export default {
  getDevice,
  isTablet,
  isDesktop,
  isMobile,
  isTabletOrLarger,
  isLandscape,
  isPortrait,
}
