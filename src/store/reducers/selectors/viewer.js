export const isAuthorized = state => state.viewer.authorized

export const getToken = state => state.viewer.token

export const getProfile = state => state.viewer.profile

export const onboardingIsFinished = () => true

export const getOnboardingStep = state => state.viewer.onboardingStep
