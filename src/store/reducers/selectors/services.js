import { createSelector } from 'reselect'
import sortBy from 'lodash/sortBy'
import values from 'lodash/values'

export const getServices = state => state.services.entities

export const getSortedServices = createSelector(getServices, services =>
  sortBy(values(services), s => -s.createdAt)
)
