import { createSelector } from 'reselect'

import qs from 'qs'
import isEmpty from 'lodash/isEmpty'

export const getLocation = (state, props) =>
  (props && props.location) || state.routing.location

export const getSearch = createSelector(getLocation, x => x.search)

export const getPathname = createSelector(
  getLocation,
  x => (x.pathname !== '/' ? x.pathname.replace(/\/+$/, '') : x.pathname)
)

export const getQuery = createSelector(
  getSearch,
  search => (isEmpty(search) ? {} : qs.parse(search.slice(1)))
)
