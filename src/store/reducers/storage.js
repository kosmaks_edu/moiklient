import CookieStorage from 'redux-persist-cookie-storage'

export default () => new CookieStorage()
