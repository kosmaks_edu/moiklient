import { PROFILE } from 'constants/onboardingSteps'
import { SIGN_UP, SIGN_IN, BECOME_USER, LOG_OUT } from 'actions/viewer'
import { SET_ONBOARDING_STEP } from 'actions/onboarding'
import { createReducer } from 'helpers/redux'

const initialState = {
  onboardingStep: PROFILE,
  authorized: false,
  token: null,
  profile: {},
}

const handlers = {
  [SIGN_UP.REQUEST]: (state, { payload }) => ({
    ...state,
    authorized: false,
    token: null,
  }),

  [SIGN_UP.SUCCESS]: (state, { payload }) => ({
    ...state,
    authorized: true,
    token: payload.token,
  }),

  [SIGN_IN.REQUEST]: (state, { payload }) => ({
    ...state,
    authorized: false,
    token: null,
  }),

  [SIGN_IN.SUCCESS]: (state, { payload }) => ({
    ...state,
    authorized: true,
    token: payload.token,
  }),

  [BECOME_USER.SUCCESS]: (state, { payload }) => ({
    ...state,
    profile: payload,
  }),

  [SET_ONBOARDING_STEP]: (state, { payload }) => ({
    ...state,
    onboardingStep: payload,
  }),

  [LOG_OUT]: (state, { payload }) => initialState,
}

export default createReducer(initialState, handlers)
