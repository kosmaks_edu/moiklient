import 'rxjs/add/operator/do'

import 'helpers/momentLocale'

import { ConnectedRouter } from 'react-router-redux'
import { Provider } from 'react-redux'
import React from 'react'
import ReactDOM from 'react-dom'
import createHistory from 'history/createBrowserHistory'

import { AppContainer } from 'react-hot-loader'
import App from 'components/App'
import createStore from 'store'

const history = createHistory()
const { store } = createStore({ history })

const render = Comp => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Comp />
        </ConnectedRouter>
      </Provider>
    </AppContainer>,
    document.getElementById('app')
  )
}

render(App)

if (module.hot) {
  module.hot.accept('./components/App', () => {
    render(require('components/App').default)
  })
}
