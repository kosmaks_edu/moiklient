module.exports = {
  extends: ['standard', 'plugin:react/recommended'],
  env: {
    browser: true,
    mocha: true,
    node: true,
  },

  plugins: ['react'],

  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      modules: true,
    },
  },

  rules: {
    'no-console': 2,
    'space-before-function-paren': 0,
    'indent': 0,
    'comma-dangle': [
      'error',
      {
        arrays: 'always-multiline',
        objects: 'always-multiline',
        imports: 'always-multiline',
        exports: 'always-multiline',
        functions: 'never',
      },
    ],
  },
}
