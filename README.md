## Android signing

* Generate keystore file using:

```bash
$ keytool -genkey -v -keystore release.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000
```

* Move keystore under `android/app`

```bash
$ mv release.keystore path/to/project/android/app
```

* Open `~/.gradle/gradle.properties` and add following lines:

```
MOIKLIENT_RELEASE_STORE_FILE=release.keystore
MOIKLIENT_RELEASE_KEY_ALIAS=my-key-alias
MOIKLIENT_RELEASE_STORE_PASSWORD=*****
MOIKLIENT_RELEASE_KEY_PASSWORD=*****
```
